using FMS.Data.Database;
using FMS.Data.Repositories;
using FMS.Data.Repositories.Contracts;
using FMS.Services.Services;
using FMS.Services.Services.Contracts;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FMS.MVC
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //DB cycle issue workaround hack
            services.AddControllersWithViews().AddNewtonsoftJson(options =>
            options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            //Services
            services.AddScoped<ICommentsService, CommentsService>();
            services.AddScoped<IRepliesService, RepliesService>();
            services.AddScoped<IUsersService, UsersService>();
            services.AddScoped<IPostsService, PostsService>();
            services.AddScoped<ITagsService, TagsService>();
            services.AddScoped<IHomeService, HomeService>();


            //Repositories
            services.AddScoped<ICommentsRepository, CommentsRepository>();
            services.AddScoped<IRepliesRepository, RepliesRepository>();
            services.AddScoped<IUsersRepository, UsersRepository>();
            services.AddScoped<ITagsRepository, TagsRepository>();

            //Mappers and managers
            services.AddScoped<IAuthManager, AuthManager>();

            //DB context
            //Copy the server name from YOUR SQL SERVER initial page below
            services.AddDbContext<FMSDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));

            //Session configuration
            //Cookies are not being checked with the user to abide with those
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromSeconds(1000);
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });

            services.AddHttpContextAccessor();

            //Razor support
            services.AddRazorPages();
            services.AddMvc().AddRazorRuntimeCompilation();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseSession();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });
        }
    }
}
