﻿using FMS.Data.Models;
using FMS.Data.Models.QueryParameters;
using FMS.Data.Repositories.Contracts;
using FMS.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FMS.Services.Services
{
    public class CommentsService : ICommentsService
    {
        private readonly ICommentsRepository commentsRepository;
        private readonly IAuthManager authManager;

        public CommentsService(ICommentsRepository commentsRepository, IAuthManager authManager)
        {
            this.commentsRepository = commentsRepository;
            this.authManager = authManager;
        }

        public Comment Create(Comment commentToCreate)
        {
            return this.commentsRepository.Create(commentToCreate);
        }

        public bool Delete(Comment commentToDelete)
        {
            if (this.commentsRepository.Delete(commentToDelete))
            {
                return true;
            }

            return false;
        }

        public IEnumerable<Comment> FilterBy(CommentQueryParameters filterParameters, int postId)
        {
            var filteredComments = this.commentsRepository
                .FilterBy(filterParameters, postId);

            // Filter by containing content
            if (!string.IsNullOrEmpty(filterParameters.ContentContains))
            {
                filteredComments = filteredComments
                    .Where(c => c.Content
                        .Contains(filterParameters.ContentContains)
                );
            }

            // Filter by User/Author username
            if (!string.IsNullOrEmpty(filterParameters.AuthorUserName))
            {
                filteredComments = filteredComments
                    .Where(c => c.User.Username
                        .ToLower() == filterParameters.AuthorUserName
                            .ToLower()
                );
            }

            // Order by ascending date
            if (!string.IsNullOrEmpty(filterParameters.SortBy))
            {
                // Order by ascending creation date
                if (filterParameters.SortBy
                    .Equals("date",StringComparison.InvariantCultureIgnoreCase)
                )
                {
                    filteredComments
                        .OrderBy(c => c.CreatedOn);
                }

                //Order by descending
                if (!string.IsNullOrEmpty(filterParameters.SortOrder) &&
                    filterParameters.SortOrder
                    .Equals("desc", StringComparison.InvariantCultureIgnoreCase)
                )
                {
                    filteredComments
                        .Reverse();
                }
            }

            return filteredComments;
        }

        public IEnumerable<Comment> GetAll()
        {
            return this.commentsRepository
                .GetAll();
        }

        public IEnumerable<Comment> GetAllByPostId(int postId)
        {
            return this.commentsRepository
                .GetAllByPostId(postId);
        }

        public IEnumerable<Comment> GetAllByUserId(int userId)
        {
            return this.commentsRepository
                .GetAllByUserId(userId);
        }

        public Comment GetById(int id)
        {
            return this.commentsRepository
                .GetById(id);
        }

        public Comment Update(int commentId, string content)
        {
            var commentToUpdate = this.GetById(commentId);

            commentToUpdate.Content = content;
            commentToUpdate.ModifiedOn = DateTime.Now;

            return this.commentsRepository
                .Update(commentToUpdate);
        }

        public int CommentCountByCurrentUser()
        {
            var userId = this.authManager.CurrentUser.Id;
            var commentsCount = this.commentsRepository
                .GetAll()
                .Where(c => c.UserId == userId)
                .Count();

            return commentsCount;
        }
    }
}
