﻿using FMS.Data.Database.Seeders;
using FMS.Data.Models.CompositeModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FMS.Data.Database.Configurations
{
    public class PostTagConfiguration : IEntityTypeConfiguration<PostTag>
    {
        public void Configure(EntityTypeBuilder<PostTag> builder)
        {
            //Many-to-many Posts and Tags relation
            builder.HasKey(pt => new { pt.PostId, pt.TagId });
            builder.HasData(PostTagSeeder.SeedData);
        }
    }
}
