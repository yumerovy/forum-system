﻿using FMS.Data.Models;
using FMS.Data.Utilities.Exceptions;
using FMS.Services.DTOs;
using FMS.Services.Mappers;
using FMS.Services.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace FMS.MVC.Controllers
{
    public class CommentsController : Controller
    {
        private readonly ICommentsService commentsService;
        private readonly IPostsService postsService;
        private readonly IAuthManager authManager;

        public CommentsController(
            ICommentsService commentsService,
            IPostsService postsService,
            IAuthManager authManager)
        {
            this.commentsService = commentsService;
            this.postsService = postsService;
            this.authManager = authManager;
        }
        // GET: CommentsController
        public ActionResult Index()
        {
            return View();
        }

        // GET: CommentsController/Details/5
        public ActionResult Details(CommentResponseDTO responce)
        {
            return View(responce);
        }

        [HttpGet]
        // GET: CommentsController/Create
        public ActionResult Create(int postId)
        {
            var model = new CommentInputDTO { PostId = postId };
            return View(model);
        }

        // POST: CommentsController/Create
        [HttpPost]
        public ActionResult Create(int postId, CommentInputDTO model)
        {
            model.PostId = postId;

            if (!this.ModelState.IsValid)
            {
                return View(model);
            }

            try
            {
                var post = this.postsService.GetById(model.PostId);
                var user = this.authManager.CurrentUser;
                var createdComment = this.commentsService.Create(model.MapToCommentModel(user, post));
                var responce = createdComment.MapToCommentResponse();
                return View("Details", responce);
            }
            catch (ApplicationException)
            {
                return this.RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            catch (Exception)
            {
                return this.Redirect("/Shared/Error");
            }
        }

        [HttpGet]
        // GET: CommentsController/Edit/5
        public ActionResult Edit(int commentId)
        {
            var currentContent = this.commentsService.GetById(commentId).Content;
            var model = new CommentInputDTO { CommentId = commentId, Content = currentContent };
            return View(model);
        }

        // POST: CommentsController/Edit/5
        [HttpPost]
        public ActionResult Edit(int commentId, CommentInputDTO model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            try
            {
                this.commentsService.Update(commentId, model.Content);
                return this.RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            catch (ApplicationException)
            {
                return this.RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            catch (Exception)
            {
                return this.Redirect("/Error");
            }
        }

        [HttpGet]
        // GET: CommentsController/Delete/5
        public ActionResult Delete(int commentId)
        {
            var model = this.commentsService.GetById(commentId);
            return View(model);
        }

        // POST: CommentsController/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int commentId)
        {
            try
            {
                this.commentsService.Delete(this.commentsService.GetById(commentId));
                return this.RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            catch (ApplicationException) 
            {
                return this.RedirectToAction(actionName: "Index", controllerName: "Home");

            }
            catch (Exception)
            {
                return this.Redirect("/Error");
            }
        }
    }
}
