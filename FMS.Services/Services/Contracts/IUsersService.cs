﻿using FMS.Data.Models;
using FMS.Data.Models.QueryParameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FMS.Services.Services.Contracts
{
    public interface IUsersService
    {
        List<User> GetAll();
        User GetById(int id);
        User GetByUsername(string username);
        User GetByEmail(string email);
        User Create(User user);
        User Update(int id, User user);
        string Delete(int id);
        void Logout(int userId);

        public IEnumerable<User> FilterBy(UserQueryParameters filterParameter);
    }
}
