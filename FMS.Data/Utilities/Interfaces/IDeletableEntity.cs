﻿namespace FMS.Data.Utilities.Interfaces
{
    public interface IDeletableEntity
    {
        bool isDeleted { get; set; }

    }
}
