﻿using FMS.Data.Models;
using FMS.Data.Models.QueryParameters;
using System.Collections.Generic;

namespace FMS.Data.Repositories.Contracts
{
    public interface ICommentsRepository
    {
        Comment Create(Comment commentToCreate);

        IEnumerable<Comment> GetAllByUserId(int userId);

        IEnumerable<Comment> GetAllByPostId(int postId);

        IEnumerable<Comment> GetAll();

        IEnumerable<Comment> FilterBy(CommentQueryParameters filterParameters, int postId);

        Comment GetById(int id);

        bool Delete(Comment commentToDelete);

        Comment Update(Comment commentToUpdate);
    }
}
