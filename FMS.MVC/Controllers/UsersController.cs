﻿using FMS.Data.Utilities.Exceptions;
using FMS.MVC.Models.Register;
using FMS.MVC.Models.User;
using FMS.MVC.Models.Users;
using FMS.Services.DTOs;
using FMS.Services.Mappers;
using FMS.Services.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FMS.MVC.Controllers
{
    public class UsersController : Controller
    {
        private readonly IUsersService usersService;
        private readonly IAuthManager authManager;
        private readonly IHomeService homeService;
        private readonly IPostsService postsService;
        private readonly ICommentsService commentsService;
        private readonly IRepliesService repliesService;

        public UsersController(IUsersService usersService, 
            IAuthManager authManager, 
            IHomeService homeService, 
            IPostsService postsService,
            ICommentsService commentsService,
            IRepliesService repliesService)
        {
            this.usersService = usersService;
            this.authManager = authManager;
            this.homeService = homeService;
            this.postsService = postsService;
            this.commentsService = commentsService;
            this.repliesService = repliesService;
        }

        public IActionResult Register(RegisterViewModel registerVM)
        {
            RegisterViewModel regViewModel = new RegisterViewModel();

            if (!this.ModelState.IsValid)
            {
                return this.View(regViewModel);
            }

            try
            {
                this.usersService.Create(registerVM.MapToUser());
                //return this.StatusCode(StatusCodes.Status201Created, "User successfully created!");
                return this.RedirectToAction("Login", "Users");

            }
            catch (ArgumentException ex)
            {
                return this.Conflict(ex.Message);

            }
        }

        [HttpGet]
        public ActionResult Login()
        {
            var model = new LogInDTO();

            return this.View(model);
        }

        [HttpPost]
        public ActionResult Login(LogInDTO model)
        {
            if (!this.ModelState.IsValid)
            {

                return this.View(model);
            }

            try
            {
                this.authManager.Authenticate(model.Username, model.Password);

                return this.RedirectToAction("Index", "Home");
            }
            catch (ApplicationException e)
            {
                this.ModelState.AddModelError("Username", e.Message);
                this.ModelState.AddModelError("Password", e.Message);

                return this.View(model);
            }
            catch (Exception) 
            {
                return this.Redirect("/Error");
            }

        }

        [HttpGet]
        public ActionResult Logout()
        {
            authManager.Logout();
            return this.RedirectToAction("Index", "Home");
        }

        // GET: UsersController/Create
        public ActionResult Create()
        {
            return View();
        }

        public IActionResult Profile()
        {
            var profileViewModel = new ProfileViewModel
            {
                PostCount = postsService.PostCountByCurrentUser(),
                CommentsCount = commentsService.CommentCountByCurrentUser(),
                RepliesCount = repliesService.RepliesCountByCurrentUser(),
                PostsByUser = postsService.GetPostsByUserId(this.authManager.CurrentUser.Id)
            };

            return View(profileViewModel);
        }

        [HttpGet]
        public ActionResult Settings()
        {
            var settingsViewModel = new SettingsViewModel();

            return this.View(settingsViewModel);
        }

        public IActionResult Settings(SettingsViewModel settingsViewModel)
        {
            if (!this.ModelState.IsValid)
            {

                return this.View(settingsViewModel);
            }

            try
            {
                var currentUserId = this.authManager.CurrentUser.Id;
                this.usersService.Update(currentUserId, settingsViewModel.MapToUser());
                return this.RedirectToAction("Profile", "Users");
            }
            catch (UnauthenticatedOperationException e)
            {
                return this.View(settingsViewModel);
            }
        }

    }
}
