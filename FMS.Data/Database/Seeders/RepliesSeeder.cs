﻿using FMS.Data.Models;
using System;

namespace FMS.Data.Database.Seeders
{
    internal static class RepliesSeeder
    {
        internal static Reply[] SeedData => new Reply[] {
    new Reply {
        Id = 1,
        CommentId = 22,
        Content = "senectus et netus et malesuada fames ac turpis egestas. Fusce aliquet magna a neque. Nullam ut nisi a odio semper cursus.",
        UserId = 38,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 2,
        CommentId = 14,
        Content = "ut cursus luctus, ipsum leo elementum sem, vitae aliquam eros turpis",
        UserId = 26,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 3,
        CommentId = 32,
        Content = "nec luctus felis purus ac tellus. Suspendisse sed dolor. Fusce mi",
        UserId = 76,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 4,
        CommentId = 29,
        Content = "Nam nulla magna, malesuada vel, convallis in, cursus et, eros. Proin ultrices. Duis volutpat nunc sit amet metus. Aliquam erat volutpat. Nulla facilisis. Suspendisse commodo tincidunt nibh. Phasellus nulla. Integer vulputate, risus a ultricies adipiscing, enim mi tempor lorem, eget mollis lectus pede et risus. Quisque libero",
        UserId = 45,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 5,
        CommentId = 11,
        Content = "accumsan laoreet ipsum. Curabitur consequat, lectus sit amet luctus vulputate, nisi sem semper erat, in consectetuer ipsum nunc id enim. Curabitur massa. Vestibulum accumsan neque et nunc. Quisque ornare tortor at risus. Nunc ac sem ut dolor dapibus gravida.",
        UserId = 61,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 6,
        CommentId = 73,
        Content = "a ultricies adipiscing, enim mi tempor lorem, eget mollis lectus pede et risus. Quisque libero lacus, varius et, euismod et, commodo",
        UserId = 59,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 7,
        CommentId = 25,
        Content = "purus mauris a nunc. In at pede. Cras vulputate velit eu sem. Pellentesque ut ipsum ac mi eleifend egestas. Sed pharetra, felis eget varius ultrices, mauris ipsum porta elit, a feugiat tellus lorem eu metus. In lorem. Donec elementum, lorem ut aliquam iaculis, lacus",
        UserId = 25,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 8,
        CommentId = 8,
        Content = "lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis diam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce aliquet magna a neque. Nullam ut nisi a odio semper cursus.",
        UserId = 11,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 9,
        CommentId = 64,
        Content = "Maecenas ornare egestas ligula. Nullam feugiat placerat velit. Quisque varius. Nam porttitor scelerisque neque. Nullam nisl. Maecenas",
        UserId = 63,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 10,
        CommentId = 55,
        Content = "volutpat nunc sit amet metus. Aliquam erat volutpat. Nulla facilisis. Suspendisse commodo tincidunt nibh. Phasellus nulla. Integer vulputate, risus",
        UserId = 52,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 11,
        CommentId = 44,
        Content = "ac orci. Ut",
        UserId = 27,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 12,
        CommentId = 71,
        Content = "tempus mauris erat eget ipsum. Suspendisse",
        UserId = 30,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 13,
        CommentId = 28,
        Content = "urna convallis erat, eget tincidunt dui augue eu tellus. Phasellus elit pede, malesuada vel, venenatis vel, faucibus id, libero. Donec consectetuer mauris id sapien. Cras dolor dolor, tempus non,",
        UserId = 56,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 14,
        CommentId = 74,
        Content = "luctus et ultrices posuere cubilia Curae",
        UserId = 8,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 15,
        CommentId = 6,
        Content = "Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean eget magna. Suspendisse tristique neque venenatis lacus. Etiam bibendum fermentum metus. Aenean sed pede nec ante blandit viverra. Donec tempus, lorem",
        UserId = 3,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 16,
        CommentId = 63,
        Content = "luctus et ultrices posuere cubilia Curae Donec tincidunt. Donec vitae erat vel pede blandit congue. In scelerisque scelerisque dui. Suspendisse ac metus vitae velit egestas lacinia. Sed congue, elit sed consequat auctor, nunc nulla vulputate dui, nec",
        UserId = 37,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 17,
        CommentId = 32,
        Content = "felis, adipiscing fringilla, porttitor vulputate, posuere vulputate, lacus. Cras interdum. Nunc sollicitudin commodo ipsum. Suspendisse non leo. Vivamus nibh dolor, nonummy ac, feugiat non,",
        UserId = 19,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 18,
        CommentId = 66,
        Content = "ultrices sit amet, risus. Donec nibh enim, gravida sit amet, dapibus id, blandit at,",
        UserId = 13,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 19,
        CommentId = 31,
        Content = "eleifend, nunc risus varius orci, in consequat enim diam vel arcu. Curabitur ut odio",
        UserId = 73,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 20,
        CommentId = 20,
        Content = "dolor. Nulla semper tellus id nunc interdum feugiat. Sed nec metus facilisis lorem tristique aliquet. Phasellus fermentum convallis ligula. Donec luctus aliquet odio. Etiam ligula tortor, dictum eu, placerat eget, venenatis a, magna. Lorem ipsum dolor sit amet, consectetuer adipiscing",
        UserId = 16,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 21,
        CommentId = 44,
        Content = "leo elementum sem, vitae aliquam eros turpis non enim. Mauris quis turpis vitae purus gravida sagittis. Duis gravida. Praesent eu nulla at sem molestie sodales. Mauris blandit enim",
        UserId = 64,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 22,
        CommentId = 29,
        Content = "Donec egestas. Aliquam nec enim. Nunc ut erat. Sed nunc est, mollis non, cursus non, egestas a, dui. Cras pellentesque. Sed dictum. Proin eget odio. Aliquam vulputate ullamcorper magna. Sed eu eros. Nam consequat dolor vitae dolor.",
        UserId = 56,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 23,
        CommentId = 8,
        Content = "suscipit nonummy. Fusce fermentum fermentum arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae Phasellus ornare. Fusce mollis.",
        UserId = 77,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 24,
        CommentId = 3,
        Content = "leo elementum sem, vitae aliquam",
        UserId = 7,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 25,
        CommentId = 73,
        Content = "tincidunt congue turpis. In condimentum. Donec at arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae Donec tincidunt. Donec vitae erat vel pede blandit congue. In scelerisque scelerisque dui. Suspendisse",
        UserId = 28,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 26,
        CommentId = 36,
        Content = "quis, pede. Suspendisse dui. Fusce diam nunc, ullamcorper eu, euismod ac, fermentum vel, mauris. Integer sem elit, pharetra",
        UserId = 8,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 27,
        CommentId = 39,
        Content = "lorem, auctor quis, tristique ac, eleifend vitae, erat. Vivamus nisi. Mauris nulla. Integer urna. Vivamus molestie dapibus ligula. Aliquam erat",
        UserId = 32,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 28,
        CommentId = 49,
        Content = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Etiam laoreet, libero et tristique pellentesque, tellus sem mollis dui, in sodales elit erat vitae risus. Duis a mi fringilla mi",
        UserId = 12,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 29,
        CommentId = 62,
        Content = "suscipit, est ac facilisis facilisis, magna tellus faucibus leo, in lobortis tellus justo sit amet nulla. Donec non justo. Proin non massa non ante bibendum ullamcorper. Duis cursus, diam at pretium aliquet, metus urna convallis erat, eget",
        UserId = 69,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 30,
        CommentId = 65,
        Content = "aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus",
        UserId = 38,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 31,
        CommentId = 36,
        Content = "iaculis nec, eleifend non,",
        UserId = 67,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 32,
        CommentId = 49,
        Content = "tellus id nunc",
        UserId = 50,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 33,
        CommentId = 32,
        Content = "sit amet orci. Ut sagittis lobortis mauris. Suspendisse aliquet molestie tellus. Aenean egestas hendrerit neque. In ornare sagittis felis. Donec tempor, est ac mattis semper, dui lectus rutrum urna, nec luctus felis purus ac tellus. Suspendisse sed dolor. Fusce mi lorem, vehicula et, rutrum eu, ultrices",
        UserId = 20,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 34,
        CommentId = 15,
        Content = "convallis in, cursus et, eros. Proin ultrices. Duis volutpat nunc sit amet metus. Aliquam erat volutpat.",
        UserId = 67,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 35,
        CommentId = 61,
        Content = "fringilla euismod enim. Etiam gravida molestie arcu. Sed eu nibh vulputate mauris sagittis placerat. Cras dictum ultricies ligula. Nullam enim. Sed nulla ante, iaculis nec, eleifend non, dapibus rutrum, justo. Praesent luctus. Curabitur egestas nunc sed libero. Proin sed",
        UserId = 63,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 36,
        CommentId = 28,
        Content = "eu tellus eu augue porttitor interdum. Sed auctor odio a purus. Duis elementum, dui",
        UserId = 76,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 37,
        CommentId = 28,
        Content = "quam quis diam. Pellentesque habitant morbi tristique senectus et netus et",
        UserId = 25,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 38,
        CommentId = 78,
        Content = "arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae Donec tincidunt. Donec vitae erat vel pede blandit congue. In scelerisque scelerisque dui. Suspendisse ac metus vitae velit egestas lacinia. Sed congue, elit sed consequat auctor, nunc nulla vulputate dui, nec tempus",
        UserId = 10,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 39,
        CommentId = 6,
        Content = "rutrum urna,",
        UserId = 46,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 40,
        CommentId = 16,
        Content = "vitae semper egestas, urna justo faucibus lectus, a sollicitudin orci sem eget massa. Suspendisse eleifend. Cras sed leo. Cras vehicula aliquet libero. Integer in magna. Phasellus dolor elit, pellentesque a, facilisis non, bibendum sed, est. Nunc laoreet lectus quis massa. Mauris vestibulum, neque",
        UserId = 17,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 41,
        CommentId = 20,
        Content = "malesuada vel, venenatis vel, faucibus id, libero. Donec consectetuer mauris id sapien. Cras dolor dolor, tempus non, lacinia",
        UserId = 69,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 42,
        CommentId = 31,
        Content = "eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a,",
        UserId = 48,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 43,
        CommentId = 26,
        Content = "vehicula et, rutrum eu, ultrices sit amet, risus. Donec nibh enim,",
        UserId = 75,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 44,
        CommentId = 71,
        Content = "tortor nibh sit amet orci.",
        UserId = 40,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 45,
        CommentId = 48,
        Content = "ac metus vitae",
        UserId = 57,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 46,
        CommentId = 47,
        Content = "est mauris, rhoncus id, mollis nec, cursus a, enim. Suspendisse aliquet, sem ut cursus luctus, ipsum leo elementum sem, vitae aliquam eros turpis non enim. Mauris quis turpis vitae purus gravida sagittis. Duis gravida.",
        UserId = 44,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 47,
        CommentId = 38,
        Content = "nibh. Donec est mauris, rhoncus id, mollis nec, cursus a, enim. Suspendisse aliquet, sem ut cursus luctus, ipsum leo elementum sem, vitae aliquam eros turpis non enim. Mauris quis turpis vitae purus gravida sagittis. Duis gravida. Praesent eu nulla at sem molestie sodales. Mauris",
        UserId = 2,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 48,
        CommentId = 10,
        Content = "accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis diam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce aliquet magna a neque. Nullam ut nisi a odio semper cursus. Integer mollis. Integer tincidunt aliquam arcu. Aliquam ultrices iaculis odio.",
        UserId = 3,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 49,
        CommentId = 33,
        Content = "elit pede, malesuada vel, venenatis vel, faucibus id, libero. Donec consectetuer mauris id sapien. Cras dolor dolor, tempus non, lacinia at, iaculis quis, pede. Praesent eu dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
        UserId = 75,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 50,
        CommentId = 24,
        Content = "velit egestas lacinia. Sed congue, elit sed consequat auctor, nunc nulla vulputate dui, nec tempus mauris erat eget ipsum. Suspendisse sagittis. Nullam vitae diam. Proin dolor. Nulla semper tellus id nunc interdum feugiat. Sed nec metus facilisis lorem tristique aliquet. Phasellus fermentum convallis ligula. Donec luctus aliquet odio. Etiam",
        UserId = 21,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 51,
        CommentId = 23,
        Content = "at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus.",
        UserId = 10,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 52,
        CommentId = 1,
        Content = "sapien imperdiet ornare. In faucibus. Morbi vehicula. Pellentesque tincidunt tempus risus. Donec egestas. Duis ac arcu. Nunc mauris. Morbi non sapien molestie orci tincidunt adipiscing. Mauris molestie pharetra nibh. Aliquam ornare, libero at auctor ullamcorper, nisl arcu iaculis enim, sit amet ornare lectus justo",
        UserId = 35,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 53,
        CommentId = 14,
        Content = "natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec dignissim magna a tortor. Nunc commodo auctor velit. Aliquam",
        UserId = 71,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 54,
        CommentId = 49,
        Content = "Vivamus molestie dapibus ligula. Aliquam erat volutpat. Nulla dignissim. Maecenas ornare egestas ligula. Nullam feugiat placerat velit. Quisque varius. Nam porttitor scelerisque neque.",
        UserId = 75,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 55,
        CommentId = 47,
        Content = "ornare lectus justo eu",
        UserId = 5,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 56,
        CommentId = 74,
        Content = "sapien imperdiet ornare. In faucibus. Morbi vehicula. Pellentesque tincidunt",
        UserId = 41,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 57,
        CommentId = 10,
        Content = "pede sagittis augue, eu tempor erat neque non quam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam fringilla cursus purus. Nullam scelerisque neque sed sem",
        UserId = 75,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 58,
        CommentId = 67,
        Content = "nisi a odio semper cursus. Integer mollis. Integer tincidunt aliquam arcu. Aliquam ultrices",
        UserId = 66,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 59,
        CommentId = 7,
        Content = "et, eros. Proin ultrices. Duis volutpat nunc sit amet metus. Aliquam erat volutpat. Nulla facilisis. Suspendisse commodo tincidunt nibh. Phasellus",
        UserId = 57,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 60,
        CommentId = 52,
        Content = "Suspendisse sagittis. Nullam vitae diam. Proin dolor. Nulla semper tellus id nunc interdum feugiat. Sed nec metus facilisis lorem tristique aliquet. Phasellus fermentum",
        UserId = 18,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 61,
        CommentId = 35,
        Content = "gravida molestie arcu. Sed eu nibh vulputate mauris sagittis placerat. Cras dictum ultricies ligula. Nullam enim. Sed nulla ante, iaculis nec, eleifend non, dapibus rutrum, justo. Praesent luctus. Curabitur egestas nunc sed libero. Proin sed turpis",
        UserId = 16,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 62,
        CommentId = 77,
        Content = "metus. Vivamus euismod urna. Nullam",
        UserId = 79,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 63,
        CommentId = 67,
        Content = "malesuada malesuada. Integer id magna et ipsum cursus vestibulum. Mauris magna. Duis dignissim tempor arcu. Vestibulum",
        UserId = 40,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 64,
        CommentId = 4,
        Content = "mattis semper, dui lectus rutrum urna, nec luctus felis purus ac tellus. Suspendisse sed dolor. Fusce mi lorem,",
        UserId = 74,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 65,
        CommentId = 29,
        Content = "aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis diam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce aliquet magna a neque. Nullam ut nisi a odio semper cursus. Integer mollis. Integer tincidunt aliquam arcu. Aliquam ultrices iaculis odio. Nam interdum enim",
        UserId = 20,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 66,
        CommentId = 40,
        Content = "odio vel est tempor bibendum. Donec felis orci, adipiscing non, luctus sit amet, faucibus ut, nulla. Cras eu tellus eu augue porttitor interdum. Sed auctor odio a purus. Duis elementum, dui quis accumsan",
        UserId = 51,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 67,
        CommentId = 49,
        Content = "dictum eu, placerat eget,",
        UserId = 57,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 68,
        CommentId = 9,
        Content = "vel, convallis in, cursus et, eros. Proin ultrices. Duis volutpat nunc sit amet metus. Aliquam erat volutpat. Nulla",
        UserId = 38,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 69,
        CommentId = 44,
        Content = "eget, dictum placerat, augue. Sed molestie.",
        UserId = 12,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 70,
        CommentId = 26,
        Content = "amet orci. Ut sagittis lobortis mauris. Suspendisse aliquet molestie tellus. Aenean egestas hendrerit neque. In ornare",
        UserId = 47,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 71,
        CommentId = 73,
        Content = "magna. Phasellus dolor elit, pellentesque a, facilisis non, bibendum sed, est. Nunc laoreet lectus quis massa. Mauris vestibulum, neque sed dictum",
        UserId = 70,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 72,
        CommentId = 50,
        Content = "consectetuer adipiscing elit. Etiam laoreet, libero et tristique pellentesque, tellus sem mollis dui,",
        UserId = 41,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 73,
        CommentId = 15,
        Content = "feugiat metus",
        UserId = 39,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 74,
        CommentId = 1,
        Content = "tortor. Nunc commodo auctor velit. Aliquam nisl.",
        UserId = 79,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 75,
        CommentId = 72,
        Content = "accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis diam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce aliquet magna a neque. Nullam ut nisi a odio",
        UserId = 13,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 76,
        CommentId = 60,
        Content = "Duis ac arcu. Nunc mauris. Morbi non sapien molestie orci tincidunt adipiscing. Mauris molestie pharetra nibh. Aliquam ornare, libero at auctor ullamcorper, nisl arcu iaculis enim, sit amet ornare lectus justo eu arcu. Morbi sit amet massa. Quisque porttitor eros nec tellus. Nunc lectus",
        UserId = 70,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 77,
        CommentId = 48,
        Content = "posuere at, velit. Cras lorem lorem, luctus ut, pellentesque eget, dictum placerat, augue. Sed molestie. Sed id risus quis diam luctus lobortis. Class aptent taciti sociosqu",
        UserId = 54,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 78,
        CommentId = 79,
        Content = "aliquam eros turpis non enim. Mauris quis turpis vitae purus gravida sagittis. Duis gravida. Praesent eu nulla at sem molestie",
        UserId = 45,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 79,
        CommentId = 4,
        Content = "id nunc interdum feugiat. Sed nec metus facilisis lorem",
        UserId = 33,
        CreatedOn = DateTime.Now
    },
    new Reply {
        Id = 80,
        CommentId = 53,
        Content = "magna",
        UserId = 55,
        CreatedOn = DateTime.Now
    }
};
    }
}