﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace FMS.Services.DTOs
{
    public class ReplyInputDTO
    {
        public int PostId { get; set; }

        public int CommentId { get; set; }

        public int ReplyId { get; set; }


        [Required(ErrorMessage = "*Required")]
        [MinLength(4, ErrorMessage = "A {0} cannot be less than {1} symbols")]
        [MaxLength(5000, ErrorMessage = "A {0} cannot be more than {1} symbols")]

        public string Content { get; set; }
    }
}
