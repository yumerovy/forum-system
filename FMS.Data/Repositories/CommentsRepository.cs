﻿using FMS.Data.Database;
using FMS.Data.Models;
using FMS.Data.Models.QueryParameters;
using FMS.Data.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;


namespace FMS.Data.Repositories
{
    public class CommentsRepository : ICommentsRepository
    {
        private readonly FMSDbContext context;

        public CommentsRepository(FMSDbContext context)
        {
            this.context = context;
        }

        public Comment Create(Comment commentToCreate)
        {
            this.context.Comments.Add(commentToCreate);
            commentToCreate.CreatedOn = DateTime.Now;
            context.SaveChanges();

            return commentToCreate;
        }

        public bool Delete(Comment commentToDelete)
        {
            commentToDelete.isDeleted = true;
            context.SaveChanges();

            return commentToDelete.isDeleted;
        }

        public IEnumerable<Comment> FilterBy(
            CommentQueryParameters filterParameters,
            int postId = default
        )
        {
            if (postId == default)
            {
                return this.GetAll();
            }

            return this.GetAllByPostId(postId);
        }

        public IEnumerable<Comment> GetAll()
        {
            //TODO...
            // need to be sure how exactly to get the other not deleted entities.
            // Once POSTS and USERS are all set up, this part of the code will need to be revised

            return this.context.Comments
                .Where(c => !c.isDeleted)
                .Include(c => c.User)
                //.Where(c => !c.User.isDeleted)
                .Include(c => c.Post)
                .Where(c => !c.Post.isDeleted);
        }

        public IEnumerable<Comment> GetAllByPostId(int postId)
        {
            //TODO...
            // need to be sure how exactly to get the other not deleted entities.
            // Once POSTS and USERS are all set up, this part of the code will need to be revised

            return this.GetAll().Where(c => c.PostId == postId);
        }

        public IEnumerable<Comment> GetAllByUserId(int userId)
        {
            //TODO...
            // need to be sure how exactly to get the other not deleted entities.
            // Once POSTS and USERS are all set up, this part of the code will need to be revised

            return this.GetAll().Where(c => c.UserId == userId);
        }

        public Comment GetById(int id)
        {
            //TODO...
            // need to be sure how exactly to get the other not deleted entities.
            // Once POSTS and USERS are all set up, this part of the code will need to be revised

            return this.GetAll().FirstOrDefault(c => c.Id == id);
        }

        public Comment Update(Comment commentToUpdate)
        {
            context.SaveChanges();
            return commentToUpdate;
        }
    }
}
