﻿using FMS.Data.Models;
using FMS.Data.Models.QueryParameters;
using FMS.Data.Repositories.Contracts;
using FMS.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FMS.Services.Services
{
    public class RepliesService : IRepliesService
    {
        private readonly IRepliesRepository repliesRepository;
        private readonly IAuthManager authManager;

        public RepliesService(IRepliesRepository repliesRepository, IAuthManager authManager)
        {
            this.repliesRepository = repliesRepository;
            this.authManager = authManager;
        }

        public Reply Create(Reply replyToCreate)
        {
            return this.repliesRepository
                .Create(replyToCreate);
        }

        public bool Delete(Reply replyToDelete)
        {
            return this.repliesRepository
                .Delete(replyToDelete);
        }

        public IEnumerable<Reply> FilterBy(ReplyQueryParameters filterParameters, int commentId)
        {
            var filteredReplies = this.repliesRepository.FilterBy(filterParameters, commentId);

            // Filter by containing content
            if (!string.IsNullOrEmpty(filterParameters.ContentContains))
            {
                filteredReplies = filteredReplies
                    .Where(r => r.Content
                        .Contains(filterParameters.ContentContains)
                );
            }

            // Filter by User/Author username
            if (!string.IsNullOrEmpty(filterParameters.AuthorUserName))
            {
                filteredReplies = filteredReplies
                    .Where(r => r.User.Username
                        .ToLower() == filterParameters.AuthorUserName
                            .ToLower()
                );
            }

            // Order by ascending date
            if (!string.IsNullOrEmpty(filterParameters.SortBy))
            {
                // Order by ascending creation date
                if (filterParameters.SortBy
                    .Equals("date", StringComparison.InvariantCultureIgnoreCase)
                )
                {
                    filteredReplies
                        .OrderBy(r => r.CreatedOn);
                }

                //Order by descending
                if (!string.IsNullOrEmpty(filterParameters.SortOrder) &&
                    filterParameters.SortOrder
                    .Equals("desc", StringComparison.InvariantCultureIgnoreCase)
                )
                {
                    filteredReplies
                        .Reverse();
                }
            }

            return filteredReplies;
        }

        public IEnumerable<Reply> GetAll()
        {
            return this.repliesRepository.GetAll();
        }

        public IEnumerable<Reply> GetAllByCommentId(int commentId)
        {
            return this.repliesRepository
                .GetAllByCommentId(commentId);
        }

        public IEnumerable<Reply> GetAllByUserId(int userId)
        {
            return this.GetAllByUserId(userId);
        }

        public Reply GetById(int id)
        {
            return this.repliesRepository
                .GetById(id);
        }

        public Reply Update(int replyId, string content)
        {
            var replyToUpdate = this.GetById(replyId);
            replyToUpdate.Content = content;
            replyToUpdate.ModifiedOn = DateTime.Now;

            return this.repliesRepository
                .Update(replyToUpdate);
        }

        public int RepliesCountByCurrentUser()
        {
            var userId = this.authManager.CurrentUser.Id;
            var commentsCount = this.repliesRepository
                .GetAll()
                .Where(c => c.UserId == userId)
                .Count();

            return commentsCount;
        }


    }
}
