﻿using FMS.Services.DTOs;
using System.Collections;
using System.Collections.Generic;

namespace FMS.MVC.Models.Replies
{
    public class AllRepliesByCommentViewModel
    {
        public int CommentId { get; set; }
        public List<ReplyResponseDTO> Replies { get; set; } = new List<ReplyResponseDTO>();

    }
}
