﻿using FMS.Data.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace FMS.Services.DTOs
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        [EmailAddress(ErrorMessage = "Email must be a valid emial address.")]
        public string Email { get; set; }
        public int RoleId { get; set; }
        public Role Role { get; set; }

        [StringLength(32, MinimumLength = 4, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public string FirstName { get; set; }

        [StringLength(32, MinimumLength = 4, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string PersonalInfo { get; set; }
        public byte[] Photo { get; set; }

    }
}
