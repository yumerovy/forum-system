﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FMS.Data.Models.QueryParameters
{
    public class UserQueryParameters
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SortBy { get; set; }
        public string SortOrder { get; set; }
    }
}
