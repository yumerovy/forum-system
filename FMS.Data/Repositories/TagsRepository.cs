﻿using FMS.Data.Database;
using FMS.Data.Models;
using FMS.Data.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FMS.Data.Repositories
{
    public class TagsRepository : ITagsRepository
    {
        private readonly FMSDbContext context;

        public TagsRepository(FMSDbContext context)
        {
            this.context = context;
        }

        public Tag Create(string name)
        {
            var createTag = new Tag
            {
                Name = name
            };

            this.context.Tags.Add(createTag);
            createTag.Id = this.context.Tags.Count();

            return createTag;
        }


        // tags will ALWQAYS be shown in alphabetical order
        public List<Tag> GetAll()
        {
            return this.context.Tags
                .OrderBy(t => t.Name)
                .ToList();
        }

        // Will need to test this one. Not sure if works fine
        public List<Tag> GetAllByPostId(int postId)
        {
            return this.context.Tags
            .Where(t => t.Posts
                .Select(p => p.PostId)
                .Contains(postId))
            .ToList();
        }

        public Tag GetByName(string name)
        {
            return this.context.Tags
                .FirstOrDefault(t => t.Name == name);
        }

        // This can be moved to the service only
        public bool Exists(string name)
        {
            return this.context.Tags
                .Any(t => t.Name == name);
        }
    }
}
