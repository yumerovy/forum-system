using FMS.Data.Database;
using FMS.Data.Repositories;
using FMS.Data.Repositories.Contracts;
using FMS.Services.Services;
using FMS.Services.Services.Contracts;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace FMS.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //DB cycle issue workaround hack
            services.AddControllers().AddNewtonsoftJson(options =>
            options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo { Title = "FMS API", Version = "v1" });
            });

            //Services
            services.AddScoped<ICommentsService, CommentsService>();            
            services.AddScoped<IRepliesService, RepliesService>();
            services.AddScoped<IUsersService, UsersService>();            
            services.AddScoped<IPostsService, PostsService>();
            services.AddScoped<ITagsService, TagsService>();
            

            //Repositories
            services.AddScoped<ICommentsRepository, CommentsRepository>();
            services.AddScoped<IRepliesRepository, RepliesRepository>();
            services.AddScoped<IUsersRepository, UsersRepository>();
            services.AddScoped<ITagsRepository, TagsRepository>();


            //Mappers and managers
            services.AddScoped<IAuthManager, AuthManager>();


            //DB context
            //Copy the server name from YOUR SQL SERVER initial page below
            services.AddDbContext<FMSDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));

            //services.AddControllersWithViews();
            //services.AddRazorPages();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
           
            //app.UseHttpsRedirection();
            //app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "FMS API V1");
                options.RoutePrefix = "api/swagger";
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
