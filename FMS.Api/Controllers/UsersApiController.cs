﻿using FMS.Data.Models.QueryParameters;
using FMS.Data.Utilities.Exceptions;
using FMS.Services.DTOs;
using FMS.Services.Mappers;
using FMS.Services.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace FMS.Web.Controllers
{
    [ApiController]
    [Route("api/users")]
    public class UsersApiController : ControllerBase
    {
        private readonly IUsersService usersService;
        public UsersApiController(IUsersService usersService)
        {
            this.usersService = usersService;
        }

        [HttpGet]
        public IActionResult GetUsers()
        {
            return this.Ok(this.usersService.GetAll());
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            try
            {
                return this.Ok(this.usersService.GetById(id));
            }
            catch (EntityNotFoundException ex)
            {
                return this.NotFound(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] UserDTO userDTO)
        {
            try
            {
                this.usersService.Create(userDTO.MapToUser());

                return this.StatusCode(StatusCodes.Status201Created, "User successfully created!");
            }
            catch (ArgumentException ex)
            {
                return this.Conflict(ex.Message);
            }
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] UserDTO userDTO)
        {
            if (id != userDTO.Id)
            {
                return this.Conflict("Id mismatch");
            }

            try
            {
                var user = userDTO.MapToUser();
                return this.Ok(this.usersService.Update(id, user));
            }
            catch (EntityNotFoundException ex)
            {

                return this.NotFound(ex.Message);
            }
            catch (DuplicateEntityException ex)
            {
                return this.Conflict(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                var result = this.usersService.Delete(id);

                return this.Ok(result);
            }
            catch (EntityNotFoundException ex)
            {

                return this.NotFound(ex.Message);
            }
        }

        [HttpGet]
        [Route("filter")]
        public IActionResult FilterUsers([FromQuery] UserQueryParameters filterParameter)
        {
            try
            {
                var result = this.usersService.FilterBy(filterParameter);

                return this.Ok(result);
            }
            catch (EntityNotFoundException ex)
            {

                return this.NotFound(ex.Message);
            }
        }
        [HttpGet]
        [Route("GetByUsername")]
        public IActionResult GetByUsername([FromQuery] UserQueryParameters filterParameter)
        {
            try
            {
                return this.Ok(this.usersService.GetByUsername(filterParameter.Username));
            }
            catch (EntityNotFoundException ex)
            {
                return this.NotFound(ex.Message);
            }
        }

        [HttpGet]
        [Route("GetByEmail")]
        public IActionResult GetByEmail([FromQuery] UserQueryParameters filterParameter)
        {
            try
            {
                return this.Ok(this.usersService.GetByEmail(filterParameter.Email));
            }
            catch (EntityNotFoundException ex)
            {
                return this.NotFound(ex.Message);
            }
        }
    }
}
