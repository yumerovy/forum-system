﻿using FMS.Data.Models;
using FMS.MVC.Models.Register;
using FMS.MVC.Models.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FMS.MVC
{
    public static class ViewMapper
    {
        public static User MapToUser(this RegisterViewModel registerVM)
        {
            return new User
            {
                Username = registerVM.Username,
                Password = registerVM.Password,
                Email = registerVM.Email,
                FirstName = registerVM.FirstName,
                LastName = registerVM.LastName,
                PhoneNumber = registerVM.PhoneNumber,
                Photo = registerVM.Photo
            };
        }

        public static User MapToUser(this SettingsViewModel settingsViewModel)
        {

            return new User
            {
                Password = settingsViewModel.Password,
                Email = settingsViewModel.NewEmail,
                FirstName = settingsViewModel.NewFirstName,
                LastName = settingsViewModel.NewLastName,
                PhoneNumber = settingsViewModel.NewPhoneNumber,
                Photo = settingsViewModel.NewPhoto,
                PersonalInfo = settingsViewModel.PersonalInfo
            };
        }
    }
}
