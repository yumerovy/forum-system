﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FMS.Data.Models.QueryParameters
{
    public class PostQueryParameters
    {
        public string UserName { get; set; }

        public string Search { get; set; }

        public string SortBy { get; set; }

        public string SortOrder { get; set; }
    }
}
