﻿using FMS.Data.Models.QueryParameters;
using FMS.Data.Utilities.Exceptions;
using FMS.Services.DTOs;
using FMS.Services.Mappers;
using FMS.Services.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace FMS.Web.Controllers
{
    [Route("api/posts/{postId}/comments")]
    [ApiController]
    public class CommentsApiController : ControllerBase
    {
        private readonly IAuthManager authManager;

        private readonly ICommentsService commentsService;

        private readonly IPostsService postsService;

        public CommentsApiController(
            ICommentsService commentsService,
            IPostsService postsService,
            IAuthManager authManager
        )
        {
            this.commentsService = commentsService;
            this.postsService = postsService;
            this.authManager = authManager;
        }

        [HttpPost("")]
        public IActionResult CreateComment(
            int postId,
            [FromHeader] string credentials,
            [FromBody] CommentInputDTO commentCreateDTO
        )
        {
            // ADD exception handling, instead of a null check!!!
            // Add authentication check
            // Add authorization check

            var splittedCredentials = credentials.Split(":");

            try
            {
                var user = authManager.Authenticate(splittedCredentials[0], splittedCredentials[1]);
                if (user.IsBlocked)
                {
                    throw new UnauthorizedOperationException(
                        "You are blocked and cannot create comments at the moment."
                            + " Please contact a system administrator!"
                    );
                }

                var post = this.postsService.GetById(postId);

                var comment = this.commentsService.Create(
                    commentCreateDTO.MapToCommentModel(user, post)
                );
                return this.Created(
                    $"localhost:5000/api/posts/{postId}/comments{comment.Id}",
                    comment.MapToCommentResponse()
                );
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
            catch (UnauthenticatedOperationException e)
            {
                return this.Forbid(e.Message);
            }
            catch (UnauthorizedOperationException e)
            {
                return this.Unauthorized(e.Message);
            }
        }

        [HttpDelete("{commentId}")]
        public IActionResult Delete(int commentId, [FromHeader] string credentials)
        {
            var splittedCredentials = credentials.Split(":");

            try
            {
                var user = authManager.Authenticate(splittedCredentials[0], splittedCredentials[1]);
                // non-existing exception here
                // unauthenticated exception here

                var commentToDelete = this.commentsService.GetById(commentId);

                if (commentToDelete == null)
                {
                    throw new EntityNotFoundException(
                        $"Comment with id {commentId} does not exist"
                    );
                }

                if (!commentToDelete.UserId.Equals(user.Id))
                {
                    throw new UnauthorizedOperationException(
                        "You are not the author of this post, hence you cannot delete it."
                    );
                }

                if (this.commentsService.Delete(commentToDelete))
                {
                    return this.Ok($"Comment with id {commentId} was deleted");
                }

                // in case the soft delete operation is unsuccessful, the system will
                //throw an exception. If this is caught, it means there is a bug
                //in the delete operation

                throw new InvalidOperationException("The comment could not be deleted!");
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
            catch (UnauthenticatedOperationException e)
            {
                return this.Forbid(e.Message);
            }
            catch (UnauthorizedOperationException e)
            {
                return this.Unauthorized(e.Message);
            }
        }

        [HttpGet("{commentId}")]
        public IActionResult GetById(int commentId)
        {
            var comment = this.commentsService.GetById(commentId);

            if (comment == null)
            {
                return this.NotFound($"Comment with id {commentId} does not exist");
            }

            return this.Ok(comment.MapToCommentResponse());
        }

        [HttpGet("")]
        public IActionResult GetComments(
            int postId,
            [FromQuery] CommentQueryParameters filterParameters
        )
        {
            if (this.postsService.GetAll().Count < postId && postId > 0)
            {
                return this.NotFound($"Sorry, this post does not exist");
            }

            var comments = this.commentsService.FilterBy(filterParameters, postId);

            if (comments.Count() == 0)
            {
                return this.NotFound("There are no comments found by the given parameters");
            }

            return this.Ok(comments.Select(c => c.MapToCommentResponse()).ToList());
        }

        [HttpPut("{commentId}")]
        public IActionResult Put(
            int commentId,
            [FromHeader] string credentials,
            [FromBody] CommentInputDTO commentInputDto
        )
        {
            // ADD exception handling, instead of a null check!!!
            // Add authentication check
            // Add authorization check

            var splittedCredentials = credentials.Split(":");

            try
            {
                //username from header
                //user service to check if the user is logged in
                //usersevrice.isLoggedIn
                var user = authManager.Authenticate(splittedCredentials[0], splittedCredentials[1]);
                
                // non-existing exception here
                // unauthenticated exception here

                var commentToUpdate = this.commentsService.GetById(commentId);

                if (commentToUpdate == null)
                {
                    throw new EntityNotFoundException(
                        $"Comment with id {commentId} does not exist"
                    );
                }

                if (!commentToUpdate.UserId.Equals(user.Id))
                {
                    throw new UnauthorizedOperationException(
                        "You are not the author of this comment, hence you cannot update it."
                    );
                }

                var updatedComment = this.commentsService.Update(
                    commentId,
                    commentInputDto.Content
                );

                return this.Accepted(updatedComment.MapToCommentResponse());
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
            catch (UnauthenticatedOperationException e)
            {
                return this.Forbid(e.Message);
            }
            catch (UnauthorizedOperationException e)
            {
                return this.Unauthorized(e.Message);
            }
        }
    }
}
