﻿using FMS.Data.Models;
using FMS.Data.Models.QueryParameters;
using FMS.Services.DTOs;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FMS.Services.Services.Contracts
{
    public interface IPostsService
    {
        List<Post> GetAll();

        Post GetById(int id);

        List<PostDTO> GetTopTenCommented();

        List<PostDTO> GetTopTenRecent();

        Post Create(Post post);

        Post Update(int id, Post post);

        void Like(Post post, User user);

        void Dislike(Post post, User user);

        string Delete(int id, User user);

        List<Post> FilterBy(PostQueryParameters filterParameters);

        int PostCountByCurrentUser();
        IEnumerable<Post> GetPostsByUserId(int userId);
    }
}
