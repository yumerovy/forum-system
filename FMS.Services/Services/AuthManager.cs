﻿using FMS.Data.Models;
using FMS.Data.Utilities.Exceptions;
using FMS.Services.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Text;

namespace FMS.Services.Services
{
    public class AuthManager : IAuthManager
    {
        private static string authorizationErrorMsg = "Invalid credentials!";
        private const string CURRENT_USER = "CURRENT_USER";
        private readonly IUsersService usersService;
        private readonly IHttpContextAccessor contextAccessor;

        public AuthManager(
            IUsersService usersService, 
            IHttpContextAccessor httpContext)
        {
            this.usersService = usersService;
            this.contextAccessor = httpContext;
        }

        public User CurrentUser 
        {
            get
            {
                try
                {
                    string username = this.contextAccessor.HttpContext.Session.GetString(CURRENT_USER);
                    return this.usersService
                        .GetByUsername(username);
                }
                catch (EntityNotFoundException)
                {
                    return null;
                }
            }
            set 
            {
                if (value != null)
                {
                    this.contextAccessor
                        .HttpContext
                        .Session
                        .SetString(CURRENT_USER, value.Username);
                }
                else
                {
                    this.contextAccessor
                        .HttpContext
                        .Session
                        .Remove(CURRENT_USER);
                }

            }
        }

        public User Authenticate(string username, string password)
        {
            try
            {
                User user = usersService.GetByUsername(username);

                if (VerifyPassword(user, password) != true)
                {
                    throw new UnauthenticatedOperationException(authorizationErrorMsg);
                }
                else
                {
                    user.IsLogged = true;
                    user.LastLogIn = DateTime.Now;
                    this.CurrentUser = user;
                }
                return user;
            }
            catch (EntityNotFoundException)
            {
                throw new EntityNotFoundException(authorizationErrorMsg);
            }
            catch (UnauthorizedOperationException)
            {
                throw new EntityNotFoundException(authorizationErrorMsg);
            }
        }

        public void Logout() 
        {
             // user service may not require the ID, as it gets the whole object
            this.CurrentUser.IsLogged = false;
            usersService.Logout(this.CurrentUser.Id);
            this.CurrentUser = null;
        }

        private bool VerifyPassword(User user, string password)
        {
            var hasher = new PasswordHasher<User>();
            var isVerified = hasher.VerifyHashedPassword(user, user.Password, password);

            switch (isVerified)
            {
                case PasswordVerificationResult.Failed:
                    return false;
                case PasswordVerificationResult.Success:
                    return true;
                case PasswordVerificationResult.SuccessRehashNeeded:
                    return false;
                default:
                    return false;
            }
        }
    }
}