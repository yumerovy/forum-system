# 1. Users

## 1.1 Roles

####

#### ---------------------------------------

# 2. Posts

##

####

#### ---------------------------------------

# 3. Comments
##

#### Question:

#### Answer:

#### ---------------------------------------

# 4. Tags

##

####

#### ---------------------------------------

# 5. Common

#### Question:

    Deleting DB entries - what is the best way when using EF core 3.1?
    Cascading delete, manual delete or soft-delete?

#### Answer:

    **Soft delete** is a good option, so we can use this approach. 
Or, we could just add OnDeleteBehaviour for one of the connected tables to Restrict - 
this will place NULL in the foreign key for the connected table

#### Question:

    Can we create custom SQL queries with the SQL Profiler?

#### Answer:

#### Question:

    Controller layer should take care of returning responses, service layer
    should take care of calling other services, if needed, and the repository
    layer should take care of database (model) field validation. Correct?
    Should those validation happen in the input DTOs?

#### Answer:

    It is fine if we make an input DTO and an output DTO (response).

#### Question:

    Likes and dislikes - what is the best way to implement this?

#### Answer:

    We will use an Enum with 3 options - Indifferent, Like, Dislike
