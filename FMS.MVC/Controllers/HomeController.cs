﻿using FMS.MVC.Models;
using FMS.MVC.Models.Home;
using FMS.Services.Services;
using FMS.Services.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace FMS.MVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly IHomeService homeService;

        public HomeController(IHomeService homeService)
        {
            this.homeService = homeService;
        }

        public IActionResult Index()
        {
            var viewModel = new HomeViewModel
            {
                MostActiveUsers = this.homeService.GetMostActiveUsers(),
                TrendingPosts = this.homeService.GetTrendingPosts(),
                MostCommentedPosts = this.homeService.GetMostCommentedPosts(),
                UsersCount = this.homeService.GetUsersCount(),
                AdminsCount = this.homeService.GetAdmisCount(),
                PostsCount = this.homeService.GetPostsCount()

            };

            return View(viewModel);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
