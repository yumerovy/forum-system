﻿namespace FMS.Services.DTOs
{
    public class CommentResponseDTO
    {
        public int Id { get; set;  }

        public string Author { get; set; }

        public string Content { get; set; }

        public string CreatedOn { get; set; }

        public int PostId { get; set; }

        public string PostTitle { get; set; }

        public string PostCreatedBy { get; set; }

        public string PostCreatedOn { get; set; }
    }
}
