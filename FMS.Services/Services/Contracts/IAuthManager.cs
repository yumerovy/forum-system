﻿using FMS.Data.Models;

namespace FMS.Services.Services.Contracts
{
    public interface IAuthManager
    {
        public User CurrentUser { get; }
        User Authenticate(string username, string password);

        void Logout();
    }
}
