﻿using FMS.Data.Database;
using FMS.Data.Models;
using FMS.Data.Models.QueryParameters;
using FMS.Data.Repositories.Contracts;
using FMS.Data.Utilities.Exceptions;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FMS.Data.Repositories
{
    public class UsersRepository : IUsersRepository
    {
        private readonly FMSDbContext dbContext;
        public UsersRepository(FMSDbContext dbContext)
        {
            this.dbContext = dbContext;
        }


        public List<User> GetAll()
        {
            return dbContext.Users.ToList();
        }

        public User GetById(int id)
        {
            User user = dbContext.Users.Where(u => u.Id == id).FirstOrDefault();

            return user ?? throw new EntityNotFoundException($"User with ID {id} does not exist!");
        }

        public User Create(User user)
        {
            dbContext.Users.Add(user);
            dbContext.SaveChanges();
            return user;
        }

        public User Update(int id, User user)
        {
            user.Id = id;
            User userToUpdate = this.GetById(id);

            if (user.FirstName != null)
            {
                userToUpdate.FirstName = user.FirstName;
            }
            if (user.LastName != null)
            {
                userToUpdate.LastName = user.LastName;
            }
            if (user.Email != null)
            {
                userToUpdate.Email = user.Email;
            }
            if (user.Password != null)
            {
                var hasher = new PasswordHasher<User>();
                user.Password = hasher.HashPassword(user, user.Password);
                userToUpdate.Password = user.Password;
            }
            if (user.PhoneNumber != null)
            {
                userToUpdate.PhoneNumber = user.PhoneNumber;
            }
            if (user.PersonalInfo != null)
            {
                userToUpdate.PersonalInfo = user.PersonalInfo;
            }
            if (user.Photo != null)
            {
                userToUpdate.Photo = user.Photo;
            }
            userToUpdate.ModifiedOn = DateTime.Now;

            //dbContext.Users.Add(userToUpdate);
            // dbContext.Entry(user).State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            dbContext.Update(userToUpdate);
            dbContext.SaveChanges();

            return userToUpdate;
        }

        public string Delete(int id)
        {
            dbContext.Users.Remove(GetById(id));
            dbContext.SaveChanges();

            return $"User with ID {id} is deleted!";
        }

        public bool IsUsernameUnique(string username)
        {
            return dbContext.Users.All(u => u.Username != username);
        }

        public bool IsEmailUnique(string email)
        {
            return dbContext.Users.All(u => u.Email != email);
        }
        public User GetByUsername(string username)
        {
            User user = dbContext.Users.Where(u => u.Username == username).FirstOrDefault();


            return user ?? throw new EntityNotFoundException($"User with Username {username} does not exist!");
        }
        public User GetByEmail(string email)
        {
            User user = dbContext.Users.Where(u => u.Email == email).FirstOrDefault();

            return user ?? throw new EntityNotFoundException($"User with Email {email} does not exist!");
        }
        public User GetByFirstName(string firstname)
        {
            User user = dbContext.Users.Where(u => u.FirstName == firstname).FirstOrDefault();

            return user ?? throw new EntityNotFoundException($"User with first name {firstname} does not exist!");
        }
        public User GetByLastName(string lastname)
        {
            User user = dbContext.Users.Where(u => u.LastName == lastname).FirstOrDefault();

            return user ?? throw new EntityNotFoundException($"User with last name {lastname} does not exist!");
        }

        public IEnumerable<User> FilterBy(UserQueryParameters filterParameter)
        {
            if (!string.IsNullOrEmpty(filterParameter.SortBy)
                && filterParameter.SortBy.Equals("Username", StringComparison.InvariantCultureIgnoreCase)
               && filterParameter.SortOrder.Equals("Ascending", StringComparison.InvariantCultureIgnoreCase))
            {
                return this.dbContext.Users
                    .OrderBy(u => u.Username)
                    .ToList();
            }
            else if (!string.IsNullOrEmpty(filterParameter.SortBy)
                && filterParameter.SortBy.Equals("Username", StringComparison.InvariantCultureIgnoreCase)
               && filterParameter.SortOrder.Equals("Descending", StringComparison.InvariantCultureIgnoreCase))
            {
                return this.dbContext.Users
                    .OrderBy(u => u.Username)
                    .Reverse()
                    .ToList();
            }

            else if (!string.IsNullOrEmpty(filterParameter.SortBy)
                && filterParameter.SortBy.Equals("FirstName", StringComparison.InvariantCultureIgnoreCase)
               && filterParameter.SortOrder.Equals("Ascending", StringComparison.InvariantCultureIgnoreCase))
            {
                return this.dbContext.Users
                    .OrderBy(u => u.FirstName)
                    .ThenBy(u => u.LastName)
                    .ToList();
            }
            else if (!string.IsNullOrEmpty(filterParameter.SortBy)
                && filterParameter.SortBy.Equals("FirstName", StringComparison.InvariantCultureIgnoreCase)
               && filterParameter.SortOrder.Equals("descending", StringComparison.InvariantCultureIgnoreCase))
            {
                return this.dbContext.Users
                    .OrderBy(u => u.FirstName)
                    .ThenBy(u => u.LastName)
                    .Reverse()
                    .ToList();
            }
            return new List<User>();
        }

        public async void Logout(int userId)
        {
            this.dbContext.Users
                .FirstOrDefault(u => u.Id == userId).IsLogged = false;
            await this.dbContext.SaveChangesAsync();
        }
    }
}
