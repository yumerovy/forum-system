﻿using System;

namespace FMS.Data.Utilities.Interfaces
{
    public interface ICreationInfo
    {
        DateTime CreatedOn { get; set; }
        DateTime? ModifiedOn { get; set; }

    }
}
