﻿using FMS.Data.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FMS.MVC.Models.Register
{
	public class RegisterViewModel
	{
		[DisplayName("Enter your Username")]
		[Required(ErrorMessage = "Field can not be empty")]
		public string Username { get; set; }
		[Required(ErrorMessage = "Field can not be empty")]
		[DataType(DataType.Password)]
		public string Password { get; set; }
		[DisplayName("Confirm Password")]
		[DataType(DataType.Password)]
		[Compare("Password")]
		public string ConfirmPassword { get; set; }
		[Required(ErrorMessage = "Field can not be empty")]
		public string FirstName { get; set; }
		[Required(ErrorMessage = "Field can not be empty")]
		public string LastName { get; set; }
		[Required(ErrorMessage = "Field can not be empty")]
		[EmailAddress(ErrorMessage = "Email must be a valid emial address.")]
		public string Email { get; set; }
		public string PhoneNumber { get; set; }
		public string PersonalInfo { get; set; }
		public byte[] Photo { get; set; }
	}
}
