﻿using FMS.Data.Models;
using FMS.Data.Repositories.Contracts;
using FMS.Services.Services.Contracts;
using System.Collections.Generic;

namespace FMS.Services.Services
{
    public class TagsService : ITagsService
    {
        private readonly ITagsRepository tagsRepository;
        public TagsService(ITagsRepository tagsRepository)
        {
            this.tagsRepository = tagsRepository;
        }
        public Tag Create(string name)
        {
            return this.tagsRepository.Create(name);
        }

        public bool Exists(string name)
        {
            return this.tagsRepository.Exists(name);
        }

        public List<Tag> GetAll()
        {
            return this.tagsRepository.GetAll();
        }

        public List<Tag> GetAllByPostId(int postId)
        {
            return this.tagsRepository.GetAllByPostId(postId);
        }

        public Tag GetByName(string name)
        {
            return this.tagsRepository.GetByName(name);
        }



        //public List<Tag> Convert(string tagNames)
        //{
        //    var tempTags = tagNames
        //        .Split(", ", StringSplitOptions.RemoveEmptyEntries)
        //        .ToList();

        //    var convertedTags = new List<Tag>();

        //    foreach (var tagName in tempTags)
        //    {
        //        if (this.Exists(tagName))
        //        {
        //            convertedTags.Add(this.GetByName(tagName));
        //        }
        //        else
        //        {
        //            convertedTags.Add(this.Create(tagName));
        //        }
        //    }

        //    return convertedTags;
        //}
    }
}
