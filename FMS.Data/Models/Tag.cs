﻿using FMS.Data.Models.CompositeModels;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FMS.Data.Models
{
    public class Tag
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MinLength(1, ErrorMessage = "A {0} cannot be less than {1} symbols")]
        [MaxLength(25, ErrorMessage = "A {0} cannot be more than {1} symbols")]
        public string Name { get; set; }

        public List<PostTag> Posts { get; set; } = new List<PostTag>();
    }
}
