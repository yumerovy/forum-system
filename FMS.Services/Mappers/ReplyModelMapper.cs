﻿using FMS.Data.Models;
using FMS.Services.DTOs;
using System.Runtime.CompilerServices;

namespace FMS.Services.Mappers
{
    public static class ReplyModelMapper
    {
        public static Reply MapToReplyModel(
            this ReplyInputDTO replyCreateDTO,
            User user,
            Comment comment)
        {
            return new Reply
            {
                Id = replyCreateDTO.ReplyId,
                Content = replyCreateDTO.Content,
                CommentId = comment.Id,
                UserId = user.Id,
                User = user,
                Comment = comment,
            };
        }

        public static ReplyResponseDTO MapToReplyResponse(this Reply reply)
        {
            return new ReplyResponseDTO
            {
                Id = reply.Id,
                Author = reply.User.Username,
                Content = reply.Content,
                CreatedOn =
                $"{reply.CreatedOn.ToShortDateString()}",

                //RepliedToUser = reply.Comment.User.Username,
                //RepliedToCommentContent = reply.Comment.Content,
                //CommentCreatedOn =
                //$"{reply.Comment.CreatedOn.ToShortDateString()}",

                //PostTitle = reply.Comment.Post.Title,
                //PostCreatedOn =
                //$"{reply.Comment.Post.CreatedOn.ToShortDateString()}",
            };
        }
    }
}
