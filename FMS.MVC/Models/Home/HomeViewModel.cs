﻿using FMS.Data.Models;
using FMS.Services.DTOs;
using System.Collections;
using System.Collections.Generic;
using DataUser = FMS.Data.Models.User;

namespace FMS.MVC.Models.Home
{
    public class HomeViewModel
    {
        public IEnumerable<DataUser> MostActiveUsers { get; set; }
        public IEnumerable<Post> TrendingPosts { get; set; }
        public IEnumerable<PostDTO> MostCommentedPosts { get; set; }
        public int UsersCount { get; set; }
        public int AdminsCount { get; set; }
        public int PostsCount { get; set; }
    }
}
