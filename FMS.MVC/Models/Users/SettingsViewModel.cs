﻿using FMS.Services.Services.Contracts;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FMS.MVC.Models.Users
{
    public class SettingsViewModel
    {

        [DisplayName("New Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [DisplayName("Confirm New Password")]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmNewPassword { get; set; }
        [DisplayName("New First Name")]
        public string NewFirstName { get; set; }
        [DisplayName("New Last Name")]
        public string NewLastName { get; set; }
        [EmailAddress(ErrorMessage = "Email must be a valid emial address.")]
        [DisplayName("New Email Address")]
        public string NewEmail { get; set; }
        [DisplayName("New Phone Number")]
        public string NewPhoneNumber { get; set; }
        [DisplayName("Personal Info")]
        public string PersonalInfo { get; set; }
        [DisplayName("New Photo")]
        public byte[] NewPhoto { get; set; }
    }
}

