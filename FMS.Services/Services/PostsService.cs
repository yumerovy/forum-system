﻿using FMS.Data;
using FMS.Data.Database;
using FMS.Data.Models;
using FMS.Data.Models.QueryParameters;
using FMS.Data.Models.CompositeModels;
using FMS.Data.Models.Enums;
using FMS.Data.Repositories.Contracts;
using FMS.Data.Utilities.Exceptions;
using FMS.Services.DTOs;
using FMS.Services.Services.Contracts;
using FMS.Services.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Collections;

namespace FMS.Services.Services
{
    public class PostsService : IPostsService
    {
        private readonly FMSDbContext database;

        private readonly IUsersRepository usersRepository;
        private readonly IAuthManager authManager;
        public PostsService(IUsersRepository usersRepository, FMSDbContext context, IAuthManager authManager)
        {
            this.usersRepository = usersRepository;
            this.database = context;
            this.authManager = authManager;
        }

        public List<Post> GetAll() =>
            this.database.Posts.Where(p => !p.isDeleted)
            .Include(p => p.PostComments.Where(c => !c.isDeleted))
                .ThenInclude(c => c.Replies.Where(r => !r.isDeleted))
            .Include(p => p.User)
            .Include(p => p.Votes)
            .Include(p => p.Tags)
            .AsSplitQuery()
            .ToList();

        public Post GetById(int id)
        {
            var post = this.GetAll().FirstOrDefault(p => p.Id == id);

            if (post == null) throw new EntityNotFoundException
                 ($"Post with id {id} does not exist!");

            return post;
        }

        public List<PostDTO> GetTopTenCommented()
        {
            var topTenCommented = this.GetAll()
                .OrderByDescending(p => p.PostComments
                    .Select(c => c.MapToCommentResponse()).Count())
                .Take(10)
                .ToList().MapToPostDTOList();

            return topTenCommented;
        }

        public List<PostDTO> GetTopTenRecent()
        {
            var topTenRecent = this.GetAll().OrderBy(p => p.CreatedOn)
                .Reverse()
                .Take(10)
                .ToList().MapToPostDTOList();

            return topTenRecent;
        }

        public Post Create(Post post)
        {
            if (post.User.IsBlocked) throw new UnauthorizedOperationException
        ("You are blocked and don't have permission to create a Post");

            // Initiate the connections for the new post and all users 
            var listOfUsers = this.usersRepository.GetAll();
            foreach (var currentUser in listOfUsers)
            {
                post.Votes.Add(new Rating
                {
                    UserId = currentUser.Id,
                    User = currentUser,
                    PostId = post.Id,
                    Post = post,
                    Vote = VoteType.Default
                });
            }

            this.database.Posts.Add(post);
            database.SaveChanges();

            return post;
        }

        // Might think of an update only the description not the title
        // Also the correctness of the implementation

        public Post Update(int id, Post post)
        {
            var postToUpdate = this.GetById(id);

            if (postToUpdate.UserId != post.UserId) throw new UnauthorizedOperationException
                ($"Post with id {id} is not your madafaka!");

            postToUpdate = post;
            postToUpdate.ModifiedOn = DateTime.Now;
            this.database.SaveChanges();

            return post;
        }

        public void Like(Post post, User user)
        {
            var rating = PostVoteValidator(post, user);

            if (rating.Vote == VoteType.Like) throw new ArgumentException
                ("You have already liked this post!");

            rating.Vote = VoteType.Like;
            database.SaveChanges();
        }

        public void Dislike(Post post, User user)
        {
            var rating = PostVoteValidator(post, user);

            if (rating.Vote == VoteType.Dislike) throw new ArgumentException
                ("You have already disliked this post!");

            rating.Vote = VoteType.Dislike;
            database.SaveChanges();
        }

        public string Delete(int id, User user)
        {
            var postToDelete = this.GetById(id);

            if (postToDelete.UserId == user.Id)
            {
                postToDelete.isDeleted = true;
                database.SaveChanges();
                return $"Your Post with id {id} was successfully deleted!";
            }

            else if (user.Role == Role.Admin)
            {
                postToDelete.isDeleted = true;
                database.SaveChanges();
                return $"Current Post with id {id} was deleted by Administrator {user.Username}!";
            }

            else throw new UnauthorizedOperationException("Who you to delete this post madafaka ?!?");
        }

        public List<Post> FilterBy(PostQueryParameters filterParameters)
        {
            var posts = this.GetAll().Where(p => p.isDeleted == false).ToList();
            var result = new List<Post>();
            IEnumerable<Post> range = new List<Post>();

            string search = filterParameters.Search;
            string[] searchParams = search.Split(' ');

            if (filterParameters.UserName != "")
            {
                result = posts.Where(p => p.User.Username
                        .ToLower() == filterParameters.UserName.ToLower()).ToList();
            }

            if (search != null && searchParams.Count() > 1)
            {
                foreach (var post in posts)
                {
                    // may be needed to implement with for
                    if (post.Title.Contains(search) || post.Description.Contains(search))
                    {
                        result.Add(post);
                        posts.Remove(post);
                    }
                }
            }

            if (searchParams != null)
            {
                foreach (var word in searchParams)
                {
                    Post post = new Post();
                    for (int i = 0; i < posts.Count; i++)
                    {
                        post = posts[i];
                        foreach (var tag in post.Tags)
                        {
                            if (tag.Name == word)
                            {
                                result.Add(post);
                                posts.Remove(post);
                                break;
                            }
                        }

                        if (post.Title.Contains(word) || post.Description.Contains(word))
                        {
                            result.Add(post);
                            posts.Remove(post);
                        }

                    }
                }
            }

            if (result == null) result = posts;

            if (filterParameters.SortBy != "")
            {
                if (filterParameters.SortBy.ToLower() == "date")
                {
                    result.OrderBy(p => p.CreatedOn);
                }

                if (filterParameters.SortOrder != ""
                    && filterParameters.SortOrder.ToLower() == "desc")
                {
                    result.Reverse();
                }
            }

            return result;
        }

        public int PostCountByCurrentUser()
        {
            var userId = this.authManager.CurrentUser.Id;
            var postCount = database.Posts.Where(p => p.UserId == userId).Count();
            return postCount;
        }
        public IEnumerable<Post> GetPostsByUserId(int userId)
        {
            var result = database.Posts.Where(p => p.UserId == userId).Take(1);
            return result;
        }

        private static Rating PostVoteValidator(Post post, User user)
        {
            if (post == null) throw new EntityNotFoundException
                ($"Post with id {post.Id} does not exist!");

            var vote = post.Votes
                .Where(r => r.PostId == post.Id && r.UserId == user.Id)
                .FirstOrDefault();

            return vote;
        }
    }
}
