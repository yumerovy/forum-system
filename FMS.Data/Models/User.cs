﻿using FMS.Data.Models.CompositeModels;
using FMS.Data.Utilities.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace FMS.Data.Models
{
	public class User : ICreationInfo, IDeletableEntity
	{
		public int Id { get; set; }

		public string Username { get; set; }
		public string Password { get; set; }
		public Role Role { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Email { get; set; }
		public string PhoneNumber { get; set; }
		public string PersonalInfo { get; set; }
		public bool IsLogged { get; set; }
		public bool IsBlocked { get; set; }
		public DateTime LastLogIn { get; set; }
		public byte[] Photo { get; set; }
		public bool isDeleted { get; set; }
		public DateTime CreatedOn { get; set; }
		public DateTime? ModifiedOn { get; set; }

		public List<Post> UserPosts { get; set; } = new List<Post>();
		public List<Comment> UserComments { get; set; } = new List<Comment>();
		public List<Reply> UserReplies { get; set; } = new List<Reply>();
		public List<Rating> UserRatings { get; set; } = new List<Rating>();

	}
}
