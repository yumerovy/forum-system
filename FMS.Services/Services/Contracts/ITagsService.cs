﻿using FMS.Data.Models;
using System.Collections.Generic;

namespace FMS.Services.Services.Contracts
{
    public interface ITagsService
    {
        Tag Create(string name);

        bool Exists(string name);

        Tag GetByName(string name);

        List<Tag> GetAll();

        List<Tag> GetAllByPostId(int postId);
    }
}
