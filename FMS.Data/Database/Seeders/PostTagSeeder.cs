﻿using FMS.Data.Models.CompositeModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace FMS.Data.Database.Seeders
{
    internal static class PostTagSeeder
    {
        internal static PostTag[] SeedData => new PostTag[] {
    new PostTag {
        PostId = 58,
        TagId = 18
    },
    new PostTag {
        PostId = 51,
        TagId = 10
    },
    new PostTag {
        PostId = 15,
        TagId = 9
    },
    new PostTag {
        PostId = 25,
        TagId = 11
    },
    new PostTag {
        PostId = 74,
        TagId = 12
    },
    new PostTag {
        PostId = 45,
        TagId = 2
    },
    new PostTag {
        PostId = 61,
        TagId = 4
    },
    new PostTag {
        PostId = 2,
        TagId = 1
    },
    new PostTag {
        PostId = 26,
        TagId = 11
    },
    new PostTag {
        PostId = 27,
        TagId = 16
    },
    new PostTag {
        PostId = 51,
        TagId = 20
    },
    new PostTag {
        PostId = 18,
        TagId = 3
    },
    new PostTag {
        PostId = 75,
        TagId = 17
    },
    new PostTag {
        PostId = 40,
        TagId = 18
    },
    new PostTag {
        PostId = 4,
        TagId = 10
    },
    new PostTag {
        PostId = 67,
        TagId = 14
    },
    new PostTag {
        PostId = 2,
        TagId = 16
    },
    new PostTag {
        PostId = 42,
        TagId = 10
    },
    new PostTag {
        PostId = 29,
        TagId = 12
    },
    new PostTag {
        PostId = 41,
        TagId = 5
    },
    new PostTag {
        PostId = 20,
        TagId = 10
    },
    new PostTag {
        PostId = 49,
        TagId = 13
    },
    new PostTag {
        PostId = 28,
        TagId = 11
    },
    new PostTag {
        PostId = 65,
        TagId = 13
    },
    new PostTag {
        PostId = 68,
        TagId = 14
    },
    new PostTag {
        PostId = 60,
        TagId = 19
    },
    new PostTag {
        PostId = 25,
        TagId = 2
    },
    new PostTag {
        PostId = 47,
        TagId = 4
    },
    new PostTag {
        PostId = 26,
        TagId = 18
    },
    new PostTag {
        PostId = 65,
        TagId = 8
    },
    new PostTag {
        PostId = 32,
        TagId = 9
    },
    new PostTag {
        PostId = 2,
        TagId = 10
    },
    new PostTag {
        PostId = 32,
        TagId = 4
    },
    new PostTag {
        PostId = 35,
        TagId = 15
    },
    new PostTag {
        PostId = 60,
        TagId = 21
    },
    new PostTag {
        PostId = 30,
        TagId = 19
    },
    new PostTag {
        PostId = 53,
        TagId = 11
    },
    new PostTag {
        PostId = 26,
        TagId = 16
    },
    new PostTag {
        PostId = 47,
        TagId = 12
    },
    new PostTag {
        PostId = 76,
        TagId = 16
    },
    new PostTag {
        PostId = 59,
        TagId = 12
    },
    new PostTag {
        PostId = 24,
        TagId = 1
    },
    new PostTag {
        PostId = 3,
        TagId = 4
    },
    new PostTag {
        PostId = 80,
        TagId = 6
    },
    new PostTag {
        PostId = 77,
        TagId = 7
    },
    new PostTag {
        PostId = 63,
        TagId = 8
    },
    new PostTag {
        PostId = 63,
        TagId = 6
    },
    new PostTag {
        PostId = 48,
        TagId = 13
    },
    new PostTag {
        PostId = 73,
        TagId = 18
    },
    new PostTag {
        PostId = 26,
        TagId = 17
    },
    new PostTag {
        PostId = 71,
        TagId = 7
    },
    new PostTag {
        PostId = 22,
        TagId = 13
    },
    new PostTag {
        PostId = 39,
        TagId = 20
    },
    new PostTag {
        PostId = 42,
        TagId = 16
    },
    new PostTag {
        PostId = 61,
        TagId = 15
    },
    new PostTag {
        PostId = 65,
        TagId = 20
    },
    new PostTag {
        PostId = 33,
        TagId = 19
    },
    new PostTag {
        PostId = 43,
        TagId = 3
    },
    new PostTag {
        PostId = 79,
        TagId = 11
    },
    new PostTag {
        PostId = 64,
        TagId = 5
    },
    new PostTag {
        PostId = 23,
        TagId = 9
    },
    new PostTag {
        PostId = 75,
        TagId = 12
    },
    new PostTag {
        PostId = 25,
        TagId = 15
    },
    new PostTag {
        PostId = 73,
        TagId = 10
    },
    new PostTag {
        PostId = 2,
        TagId = 19
    },
    new PostTag {
        PostId = 79,
        TagId = 7
    },
    new PostTag {
        PostId = 46,
        TagId = 7
    },
    new PostTag {
        PostId = 46,
        TagId = 5
    },
    new PostTag {
        PostId = 40,
        TagId = 11
    },
    new PostTag {
        PostId = 34,
        TagId = 15
    },
    new PostTag {
        PostId = 4,
        TagId = 11
    },
    new PostTag {
        PostId = 32,
        TagId = 16
    },
    new PostTag {
        PostId = 25,
        TagId = 13
    },
    new PostTag {
        PostId = 14,
        TagId = 12
    },
    new PostTag {
        PostId = 64,
        TagId = 13
    },
    new PostTag {
        PostId = 38,
        TagId = 11
    },
    new PostTag {
        PostId = 2,
        TagId = 6
    },
    new PostTag {
        PostId = 63,
        TagId = 3
    },
    new PostTag {
        PostId = 54,
        TagId = 3
    },
    new PostTag {
        PostId = 8,
        TagId = 2
    },
    new PostTag {
        PostId = 33,
        TagId = 12
    },
    new PostTag {
        PostId = 55,
        TagId = 12
    },
    new PostTag {
        PostId = 20,
        TagId = 14
    },
    new PostTag {
        PostId = 21,
        TagId = 21
    },
    new PostTag {
        PostId = 71,
        TagId = 11
    },
    new PostTag {
        PostId = 66,
        TagId = 4
    },
    new PostTag {
        PostId = 15,
        TagId = 18
    },
    new PostTag {
        PostId = 79,
        TagId = 2
    },
    new PostTag {
        PostId = 10,
        TagId = 11
    },
    new PostTag {
        PostId = 75,
        TagId = 16
    },
    new PostTag {
        PostId = 20,
        TagId = 8
    },
    new PostTag {
        PostId = 18,
        TagId = 19
    },
    new PostTag {
        PostId = 71,
        TagId = 20
    },
    new PostTag {
        PostId = 72,
        TagId = 10
    },
    new PostTag {
        PostId = 14,
        TagId = 3
    },
    new PostTag {
        PostId = 28,
        TagId = 3
    },
    new PostTag {
        PostId = 71,
        TagId = 2
    },
    new PostTag {
        PostId = 23,
        TagId = 7
    },
    new PostTag {
        PostId = 59,
        TagId = 20
    },
    new PostTag {
        PostId = 17,
        TagId = 12
    },
    new PostTag {
        PostId = 77,
        TagId = 19
    },
    new PostTag {
        PostId = 67,
        TagId = 11
    },
    new PostTag {
        PostId = 39,
        TagId = 14
    },
    new PostTag {
        PostId = 71,
        TagId = 8
    },
    new PostTag {
        PostId = 78,
        TagId = 15
    },
    new PostTag {
        PostId = 57,
        TagId = 17
    },
    new PostTag {
        PostId = 70,
        TagId = 9
    },
    new PostTag {
        PostId = 55,
        TagId = 18
    },
    new PostTag {
        PostId = 30,
        TagId = 13
    },
    new PostTag {
        PostId = 11,
        TagId = 19
    },
    new PostTag {
        PostId = 20,
        TagId = 13
    },
    new PostTag {
        PostId = 5,
        TagId = 17
    },
    new PostTag {
        PostId = 79,
        TagId = 9
    },
    new PostTag {
        PostId = 78,
        TagId = 19
    },
    new PostTag {
        PostId = 31,
        TagId = 2
    },
    new PostTag {
        PostId = 7,
        TagId = 13
    },
    new PostTag {
        PostId = 3,
        TagId = 16
    },
    new PostTag {
        PostId = 11,
        TagId = 9
    },
    new PostTag {
        PostId = 51,
        TagId = 16
    },
    new PostTag {
        PostId = 48,
        TagId = 19
    },
    new PostTag {
        PostId = 35,
        TagId = 20
    },
    new PostTag {
        PostId = 28,
        TagId = 4
    },
    new PostTag {
        PostId = 12,
        TagId = 11
    },
    new PostTag {
        PostId = 44,
        TagId = 16
    },
    new PostTag {
        PostId = 2,
        TagId = 8
    },
    new PostTag {
        PostId = 19,
        TagId = 8
    },
    new PostTag {
        PostId = 69,
        TagId = 11
    },
    new PostTag {
        PostId = 56,
        TagId = 10
    },
    new PostTag {
        PostId = 31,
        TagId = 17
    },
    new PostTag {
        PostId = 4,
        TagId = 13
    },
    new PostTag {
        PostId = 29,
        TagId = 18
    },
    new PostTag {
        PostId = 64,
        TagId = 20
    },
    new PostTag {
        PostId = 52,
        TagId = 12
    },
    new PostTag {
        PostId = 55,
        TagId = 21
    },
    new PostTag {
        PostId = 29,
        TagId = 19
    },
    new PostTag {
        PostId = 61,
        TagId = 6
    },
    new PostTag {
        PostId = 21,
        TagId = 14
    },
    new PostTag {
        PostId = 25,
        TagId = 5
    },
    new PostTag {
        PostId = 27,
        TagId = 6
    },
    new PostTag {
        PostId = 23,
        TagId = 11
    },
    new PostTag {
        PostId = 42,
        TagId = 4
    },
    new PostTag {
        PostId = 73,
        TagId = 15
    },
    new PostTag {
        PostId = 24,
        TagId = 4
    },
    new PostTag {
        PostId = 46,
        TagId = 6
    },
    new PostTag {
        PostId = 70,
        TagId = 21
    }
};
    }
}