﻿using FMS.Data.Models;
using FMS.Data.Models.QueryParameters;
using System.Collections.Generic;

namespace FMS.Services.Services.Contracts
{
    public interface IRepliesService
    {
        Reply Create(Reply replyToCreate);

        bool Delete(Reply replyToDelete);

        IEnumerable<Reply> FilterBy(ReplyQueryParameters filterParameters, int commentId);

        IEnumerable<Reply> GetAll();

        IEnumerable<Reply> GetAllByCommentId(int commentId);

        IEnumerable<Reply> GetAllByUserId(int userId);

        Reply GetById(int id);

        Reply Update(int replyId, string content);

        public int RepliesCountByCurrentUser();
    }
}
