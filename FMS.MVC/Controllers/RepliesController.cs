﻿using FMS.Data.Utilities.Exceptions;
using FMS.MVC.Models.Replies;
using FMS.Services.DTOs;
using FMS.Services.Mappers;
using FMS.Services.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace FMS.MVC.Controllers
{
    public class RepliesController : Controller
    {
        private readonly ICommentsService commentsService;
        private readonly IRepliesService repliesService;
        private readonly IAuthManager authManager;

        public RepliesController(
            IRepliesService repliesService, 
            IAuthManager authManager, 
            ICommentsService commentsService)
        {
            this.authManager = authManager;
            this.repliesService = repliesService;
            this.commentsService = commentsService;
        }
        // GET: RepliesController
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult All(int commentId)
        {
            var model = new AllRepliesByCommentViewModel();

            var replieDTOs = this.repliesService
                .GetAllByCommentId(commentId)
                .Select(c => c.MapToReplyResponse())
                .ToList();

            foreach (var replyDTO in replieDTOs)
            {
                model.Replies.Add(replyDTO);
            }

            model.CommentId = commentId;

            return View(model);
        }

        // GET: RepliesController/Details/5
        public ActionResult Details(ReplyResponseDTO response)
        {
            return View(response);
        }

        [HttpGet]
        // GET: RepliesController/Create
        public ActionResult Create(int commentId)
        {
            var model = new ReplyInputDTO 
            { 
                CommentId = commentId, 
                PostId = this.commentsService.GetById(commentId).PostId
            };

            return View(model);
        }

        // POST: RepliesController/Create
        [HttpPost]
        public ActionResult Create(int commentId, ReplyInputDTO model)
        {
            model.CommentId = commentId;

            if (!this.ModelState.IsValid)
            {
                return View(model);
            }

            try
            {
                var comment = this.commentsService.GetById(model.CommentId);
                var user = this.authManager.CurrentUser;
                var createdReply = this.repliesService.Create(model.MapToReplyModel(user, comment));
                var response = createdReply.MapToReplyResponse();
                return View("Details", response);
            }
            catch (ApplicationException)
            {
                return this.RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            catch (Exception)
            {
                return this.Redirect("/Error");
            }
        }

        [HttpGet]
        // GET: RepliesController/Edit/5
        public ActionResult Edit(int replyId)
        {
            var currentContent = this.repliesService.GetById(replyId).Content;
            var model = new ReplyInputDTO { ReplyId = replyId, Content = currentContent };
            return View(model);
        }

        // POST: RepliesController/Edit/5
        [HttpPost]
        public ActionResult Edit(int replyId, ReplyInputDTO model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            try
            {
                this.repliesService.Update(replyId, model.Content);
                return this.RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            catch (ApplicationException)
            {
                return this.RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            catch (Exception)
            {
                return this.Redirect("/Error");
            }
        }

        [HttpGet]
        // GET: RepliesController/Delete/5
        public ActionResult Delete(int replyId)
        {
            var model = this.repliesService.GetById(replyId);
            return View(model);
        }

        // POST: RepliesController/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int replyId)
        {
            try
            {
                this.repliesService.Delete(this.repliesService.GetById(replyId));
                return this.RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            catch (ApplicationException)
            {
                return this.RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            catch (Exception)
            {
                return this.Redirect("/Error");
            }
        }
    }
}
