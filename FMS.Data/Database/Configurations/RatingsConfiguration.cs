﻿using FMS.Data.Database.Seeders;
using FMS.Data.Models.CompositeModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FMS.Data.Database.Configurations
{
    public class RatingsConfiguration : IEntityTypeConfiguration<Rating>
    {
        public void Configure(EntityTypeBuilder<Rating> builder)
        {
            //Many-to-many Posts and Users relation
            builder.HasKey(rt => new { rt.PostId, rt.UserId });
            builder.HasData(RatingsSeeder.SeedData);
        }
    }
}
