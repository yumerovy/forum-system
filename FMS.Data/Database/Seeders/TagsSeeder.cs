﻿using FMS.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace FMS.Data.Database.Seeders
{
    internal static class TagsSeeder
    {
        internal static Tag[] SeedData => new Tag[] {
    new Tag {
        Id = 1,
        Name = "stairs"
    },
    new Tag {
        Id = 2,
        Name = "house"
    },
    new Tag {
        Id = 3,
        Name = "sell"
    },
    new Tag {
        Id = 4,
        Name = "convertible"
    },
    new Tag {
        Id = 5,
        Name = "expensive"
    },
    new Tag {
        Id = 6,
        Name = "roof"
    },
    new Tag {
        Id = 7,
        Name = "two-stories"
    },
    new Tag {
        Id = 8,
        Name = "cheap"
    },
    new Tag {
        Id = 9,
        Name = "wooden"
    },
    new Tag {
        Id = 10,
        Name = "countryside"
    },
    new Tag {
        Id = 11,
        Name = "free"
    },
    new Tag {
        Id = 12,
        Name = "city"
    },
    new Tag {
        Id = 13,
        Name = "someothertag"
    },
    new Tag {
        Id = 14,
        Name = "imagine"
    },
    new Tag {
        Id = 15,
        Name = "staircase"
    },
    new Tag {
        Id = 16,
        Name = "uniform"
    },
    new Tag {
        Id = 17,
        Name = "creative"
    },
    new Tag {
        Id = 18,
        Name = "high"
    },
    new Tag {
        Id = 19,
        Name = "tent"
    },
    new Tag {
        Id = 20,
        Name = "rent"
    },
    new Tag {
        Id = 21,
        Name = "beautiful"
    }
};
    }
}
