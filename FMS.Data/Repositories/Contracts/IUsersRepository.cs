﻿using FMS.Data.Models;
using FMS.Data.Models.QueryParameters;
using System.Collections.Generic;

namespace FMS.Data.Repositories.Contracts
{
    public interface IUsersRepository
    {
        List<User> GetAll();
        User GetById(int id);
        User GetByUsername(string username);
        User GetByEmail(string email);
        User Create(User user);
        User Update(int id, User user);
        string Delete(int id);
        public bool IsUsernameUnique(string username);
        public bool IsEmailUnique(string email);

        void Logout(int userId);
        public IEnumerable<User> FilterBy(UserQueryParameters filterParameter);
    }
}