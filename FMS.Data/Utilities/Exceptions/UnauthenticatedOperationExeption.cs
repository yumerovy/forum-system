﻿using System;

namespace FMS.Data.Utilities.Exceptions
{
    public class UnauthenticatedOperationException : ApplicationException
    {
        public UnauthenticatedOperationException(string message)
            : base(message)
        {

        }
    }
}