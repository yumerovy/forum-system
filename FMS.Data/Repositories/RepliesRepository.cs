﻿using FMS.Data.Database;
using FMS.Data.Models;
using FMS.Data.Models.QueryParameters;
using FMS.Data.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FMS.Data.Repositories
{
    public class RepliesRepository : IRepliesRepository
    {
        private readonly FMSDbContext context;

        public RepliesRepository(FMSDbContext context)
        {
            this.context = context;
        }

        public Reply Create(Reply replyToCreate)
        {
            this.context.Replies.Add(replyToCreate);
            replyToCreate.CreatedOn = DateTime.Now;
            context.SaveChanges();
            return replyToCreate;
        }

        public bool Delete(Reply replyToDelete)
        {
            replyToDelete.isDeleted = true;
            context.SaveChanges();

            return replyToDelete.isDeleted;
        }

        public IEnumerable<Reply> FilterBy(
            ReplyQueryParameters filterParameters,
            int commentId = default
        )
        {
            if (commentId == default)
            {
                return this.GetAll();
            }

            return this.GetAllByCommentId(commentId);
        }

        public IEnumerable<Reply> GetAll()
        {
            //TODO...
            // need to be sure how exactly to get the other not deleted entities.
            // Once POSTS and USERS are all set up, this part of the code will need to be revised


            return this.context.Comments
                .Where(c => !c.isDeleted)
                    .Include(c => c.Replies
                        .Where(r => !r.isDeleted))
                .SelectMany(cr => cr.Replies)
                    .Include(r => r.User)
                        .ThenInclude(u => u.UserPosts
                            .Where(p => !p.isDeleted))
                .ToList();
        }

        public IEnumerable<Reply> GetAllByCommentId(int commentId)
        {
            //TODO...
            // need to be sure how exactly to get the other not deleted entities.
            // Once POSTS and USERS are all set up, this part of the code will need to be revised

            return this.GetAll().Where(r => r.CommentId == commentId);
        }

        public IEnumerable<Reply> GetAllByUserId(int userId)
        {
            //TODO...
            // need to be sure how exactly to get the other not deleted entities.
            // Once POSTS and USERS are all set up, this part of the code will need to be revised

            return this.GetAll().Where(r => r.UserId == userId);
        }

        public Reply GetById(int id)
        {
            //TODO...
            // need to be sure how exactly to get the other not deleted entities.
            // Once POSTS and USERS are all set up, this part of the code will need to be revised

            return this.GetAll().FirstOrDefault(r => r.Id == id);
        }

        public Reply Update(Reply replyToUpdate)
        {
            this.context.SaveChanges();
            return replyToUpdate;
        }
    }
}
