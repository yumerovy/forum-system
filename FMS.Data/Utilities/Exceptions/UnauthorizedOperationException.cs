﻿using System;

namespace FMS.Data.Utilities.Exceptions
{
    public class UnauthorizedOperationException : ApplicationException
    {
        public UnauthorizedOperationException(string message) 
            : base(message)
        {
        }
    }
}
