﻿using FMS.Data.Models;
using FMS.Services.DTOs;

namespace FMS.Services.Mappers
{
    public static class CommentsModelMapper
    {
        public static Comment MapToCommentModel(
            this CommentInputDTO commentCreateDTO,
            User user,
            Post post)
        {
            return new Comment
            {
                Content = commentCreateDTO.Content,
                PostId = post.Id,
                UserId = user.Id,
                User = user,
                Post = post
            };
        }

        public static CommentResponseDTO MapToCommentResponse(this Comment comment)
        {
            return new CommentResponseDTO
            {
                Id = comment.Id,
                Author = comment.User.Username,
                Content = comment.Content,
                CreatedOn =
                $"{comment.CreatedOn.ToShortDateString()}" +
                $" at {comment.CreatedOn.ToShortTimeString()}",

                PostId = comment.PostId,
                PostTitle = comment.Post.Title,
                PostCreatedBy = comment.Post.User.Username,
                PostCreatedOn =
                $"{comment.Post.CreatedOn.ToShortDateString()}" +
                $" at {comment.Post.CreatedOn.ToShortTimeString()}",
            };
        }
    }
}
