﻿using FMS.Services.Services.Contracts;
using FMS.Services.Mappers;
using Microsoft.AspNetCore.Mvc;
using FMS.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FMS.Data.Models.QueryParameters;
using FMS.Services.DTOs;
using FMS.Data.Utilities.Exceptions;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FMS.Web.Controllers
{
    [Route("api/posts")]
    [ApiController]
    public class PostsApiController : ControllerBase
    {
        private readonly IAuthManager authManager;

        private readonly IPostsService postsService;

        public PostsApiController(IAuthManager authManager, IPostsService postsService)
        {
            this.authManager = authManager;
            this.postsService = postsService;
        }

        //ToDo split exceptiontypes with different status codes

        [HttpGet]
        public IActionResult Get([FromQuery] PostQueryParameters filterParams)
        {
            try
            {
                if (FilterParamsChecher(filterParams))
                    return Ok(this.postsService.GetAll().MapToPostDTOList());

                var result = this.postsService.FilterBy(filterParams).MapToPostDTOList();

                if (result.Count == 0)
                    return NotFound("There are no posts with the given critera");

                else return Ok(result);


            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }

        }


        [HttpGet("{postId}")]
        public IActionResult GetById(int postId)
        {
            try
            {
                return this.Ok(this.postsService.GetById(postId).MapToPostDTO());
            }
            catch (EntityNotFoundException ex)
            {
                return this.NotFound(ex.Message);
            }
        }

        // think of a way to implement 2 top10-s in the filters
        // ?!?!?!? query parameters or both 2( or more) searches in one request

        [HttpGet("top-ten-commented")]
        public IActionResult GetTopTenCommented()
        {
            return Ok(this.postsService.GetTopTenCommented());
        }

        [HttpGet("top-ten-recent")]
        public IActionResult GetTopTenRecent()
        {
            return Ok(this.postsService.GetTopTenRecent());
        }

        [HttpPost]
        public IActionResult Create([FromBody] PostInDTO incomigPost,
            [FromHeader] string credentials)
        {
            var splittedCredentials = credentials.Split(":");

            try
            {
                var user = authManager.Authenticate(splittedCredentials[0], splittedCredentials[1]);

                return Ok(this.postsService.Create(incomigPost.MapToPost(user)));
            }

            catch (UnauthorizedOperationException onaqtam)
            {
                return this.Unauthorized(onaqtam.Message);
            }

            catch (Exception to)
            {
                return this.BadRequest(to.Message);
            }
        }

        [HttpPut("{postId}")]
        public IActionResult Update(int postId,
            [FromBody] PostInDTO incomingPost,
            [FromHeader] string credentials)
        {
            var splittedCredentials = credentials.Split(":");

            try
            {
                var user = authManager.Authenticate(splittedCredentials[0], splittedCredentials[1]);

                var post = this.postsService.Update(postId, incomingPost.MapToPost(user));

                return Ok(post.MapToPostDTO());
            }

            catch (UnauthorizedOperationException i_t_)
            {
                return this.Unauthorized(i_t_.Message);
            }

            catch (Exception onaqtam)
            {
                return BadRequest(onaqtam.Message);
            }

        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id, [FromBody] UserDTO user)
        {
            try
            {
                var result = this.postsService.Delete(id, user.MapToUser());
                return this.Ok(result);
            }
            catch (Exception onqtam)
            {
                return this.NotFound(onqtam.Message);
            }
        }

        private static bool FilterParamsChecher(PostQueryParameters filterParams)
        {
            if (filterParams.Search == null && filterParams.SortBy == null &&
                    filterParams.SortOrder == null && filterParams.UserName == null)
                return true;

            else return false;
        }
    }
}
