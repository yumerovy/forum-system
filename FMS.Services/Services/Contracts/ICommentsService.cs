﻿using FMS.Data.Models;
using FMS.Data.Models.QueryParameters;
using System.Collections.Generic;

namespace FMS.Services.Services.Contracts
{
    public interface ICommentsService
    {
        Comment Create(Comment commentToCreate);

        bool Delete(Comment commentToDelete);

        IEnumerable<Comment> FilterBy(CommentQueryParameters filterParameters, int postId);

        IEnumerable<Comment> GetAll();

        IEnumerable<Comment> GetAllByPostId(int postId);

        IEnumerable<Comment> GetAllByUserId(int userId);

        Comment GetById(int id);

        Comment Update(int commentId, string content);

        public int CommentCountByCurrentUser();
    }
}
