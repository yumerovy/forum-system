﻿using FMS.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace FMS.Data.Database.Seeders
{
    internal static class CommentsSeeder
    {
        internal static Comment[] SeedData => new Comment[] {
    new Comment {
        Id = 1,
        PostId = 31,
        Content = "at, iaculis quis, pede. Praesent eu dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean eget magna. Suspendisse tristique neque",
        UserId = 75,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 2,
        PostId = 16,
        Content = "metus. In lorem. Donec elementum, lorem ut aliquam iaculis, lacus pede sagittis augue, eu tempor erat neque non quam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam fringilla cursus purus. Nullam",
        UserId = 65,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 3,
        PostId = 24,
        Content = "a, dui. Cras pellentesque. Sed dictum. Proin eget odio. Aliquam vulputate ullamcorper magna. Sed eu eros. Nam consequat dolor vitae dolor. Donec fringilla. Donec feugiat metus sit amet ante. Vivamus non lorem vitae odio sagittis semper. Nam",
        UserId = 49,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 4,
        PostId = 43,
        Content = "et risus. Quisque libero lacus, varius et, euismod et, commodo at,",
        UserId = 13,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 5,
        PostId = 48,
        Content = "Sed malesuada augue ut lacus. Nulla tincidunt, neque vitae semper egestas, urna justo faucibus lectus, a sollicitudin orci sem eget massa. Suspendisse eleifend. Cras sed leo. Cras vehicula aliquet libero. Integer",
        UserId = 48,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 6,
        PostId = 37,
        Content = "fermentum risus, at fringilla purus mauris a nunc. In at pede. Cras vulputate velit eu sem. Pellentesque ut ipsum ac mi eleifend egestas.",
        UserId = 69,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 7,
        PostId = 41,
        Content = "Integer vulputate, risus a ultricies adipiscing, enim mi tempor lorem, eget mollis lectus pede et risus. Quisque",
        UserId = 27,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 8,
        PostId = 52,
        Content = "suscipit, est ac facilisis facilisis, magna",
        UserId = 25,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 9,
        PostId = 18,
        Content = "Aliquam ornare, libero at auctor ullamcorper, nisl arcu iaculis enim, sit amet ornare lectus justo eu arcu. Morbi sit amet massa. Quisque porttitor eros nec tellus. Nunc lectus pede, ultrices a, auctor non, feugiat nec, diam.",
        UserId = 57,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 10,
        PostId = 35,
        Content = "Integer sem elit, pharetra ut, pharetra sed, hendrerit a, arcu. Sed et libero. Proin mi. Aliquam gravida mauris ut mi. Duis risus odio, auctor vitae, aliquet nec, imperdiet",
        UserId = 25,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 11,
        PostId = 65,
        Content = "Donec tempus, lorem fringilla ornare placerat,",
        UserId = 6,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 12,
        PostId = 65,
        Content = "senectus",
        UserId = 74,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 13,
        PostId = 67,
        Content = "ante dictum mi, ac mattis velit justo nec ante. Maecenas mi felis, adipiscing fringilla, porttitor vulputate, posuere vulputate, lacus. Cras interdum. Nunc sollicitudin commodo ipsum. Suspendisse non leo. Vivamus nibh dolor, nonummy",
        UserId = 46,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 14,
        PostId = 39,
        Content = "ac tellus. Suspendisse sed dolor. Fusce mi lorem, vehicula et, rutrum eu, ultrices sit amet, risus. Donec nibh enim, gravida sit amet, dapibus id, blandit at, nisi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin",
        UserId = 59,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 15,
        PostId = 34,
        Content = "molestie orci tincidunt adipiscing. Mauris molestie pharetra nibh. Aliquam ornare, libero at auctor ullamcorper, nisl arcu iaculis enim, sit amet ornare lectus justo eu arcu. Morbi sit amet massa. Quisque porttitor eros nec tellus. Nunc lectus",
        UserId = 22,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 16,
        PostId = 13,
        Content = "luctus et ultrices posuere cubilia Curae Phasellus ornare. Fusce mollis. Duis sit amet diam eu dolor",
        UserId = 6,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 17,
        PostId = 17,
        Content = "turpis vitae purus gravida sagittis. Duis gravida. Praesent eu nulla at sem molestie sodales. Mauris blandit enim consequat purus. Maecenas libero est, congue a, aliquet vel, vulputate eu, odio. Phasellus at augue id ante dictum",
        UserId = 41,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 18,
        PostId = 9,
        Content = "Ut sagittis lobortis mauris. Suspendisse",
        UserId = 53,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 19,
        PostId = 8,
        Content = "dignissim magna a tortor. Nunc commodo auctor velit. Aliquam nisl. Nulla eu neque pellentesque massa lobortis ultrices. Vivamus rhoncus. Donec est. Nunc ullamcorper, velit in aliquet lobortis, nisi nibh lacinia orci, consectetuer euismod est arcu ac orci. Ut semper pretium neque.",
        UserId = 79,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 20,
        PostId = 66,
        Content = "eu metus. In lorem.",
        UserId = 60,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 21,
        PostId = 28,
        Content = "sed leo. Cras vehicula aliquet libero. Integer in magna. Phasellus dolor elit, pellentesque a, facilisis non, bibendum sed, est. Nunc laoreet lectus quis massa. Mauris vestibulum, neque sed dictum",
        UserId = 39,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 22,
        PostId = 57,
        Content = "eleifend nec, malesuada ut, sem. Nulla interdum. Curabitur dictum. Phasellus in felis. Nulla tempor augue ac ipsum. Phasellus vitae mauris sit amet lorem semper auctor. Mauris vel turpis. Aliquam adipiscing lobortis risus. In mi pede, nonummy ut, molestie in, tempus eu, ligula. Aenean",
        UserId = 43,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 23,
        PostId = 12,
        Content = "augue id ante dictum cursus. Nunc mauris elit, dictum eu, eleifend nec, malesuada ut, sem. Nulla interdum.",
        UserId = 56,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 24,
        PostId = 10,
        Content = "sagittis augue, eu tempor erat neque non quam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam fringilla cursus purus. Nullam scelerisque neque sed sem egestas blandit. Nam nulla magna, malesuada vel, convallis in,",
        UserId = 3,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 25,
        PostId = 63,
        Content = "mollis. Duis sit amet diam eu dolor egestas rhoncus. Proin nisl sem, consequat nec, mollis vitae, posuere at, velit. Cras lorem lorem, luctus ut, pellentesque eget, dictum placerat, augue. Sed molestie. Sed id risus quis diam luctus lobortis. Class aptent taciti sociosqu ad litora torquent per",
        UserId = 13,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 26,
        PostId = 55,
        Content = "augue, eu tempor erat neque non quam.",
        UserId = 17,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 27,
        PostId = 57,
        Content = "dolor dolor, tempus non, lacinia at, iaculis quis, pede. Praesent eu dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean eget",
        UserId = 23,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 28,
        PostId = 16,
        Content = "parturient montes, nascetur ridiculus mus. Proin vel nisl. Quisque fringilla euismod enim. Etiam",
        UserId = 1,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 29,
        PostId = 16,
        Content = "luctus felis purus ac tellus. Suspendisse sed dolor. Fusce mi lorem, vehicula et, rutrum eu, ultrices sit amet, risus. Donec nibh enim, gravida sit amet, dapibus id, blandit at, nisi. Cum sociis natoque penatibus et magnis dis",
        UserId = 5,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 30,
        PostId = 39,
        Content = "sit amet ultricies sem magna nec quam. Curabitur vel lectus. Cum sociis natoque penatibus",
        UserId = 76,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 31,
        PostId = 35,
        Content = "a neque. Nullam ut nisi a odio semper cursus. Integer mollis. Integer tincidunt aliquam arcu. Aliquam ultrices iaculis odio. Nam interdum enim non nisi. Aenean eget metus. In nec orci. Donec nibh. Quisque nonummy ipsum non arcu. Vivamus sit amet",
        UserId = 50,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 32,
        PostId = 70,
        Content = "suscipit, est ac facilisis facilisis, magna tellus faucibus leo, in lobortis tellus justo sit amet nulla. Donec non justo. Proin non massa non ante bibendum ullamcorper. Duis cursus, diam at pretium aliquet, metus urna convallis erat, eget tincidunt dui",
        UserId = 23,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 33,
        PostId = 58,
        Content = "sit amet, faucibus ut, nulla. Cras eu tellus eu augue porttitor interdum. Sed auctor odio a",
        UserId = 9,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 34,
        PostId = 25,
        Content = "neque. Nullam ut nisi a odio semper cursus. Integer mollis. Integer tincidunt",
        UserId = 61,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 35,
        PostId = 7,
        Content = "Proin nisl sem, consequat nec, mollis vitae, posuere at, velit. Cras lorem lorem, luctus ut, pellentesque eget, dictum placerat, augue. Sed molestie. Sed id risus quis diam luctus lobortis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Mauris ut quam vel sapien imperdiet",
        UserId = 65,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 36,
        PostId = 64,
        Content = "justo eu arcu. Morbi sit amet massa. Quisque porttitor eros nec tellus. Nunc lectus pede, ultrices a, auctor non, feugiat nec, diam. Duis mi enim, condimentum eget, volutpat ornare, facilisis eget, ipsum. Donec sollicitudin adipiscing ligula. Aenean gravida nunc sed pede. Cum sociis",
        UserId = 52,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 37,
        PostId = 25,
        Content = "elit, pellentesque a, facilisis non, bibendum sed, est. Nunc laoreet lectus quis massa. Mauris vestibulum, neque sed dictum eleifend, nunc risus varius orci, in consequat",
        UserId = 76,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 38,
        PostId = 58,
        Content = "mattis ornare, lectus ante dictum mi, ac mattis velit justo nec ante. Maecenas mi felis, adipiscing fringilla, porttitor vulputate, posuere vulputate, lacus. Cras interdum. Nunc sollicitudin",
        UserId = 64,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 39,
        PostId = 43,
        Content = "Donec luctus aliquet odio. Etiam ligula tortor, dictum eu, placerat eget, venenatis a, magna. Lorem ipsum dolor sit",
        UserId = 70,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 40,
        PostId = 29,
        Content = "tincidunt aliquam arcu. Aliquam ultrices iaculis odio. Nam interdum enim non nisi. Aenean eget metus. In",
        UserId = 15,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 41,
        PostId = 6,
        Content = "libero et tristique pellentesque, tellus sem mollis",
        UserId = 61,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 42,
        PostId = 73,
        Content = "lorem eu metus. In",
        UserId = 14,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 43,
        PostId = 39,
        Content = "rutrum urna, nec luctus felis purus ac tellus. Suspendisse sed dolor. Fusce mi lorem, vehicula et, rutrum eu, ultrices sit amet, risus. Donec nibh enim, gravida sit amet, dapibus id, blandit at, nisi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
        UserId = 11,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 44,
        PostId = 52,
        Content = "enim non nisi. Aenean eget metus. In nec orci. Donec nibh.",
        UserId = 78,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 45,
        PostId = 47,
        Content = "magna. Phasellus dolor elit, pellentesque a, facilisis non, bibendum sed, est. Nunc laoreet lectus quis massa. Mauris vestibulum, neque sed dictum eleifend, nunc risus varius orci, in consequat enim diam vel arcu. Curabitur ut odio vel est",
        UserId = 34,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 46,
        PostId = 77,
        Content = "nulla. Donec non justo. Proin non massa non ante bibendum ullamcorper. Duis cursus, diam at pretium aliquet, metus urna convallis erat, eget tincidunt dui augue eu tellus. Phasellus elit pede, malesuada vel,",
        UserId = 71,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 47,
        PostId = 29,
        Content = "Morbi accumsan laoreet ipsum. Curabitur consequat, lectus",
        UserId = 21,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 48,
        PostId = 15,
        Content = "purus, accumsan",
        UserId = 29,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 49,
        PostId = 58,
        Content = "Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc",
        UserId = 13,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 50,
        PostId = 3,
        Content = "a, dui. Cras pellentesque. Sed dictum. Proin eget odio. Aliquam vulputate ullamcorper magna. Sed eu eros. Nam consequat dolor vitae dolor. Donec fringilla. Donec feugiat metus sit amet ante. Vivamus non lorem vitae odio sagittis semper. Nam tempor diam dictum sapien. Aenean massa. Integer vitae nibh. Donec",
        UserId = 55,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 51,
        PostId = 41,
        Content = "mauris elit, dictum eu, eleifend nec, malesuada ut, sem. Nulla interdum. Curabitur dictum. Phasellus in felis. Nulla tempor augue ac ipsum. Phasellus vitae mauris sit amet lorem semper auctor. Mauris",
        UserId = 51,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 52,
        PostId = 75,
        Content = "ullamcorper viverra. Maecenas iaculis aliquet diam. Sed diam lorem, auctor quis, tristique ac, eleifend vitae, erat. Vivamus nisi. Mauris nulla. Integer urna. Vivamus molestie dapibus ligula. Aliquam erat volutpat. Nulla dignissim. Maecenas ornare egestas ligula. Nullam feugiat placerat velit. Quisque varius. Nam porttitor scelerisque neque. Nullam",
        UserId = 62,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 53,
        PostId = 10,
        Content = "Proin mi. Aliquam gravida mauris ut mi. Duis risus odio, auctor vitae, aliquet nec, imperdiet nec, leo. Morbi neque tellus, imperdiet",
        UserId = 77,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 54,
        PostId = 18,
        Content = "Fusce mollis. Duis sit amet diam eu dolor egestas rhoncus. Proin nisl sem,",
        UserId = 34,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 55,
        PostId = 3,
        Content = "faucibus",
        UserId = 3,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 56,
        PostId = 33,
        Content = "Sed neque. Sed eget lacus. Mauris non dui nec urna suscipit nonummy. Fusce fermentum fermentum arcu.",
        UserId = 35,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 57,
        PostId = 45,
        Content = "turpis vitae",
        UserId = 72,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 58,
        PostId = 70,
        Content = "at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis diam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames",
        UserId = 8,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 59,
        PostId = 57,
        Content = "id nunc interdum feugiat. Sed nec metus facilisis lorem tristique aliquet. Phasellus fermentum convallis ligula. Donec luctus aliquet odio. Etiam ligula tortor, dictum eu, placerat eget, venenatis a, magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Etiam laoreet, libero et tristique pellentesque, tellus sem mollis",
        UserId = 66,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 60,
        PostId = 44,
        Content = "id magna et ipsum cursus vestibulum. Mauris magna. Duis dignissim tempor arcu. Vestibulum ut eros non enim commodo hendrerit. Donec porttitor tellus non magna. Nam ligula elit, pretium et, rutrum non, hendrerit id, ante. Nunc mauris sapien, cursus in,",
        UserId = 31,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 61,
        PostId = 75,
        Content = "quis, tristique ac, eleifend vitae, erat. Vivamus nisi. Mauris nulla. Integer urna. Vivamus molestie dapibus ligula. Aliquam erat volutpat. Nulla dignissim. Maecenas ornare egestas ligula. Nullam feugiat placerat velit. Quisque varius. Nam porttitor scelerisque neque. Nullam nisl. Maecenas malesuada fringilla est. Mauris eu turpis. Nulla aliquet. Proin velit.",
        UserId = 19,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 62,
        PostId = 20,
        Content = "torquent per",
        UserId = 63,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 63,
        PostId = 60,
        Content = "sem molestie sodales. Mauris blandit enim consequat purus. Maecenas libero est, congue a, aliquet vel, vulputate eu, odio. Phasellus at augue id ante dictum cursus. Nunc mauris elit, dictum eu, eleifend nec, malesuada ut, sem. Nulla interdum. Curabitur",
        UserId = 27,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 64,
        PostId = 68,
        Content = "mi tempor lorem, eget mollis lectus pede et risus. Quisque libero lacus, varius et, euismod et, commodo at, libero. Morbi accumsan laoreet ipsum. Curabitur consequat, lectus sit amet luctus vulputate, nisi sem semper erat, in consectetuer ipsum nunc id enim. Curabitur massa. Vestibulum accumsan neque et",
        UserId = 14,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 65,
        PostId = 63,
        Content = "convallis est, vitae sodales nisi magna sed dui. Fusce aliquam, enim nec tempus scelerisque, lorem ipsum sodales purus, in molestie tortor nibh sit amet orci. Ut sagittis lobortis mauris. Suspendisse aliquet molestie tellus. Aenean egestas hendrerit neque. In ornare sagittis felis.",
        UserId = 22,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 66,
        PostId = 43,
        Content = "mus. Donec dignissim magna a tortor. Nunc commodo auctor velit. Aliquam nisl. Nulla eu neque pellentesque massa lobortis ultrices. Vivamus rhoncus. Donec est. Nunc ullamcorper,",
        UserId = 26,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 67,
        PostId = 72,
        Content = "Maecenas malesuada fringilla est. Mauris eu turpis. Nulla aliquet. Proin velit. Sed malesuada augue ut lacus. Nulla tincidunt, neque vitae semper egestas, urna justo faucibus lectus, a sollicitudin orci sem eget massa. Suspendisse eleifend. Cras sed leo. Cras vehicula aliquet libero. Integer in magna. Phasellus dolor",
        UserId = 58,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 68,
        PostId = 58,
        Content = "at, velit. Cras lorem lorem, luctus ut, pellentesque eget, dictum placerat, augue. Sed molestie. Sed id risus quis diam luctus lobortis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Mauris ut quam vel sapien imperdiet ornare. In faucibus. Morbi vehicula. Pellentesque",
        UserId = 10,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 69,
        PostId = 24,
        Content = "Donec tincidunt. Donec vitae erat vel pede blandit congue. In scelerisque scelerisque dui. Suspendisse ac metus vitae velit",
        UserId = 36,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 70,
        PostId = 64,
        Content = "ridiculus mus. Aenean eget magna. Suspendisse tristique neque venenatis lacus. Etiam bibendum fermentum metus. Aenean sed pede nec ante blandit viverra. Donec tempus, lorem fringilla ornare placerat, orci lacus vestibulum lorem, sit amet ultricies sem magna nec quam. Curabitur vel lectus. Cum sociis natoque penatibus et magnis",
        UserId = 18,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 71,
        PostId = 64,
        Content = "ac turpis egestas. Fusce aliquet magna a neque. Nullam ut nisi a odio semper cursus. Integer mollis. Integer tincidunt aliquam arcu. Aliquam ultrices iaculis odio. Nam interdum enim non nisi. Aenean eget metus. In nec orci. Donec nibh.",
        UserId = 6,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 72,
        PostId = 14,
        Content = "sodales at, velit. Pellentesque ultricies dignissim lacus. Aliquam rutrum lorem ac risus. Morbi metus. Vivamus euismod urna. Nullam lobortis quam a felis ullamcorper viverra. Maecenas iaculis aliquet diam. Sed diam",
        UserId = 22,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 73,
        PostId = 25,
        Content = "Curabitur consequat, lectus sit amet luctus vulputate, nisi sem semper erat, in",
        UserId = 28,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 74,
        PostId = 45,
        Content = "montes, nascetur ridiculus mus. Aenean eget",
        UserId = 72,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 75,
        PostId = 68,
        Content = "vel nisl. Quisque fringilla euismod enim.",
        UserId = 6,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 76,
        PostId = 3,
        Content = "mattis semper, dui lectus rutrum urna, nec luctus felis purus ac tellus. Suspendisse sed dolor. Fusce mi lorem, vehicula et, rutrum eu, ultrices sit amet,",
        UserId = 38,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 77,
        PostId = 17,
        Content = "morbi tristique senectus",
        UserId = 10,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 78,
        PostId = 40,
        Content = "a odio semper cursus. Integer mollis. Integer tincidunt aliquam arcu. Aliquam ultrices iaculis odio. Nam interdum enim non nisi. Aenean eget metus. In nec orci. Donec nibh. Quisque nonummy ipsum non arcu. Vivamus sit amet risus. Donec egestas. Aliquam nec enim. Nunc ut erat. Sed nunc",
        UserId = 11,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 79,
        PostId = 23,
        Content = "Aliquam erat volutpat. Nulla dignissim. Maecenas ornare egestas ligula. Nullam feugiat placerat velit. Quisque varius. Nam porttitor scelerisque neque. Nullam nisl. Maecenas malesuada fringilla est. Mauris eu turpis. Nulla aliquet.",
        UserId = 41,
        CreatedOn = DateTime.Now
    },
    new Comment {
        Id = 80,
        PostId = 17,
        Content = "eu, eleifend nec, malesuada ut, sem. Nulla interdum. Curabitur dictum. Phasellus in",
        UserId = 13,
        CreatedOn = DateTime.Now
    }
};

    }
}
