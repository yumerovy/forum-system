﻿using FMS.Services.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FMS.Web.Controllers
{
    [Route("api/tags")]
    [ApiController]
    public class TagsApiController : ControllerBase
    {

        private readonly ITagsService tagsService;
        public TagsApiController(ITagsService tagsService)
        {
            this.tagsService = tagsService;
        }

        [HttpGet("")]
        public IActionResult GetTags()
        {
            var tags = this.tagsService.GetAll();

            if (tags.Count() == 0)
            {
                return this.Accepted("There are no tags int the database");
            }

            return this.Ok(tags);
        }

        [HttpGet("{name}")]
        public IActionResult GetByName(string name)
        {
            var tag = this.tagsService.GetByName(name);
            if (tag == null)
            {
                return this.NotFound($"No tag with name {name} was found");
            }

            return this.Ok(tag.Name);
        }

        [HttpGet("api/posts/{id}")]
        public IActionResult GetTagsByPostId(int postId)
        {
            throw new NotImplementedException();
            // get posts service and all the tags for the given postId
        }

        [HttpPost("api/posts/{id}")]
        public void CreateTag(int postId, [FromBody] string tagName)
        {
            if (!this.tagsService.Exists(tagName))
            {
                // check if the tag exists and go on from there
            }
            throw new NotImplementedException();

            //else create the tag, add it to the list of tags
            //and attach it to the postId.
        }

    }
}
