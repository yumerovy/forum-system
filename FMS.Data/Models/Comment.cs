﻿using FMS.Data.Utilities.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FMS.Data.Models
{
    public class Comment : ICreationInfo, IDeletableEntity
    {
        public int Id { get; set; }

        [Required]
        [MinLength(4, ErrorMessage = "A {0} cannot be less than {1} symbols")]
        [MaxLength(5000, ErrorMessage = "A {0} cannot be more than {1} symbols")]
        public string Content { get; set; }

        public int PostId { get; set; }

        public Post Post { get; set; }

        public int UserId { get; set; }

        public User User { get; set; }

        public bool isDeleted { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public List<Reply> Replies { get; set; } = new List<Reply>();
    }
}
