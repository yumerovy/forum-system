﻿using FMS.Services.DTOs;
using FMS.Services.Services.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace FMS.Web.Controllers
{
    [ApiController]
    [Route("api/login")]
    public class LogInController : ControllerBase
    {
        private readonly IAuthManager authManager;

        public LogInController(IAuthManager authManager)
        {
            this.authManager = authManager;
        }

        [HttpPost]
        public IActionResult LogIn(LogInDTO logInDTO)
        {
            //Redirect
            return this.Ok(this.authManager.Authenticate(logInDTO.Username, logInDTO.Password));
        }

    }
}
