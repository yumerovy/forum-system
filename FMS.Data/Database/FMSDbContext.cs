﻿using FMS.Data.Database.Configurations;
using FMS.Data.Models;
using FMS.Data.Models.CompositeModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using ZendeskApi_v2.Models.Constants;
using System.Reflection;

namespace FMS.Data.Database
{
    public class FMSDbContext : DbContext
    {

        public FMSDbContext(DbContextOptions<FMSDbContext> options)
            : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Reply> Replies { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<PostTag> PostTags { get; set; }
        public DbSet<Rating> Votes { get; set; }



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UsersConfiguration());
            modelBuilder.ApplyConfiguration(new PostsConfiguration());
            modelBuilder.ApplyConfiguration(new CommentsConfiguration());
            modelBuilder.ApplyConfiguration(new RepliesConfiguration());
            modelBuilder.ApplyConfiguration(new TagsConfiguration());    
            modelBuilder.ApplyConfiguration(new PostTagConfiguration());
            modelBuilder.ApplyConfiguration(new RatingsConfiguration());

            base.OnModelCreating(modelBuilder);
        }

    }
}
