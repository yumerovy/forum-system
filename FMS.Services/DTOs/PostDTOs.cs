﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection.Metadata;
using System.Threading.Tasks;

using Microsoft.Extensions.Hosting;
using FMS.Data.Models;
using FMS.Data.Models.CompositeModels;

namespace FMS.Services.DTOs
{
    public class PostInDTO
    {

        //o The title must be between 16 and 64 symbols.
        [Required]
        [StringLength(64, MinimumLength = 16, ErrorMessage = "The length of {0} must be between {1} and {2}.")]
        public string Title;

        //o The content must be between 32 symbols and 8192 symbols.
        [Required]
        [StringLength(8192, MinimumLength = 32, ErrorMessage = "The length of {0} must be between {1} and {2}.")]
        public string Description;

        public List<Tag> Tags { get; set; } =  new List<Tag>();
    }

    public class PostDTO
    {
        public int Id;

        public string Author;

        public int AuthorId;

        public string Title;

        public string Description;

        public DateTime CreatedOn;

        public IEnumerable<Comment> PostComments { get; set; } = new List<Comment>();

        public IEnumerable<Rating> Votes { get; set; } = new List<Rating>();
    }
}
