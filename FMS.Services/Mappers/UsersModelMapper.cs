﻿using FMS.Data.Models;
using FMS.Services.DTOs;

namespace FMS.Services.Mappers
{
    public static class UsersModelMapper
    {
         public static User MapToUser(this UserDTO userDTO)
         {
             return new User
             {
                 Id = userDTO.Id,
                 Username = userDTO.Username,
                 Password = userDTO.Password,
                 Role = userDTO.Role,
                 Email = userDTO.Email,
                 FirstName = userDTO.FirstName,
                 LastName = userDTO.LastName,
                 PhoneNumber = userDTO.PhoneNumber,
                 Photo = userDTO.Photo

             };
         }
    }
}
