﻿using FMS.Data.Models.QueryParameters;
using FMS.Data.Utilities.Exceptions;
using FMS.Services.DTOs;
using FMS.Services.Mappers;
using FMS.Services.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace FMS.Web.Controllers
{
    [Route("api/posts/{postId}/comments/{commentId}/replies")]
    [ApiController]
    public class RepliesApiController : ControllerBase
    {
        private readonly IAuthManager authManager;

        private readonly ICommentsService commentsService;

        private readonly IRepliesService repliesService;

        public RepliesApiController(
            IRepliesService repliesService,
            ICommentsService commentsService,
            IAuthManager authManager
        )
        {
            this.repliesService = repliesService;
            this.commentsService = commentsService;
            this.authManager = authManager;
        }

        [HttpPost("")]
        public IActionResult CreateReply(
            int postId,
            int commentId,
            [FromHeader] string credentials,
            [FromBody] ReplyInputDTO createReplyDTO
        )
        {
            var splittedCredentials = credentials.Split(":");

            try
            {
                var user = authManager.Authenticate(splittedCredentials[0], splittedCredentials[1]);
                if (user.IsBlocked)
                {
                    throw new UnauthorizedOperationException(
                        "You are blocked and cannot create replies at the moment."
                            + " Please contact a system administrator!"
                    );
                }

                var comment = this.commentsService.GetById(commentId);
                var reply = this.repliesService.Create(
                    createReplyDTO.MapToReplyModel(user, comment)
                );
                return this.Created(
                    $"localhost:5000/api/posts/{postId}/comments{comment.Id}/replies/{reply.Id}",
                    reply.MapToReplyResponse()
                );
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
            catch (UnauthenticatedOperationException e)
            {
                return this.Forbid(e.Message);
            }
            catch (UnauthorizedOperationException e)
            {
                return this.Unauthorized(e.Message);
            }
        }

        [HttpDelete("{replyId}")]
        public IActionResult Delete(int replyId, [FromHeader] string credentials)
        {
            var splittedCredentials = credentials.Split(":");

            try
            {
                var user = authManager.Authenticate(splittedCredentials[0], splittedCredentials[1]);

                var replyToDelete = this.repliesService.GetById(replyId);

                if (replyToDelete == null)
                {
                    throw new EntityNotFoundException($"Comment with id {replyId} does not exist");
                }

                if (!replyToDelete.UserId.Equals(user.Id))
                {
                    throw new UnauthorizedOperationException(
                        "You are not the author of this reply, hence you cannot delete it."
                    );
                }

                if (this.repliesService.Delete(replyToDelete))
                {
                    return this.Ok($"Comment with id {replyId} was deleted");
                }

                // in case the soft delete operation is unsuccessful, the system will
                //throw an exception. If this is caught, it means there is a bug
                //in the delete operation

                throw new InvalidOperationException("The reply could not be deleted!");
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
            catch (UnauthenticatedOperationException e)
            {
                return this.Forbid(e.Message);
            }
            catch (UnauthorizedOperationException e)
            {
                return this.Unauthorized(e.Message);
            }
        }

        [HttpGet("{replyId}")]
        public IActionResult Get(int replyId)
        {
            var reply = this.repliesService.GetById(replyId);
            if (reply == null)
            {
                return this.NotFound($"Reply with id {replyId} does not exist");
            }

            return this.Ok(reply.MapToReplyResponse());
        }

        [HttpGet("")]
        public IActionResult GetReplies(
            int commentId,
            [FromQuery] ReplyQueryParameters filterParameters
        )
        {
            var replies = this.repliesService.FilterBy(filterParameters, commentId);

            if (replies.Count() == 0)
            {
                return this.NotFound("There are no replies found by the given parameters");
            }

            return this.Ok(replies.Select(r => r.MapToReplyResponse()).ToList());
        }

        [HttpPut("{replyId}")]
        public IActionResult Put(
            int replyId,
            [FromHeader] string credentials,
            [FromBody] ReplyInputDTO replyInputDTO
        )
        {
            var splittedCredentials = credentials.Split(":");

            try
            {
                var user = authManager.Authenticate(splittedCredentials[0], splittedCredentials[1]);

                var replyToUpdate = this.repliesService.GetById(replyId);

                if (replyToUpdate == null)
                {
                    throw new EntityNotFoundException($"Reply with id {replyId} does not exist");
                }

                if (!replyToUpdate.UserId.Equals(user.Id))
                {
                    throw new UnauthorizedOperationException(
                        "You are not the author of this reply, hence you cannot update it."
                    );
                }

                var updatedReply = this.repliesService.Update(replyToUpdate.Id, replyInputDTO.Content);

                return this.Accepted(updatedReply.MapToReplyResponse());
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
            catch (UnauthenticatedOperationException e)
            {
                return this.Forbid(e.Message);
            }
            catch (UnauthorizedOperationException e)
            {
                return this.Unauthorized(e.Message);
            }
        }
    }
}
