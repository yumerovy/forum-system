﻿using FMS.Data.Models;
using FMS.Services.DTOs;
using System;
using System.Collections.Generic;
using FMS.Data.Repositories;
using FMS.Services.Services;
using FMS.Services.Services.Contracts;

using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Infrastructure;

namespace FMS.Services.Mappers
{
    public static class PostsModelMappers
    {

        public static Post MapToPost(this PostInDTO postWebModel, User user)
        {
            //o The post must have a user who created it.

            return new Post
            {
                UserId = user.Id,
                Title = postWebModel.Title,
                Description = postWebModel.Description,
                CreatedOn = DateTime.Now,
                Tags = postWebModel.Tags

            };
        }

        public static PostDTO MapToPostDTO(this Post post)
        {
            var result = new PostDTO { };

            result.Id = post.Id;
            result.Author = post.User.Username;
            result.AuthorId = post.UserId;
            result.Title = post.Title;
            result.Description = post.Description;
            result.CreatedOn = post.ModifiedOn ?? post.CreatedOn;
            result.PostComments = 
                post.PostComments;
            result.Votes = post.Votes;

            return result;
        }

        public static List<PostDTO> MapToPostDTOList(this List<Post> postList)
        {
            var result = new List<PostDTO>();

            foreach (var post in postList)
            {
                result.Add(post.MapToPostDTO());
            }

            return result;
        }

    }
}
