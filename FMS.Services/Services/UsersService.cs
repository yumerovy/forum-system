﻿using FMS.Data.Models;
using FMS.Data.Repositories.Contracts;
using FMS.Data.Models.QueryParameters;
using FMS.Services.Services.Contracts;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FMS.Services.Services
{
    public class UsersService : IUsersService
    {
        private readonly IUsersRepository repository;

        public UsersService(IUsersRepository repository)
        {
            this.repository = repository;
        }


        public List<User> GetAll()
        {
            return this.repository.GetAll();
        }


        public User GetById(int id)
        {
            return this.repository.GetById(id);
        }

        public User GetByUsername(string username)
        {
            return this.repository.GetByUsername(username);
        }

        public User GetByEmail(string email)
        {
            return this.repository.GetByEmail(email);
        }
        public User Create(User user)
        {
            var isUsernameUnique = this.repository.IsUsernameUnique(user.Username);
            if (isUsernameUnique)
            {
                var isEmailUnique = this.repository.IsEmailUnique(user.Email);
                if (!isEmailUnique)
                {
                    throw new ArgumentException("Email is already in use!");
                }
                user = HashPassword(user);

                User createUser = this.repository.Create(user);
                return createUser;
            }
            else
            {
                throw new ArgumentException("Username already exist!");
            }
        }

        public User Update(int id, User user)
        {
            return this.repository.Update(id, user);
        }
        public string Delete(int id)
        {
            var result = this.repository.Delete(id);
            return result;
        }

        private User HashPassword(User user)
        {
            var hasher = new PasswordHasher<User>();
            var passHash = hasher.HashPassword(user, user.Password);
            user.Password = passHash;
            return user;
        }

        public IEnumerable<User> FilterBy(UserQueryParameters filterParameter)
        {
            var filteredUsers = this.repository.FilterBy(filterParameter);

            return filteredUsers;
        }

        public void Logout(int userId)
        {
            this.repository.Logout(userId);
        }
    }
}