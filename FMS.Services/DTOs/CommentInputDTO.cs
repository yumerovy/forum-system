﻿using System.ComponentModel.DataAnnotations;

namespace FMS.Services.DTOs
{
    public class CommentInputDTO
    {
        public int CommentId { get; set; }
        public int PostId { get; set; }

        [Required(ErrorMessage = "*Required")]
        [MinLength(4, ErrorMessage = "A {0} cannot be less than {1} symbols")]
        [MaxLength(5000, ErrorMessage = "A {0} cannot be more than {1} symbols")]

        public string Content { get; set; }
    }
}
