﻿using FMS.Data.Models.CompositeModels;
using FMS.Data.Utilities.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FMS.Data.Models
{
    public class Post : IDeletableEntity, ICreationInfo

    // can implement ICreationInfo .. 
    {
        [Key]
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public int UserId { get; set; }

        public User User { get; set; }

        public List<Comment> PostComments { get; set; } = new List<Comment>();

        public List<Rating> Votes { get; set; } = new List<Rating>();

        public List<Tag> Tags { get; set; } = new List<Tag>();

        public string Photo { get; set; } // link to the photo

        public DateTime CreatedOn { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public bool isDeleted { get; set; }
    }
}
