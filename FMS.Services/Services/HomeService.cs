﻿using FMS.Data.Models;
using FMS.Services.DTOs;
using FMS.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FMS.Services.Services
{
    public class HomeService : IHomeService 
    {
        private readonly IUsersService userService;
        private readonly IPostsService postsService;

        public HomeService(
            IUsersService userService,
            IPostsService postsService)
        {
            this.userService = userService;
            this.postsService = postsService;
        }
        public IEnumerable<User> GetMostActiveUsers()
        {
            return this.userService.GetAll()
                .OrderByDescending(u =>
                (u.UserPosts.Count() + (u.UserComments.Count() / 2) + (u.UserReplies.Count() / 3)))
                .Take(10);
        }

        public IEnumerable<PostDTO> GetMostRecentPosts()
        {
            return this.postsService.GetTopTenRecent();
        }

        public int GetPostsCount()
        {
           return this.postsService.GetAll().Count();
        }

        public IEnumerable<PostDTO> GetMostCommentedPosts()
        {
            return this.postsService.GetTopTenCommented();
        }

        public IEnumerable<Post> GetTrendingPosts()
        {
            return this.postsService.GetAll()
                .OrderByDescending(p => p.PostComments.Count());
        }

        public int GetUsersCount()
        {
            return this.userService.GetAll()
                .Where(u => u.Role == Role.User)
                .Count();
        }

        public int GetAdmisCount()
        {
            return this.userService.GetAll()
                .Where(u => u.Role == Role.Admin)
                .Count();
        }
    }
}
