﻿using FMS.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FMS.MVC.Models.User
{
    public class ProfileViewModel
    {
        public int PostCount { get; set; }
        public int CommentsCount { get; set; }
        public int RepliesCount { get; set; }
        public int ReactionsCount { get; set; }
        public IEnumerable<Post> PostsByUser { get; set; }
    }
}