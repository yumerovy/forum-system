﻿using FMS.Data.Models;
using FMS.Data.Models.QueryParameters;
using System.Collections.Generic;

namespace FMS.Data.Repositories.Contracts
{
    public interface IRepliesRepository
    {
        Reply Create(Reply replyToCreate);

        IEnumerable<Reply> GetAllByUserId(int userId);

        IEnumerable<Reply> GetAllByCommentId(int commentId);

        IEnumerable<Reply> GetAll();

        IEnumerable<Reply> FilterBy(ReplyQueryParameters filterParameters, int commentId);

        Reply GetById(int id);

        bool Delete(Reply replyToDelete);

        Reply Update(Reply replyToUpdate);
    }
}
