﻿using FMS.Data.Models.CompositeModels;
using FMS.Data.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace FMS.Data.Database.Seeders
{
    internal static class RatingsSeeder
    {
        internal static Rating[] SeedData => new Rating[] {
    new Rating {
        PostId = 6,
        UserId = 59,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 17,
        UserId = 16,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 11,
        UserId = 7,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 72,
        UserId = 35,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 35,
        UserId = 16,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 60,
        UserId = 10,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 60,
        UserId = 39,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 10,
        UserId = 20,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 53,
        UserId = 54,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 48,
        UserId = 64,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 2,
        UserId = 24,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 72,
        UserId = 29,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 25,
        UserId = 63,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 21,
        UserId = 74,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 21,
        UserId = 19,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 78,
        UserId = 25,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 29,
        UserId = 15,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 58,
        UserId = 4,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 50,
        UserId = 77,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 4,
        UserId = 70,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 16,
        UserId = 65,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 5,
        UserId = 48,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 50,
        UserId = 74,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 78,
        UserId = 1,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 60,
        UserId = 66,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 69,
        UserId = 53,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 14,
        UserId = 25,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 27,
        UserId = 35,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 35,
        UserId = 4,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 76,
        UserId = 20,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 15,
        UserId = 71,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 40,
        UserId = 51,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 53,
        UserId = 33,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 38,
        UserId = 50,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 7,
        UserId = 52,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 51,
        UserId = 77,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 5,
        UserId = 79,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 49,
        UserId = 61,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 75,
        UserId = 16,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 45,
        UserId = 9,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 51,
        UserId = 30,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 5,
        UserId = 63,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 73,
        UserId = 13,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 48,
        UserId = 11,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 36,
        UserId = 25,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 47,
        UserId = 22,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 61,
        UserId = 53,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 79,
        UserId = 67,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 40,
        UserId = 74,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 13,
        UserId = 15,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 78,
        UserId = 79,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 25,
        UserId = 53,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 25,
        UserId = 26,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 80,
        UserId = 22,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 7,
        UserId = 33,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 55,
        UserId = 80,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 45,
        UserId = 18,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 67,
        UserId = 54,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 6,
        UserId = 52,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 9,
        UserId = 39,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 9,
        UserId = 25,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 63,
        UserId = 7,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 32,
        UserId = 50,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 32,
        UserId = 11,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 58,
        UserId = 10,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 36,
        UserId = 18,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 56,
        UserId = 67,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 45,
        UserId = 52,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 65,
        UserId = 27,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 24,
        UserId = 71,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 8,
        UserId = 71,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 44,
        UserId = 49,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 37,
        UserId = 4,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 40,
        UserId = 38,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 32,
        UserId = 67,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 36,
        UserId = 24,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 29,
        UserId = 29,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 8,
        UserId = 46,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 16,
        UserId = 68,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 72,
        UserId = 11,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 68,
        UserId = 43,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 20,
        UserId = 10,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 50,
        UserId = 46,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 15,
        UserId = 51,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 2,
        UserId = 70,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 74,
        UserId = 64,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 33,
        UserId = 55,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 60,
        UserId = 35,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 50,
        UserId = 44,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 67,
        UserId = 25,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 25,
        UserId = 28,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 10,
        UserId = 25,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 37,
        UserId = 28,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 18,
        UserId = 53,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 72,
        UserId = 67,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 2,
        UserId = 69,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 52,
        UserId = 73,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 56,
        UserId = 7,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 62,
        UserId = 18,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 55,
        UserId = 78,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 72,
        UserId = 72,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 8,
        UserId = 56,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 13,
        UserId = 32,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 19,
        UserId = 74,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 72,
        UserId = 15,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 26,
        UserId = 29,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 75,
        UserId = 55,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 65,
        UserId = 76,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 28,
        UserId = 51,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 44,
        UserId = 24,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 59,
        UserId = 54,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 39,
        UserId = 30,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 27,
        UserId = 78,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 9,
        UserId = 75,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 48,
        UserId = 56,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 23,
        UserId = 7,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 2,
        UserId = 52,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 37,
        UserId = 9,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 7,
        UserId = 16,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 5,
        UserId = 3,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 6,
        UserId = 72,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 40,
        UserId = 9,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 56,
        UserId = 13,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 15,
        UserId = 6,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 75,
        UserId = 14,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 80,
        UserId = 32,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 39,
        UserId = 68,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 45,
        UserId = 66,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 6,
        UserId = 27,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 6,
        UserId = 80,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 70,
        UserId = 55,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 67,
        UserId = 52,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 34,
        UserId = 38,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 46,
        UserId = 50,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 63,
        UserId = 51,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 27,
        UserId = 50,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 18,
        UserId = 37,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 62,
        UserId = 44,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 27,
        UserId = 19,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 43,
        UserId = 57,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 30,
        UserId = 24,
        Vote = VoteType.Dislike
    },
    new Rating {
        PostId = 44,
        UserId = 27,
        Vote = VoteType.Like
    },
    new Rating {
        PostId = 31,
        UserId = 21,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 25,
        UserId = 78,
        Vote = VoteType.Default
    },
    new Rating {
        PostId = 35,
        UserId = 39,
        Vote = VoteType.Like
    }
};
    }
}
