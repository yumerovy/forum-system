﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FMS.Data.Models.QueryParameters
{
    public class ReplyQueryParameters
    {
        public string AuthorUserName { get; set; }

        // For moderation purposes:
        // we may need to search by keywords in the content
        public string ContentContains { get; set; }

        public string SortBy { get; set; }

        public string SortOrder { get; set; }
    }
}
