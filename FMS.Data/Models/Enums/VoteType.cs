﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FMS.Data.Models.Enums
{
    public enum VoteType
    {
        Default,
        Like,
        Dislike
    }
}
