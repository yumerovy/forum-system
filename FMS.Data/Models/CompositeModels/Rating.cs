﻿using FMS.Data.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace FMS.Data.Models.CompositeModels
{

    public class Rating
    {
        public int UserId { get; set; }

        public User User { get; set; }

        public int PostId { get; set; }

        public Post Post { get; set; }

        public VoteType Vote {get; set;}

    }
}
