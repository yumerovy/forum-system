﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FMS.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Username = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Role = table.Column<int>(type: "int", nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PersonalInfo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsLogged = table.Column<bool>(type: "bit", nullable: false),
                    IsBlocked = table.Column<bool>(type: "bit", nullable: false),
                    LastLogIn = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Photo = table.Column<byte[]>(type: "varbinary(max)", nullable: true),
                    isDeleted = table.Column<bool>(type: "bit", nullable: false),
                    CreatedOn = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifiedOn = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Posts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    Photo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedOn = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifiedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    isDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Posts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Posts_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Content = table.Column<string>(type: "nvarchar(max)", maxLength: 5000, nullable: false),
                    PostId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    isDeleted = table.Column<bool>(type: "bit", nullable: false),
                    CreatedOn = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifiedOn = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comments_Posts_PostId",
                        column: x => x.PostId,
                        principalTable: "Posts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Comments_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Tags",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(25)", maxLength: 25, nullable: false),
                    PostId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tags", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tags_Posts_PostId",
                        column: x => x.PostId,
                        principalTable: "Posts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Votes",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "int", nullable: false),
                    PostId = table.Column<int>(type: "int", nullable: false),
                    Vote = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Votes", x => new { x.PostId, x.UserId });
                    table.ForeignKey(
                        name: "FK_Votes_Posts_PostId",
                        column: x => x.PostId,
                        principalTable: "Posts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Votes_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Replies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Content = table.Column<string>(type: "nvarchar(max)", maxLength: 5000, nullable: false),
                    CommentId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    isDeleted = table.Column<bool>(type: "bit", nullable: false),
                    CreatedOn = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifiedOn = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Replies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Replies_Comments_CommentId",
                        column: x => x.CommentId,
                        principalTable: "Comments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Replies_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PostTags",
                columns: table => new
                {
                    PostId = table.Column<int>(type: "int", nullable: false),
                    TagId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostTags", x => new { x.PostId, x.TagId });
                    table.ForeignKey(
                        name: "FK_PostTags_Posts_PostId",
                        column: x => x.PostId,
                        principalTable: "Posts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PostTags_Tags_TagId",
                        column: x => x.TagId,
                        principalTable: "Tags",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Tags",
                columns: new[] { "Id", "Name", "PostId" },
                values: new object[,]
                {
                    { 1, "stairs", null },
                    { 21, "beautiful", null },
                    { 19, "tent", null },
                    { 18, "high", null },
                    { 17, "creative", null },
                    { 16, "uniform", null },
                    { 15, "staircase", null },
                    { 14, "imagine", null },
                    { 13, "someothertag", null },
                    { 12, "city", null },
                    { 20, "rent", null },
                    { 10, "countryside", null },
                    { 9, "wooden", null },
                    { 8, "cheap", null },
                    { 7, "two-stories", null },
                    { 6, "roof", null },
                    { 5, "expensive", null },
                    { 4, "convertible", null },
                    { 3, "sell", null },
                    { 2, "house", null },
                    { 11, "free", null }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "CreatedOn", "Email", "FirstName", "IsBlocked", "IsLogged", "LastLogIn", "LastName", "ModifiedOn", "Password", "PersonalInfo", "PhoneNumber", "Photo", "Role", "Username", "isDeleted" },
                values: new object[,]
                {
                    { 52, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "maecenas.malesuada@icloud.org", "Samuel", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Sweet", null, "AQAAAAEAACcQAAAAEA8eGmWK0skvxNCs5K+h5fJnsuHfJGpmmt9Bf5kxfcusvUcw3KIK+DMFi/Y8ogvdEQ==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-453-870-4534", null, 1, "Colebest", false },
                    { 53, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "purus.ac@yahoo.edu", "Oleg", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Rodriquez", null, "AQAAAAEAACcQAAAAEBJztdtFjMCqtuM5IOQbbouNfLrSZaYsT7TtrRGsCWbsZDUHW0B3zDr1fsdai5AKZA==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-727-245-1073", null, 1, "FightJoy", false },
                    { 57, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "pharetra.quisque.ac@outlook.org", "Daniel", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Richards", null, "AQAAAAEAACcQAAAAEMBn5APB21/iHAFACYbdjfSTn+1SaWQOV7fd42NndjZGAiAYZugl7FUQl9xxjDuJBQ==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-348-951-9415", null, 0, "FinestFiance", false },
                    { 55, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "sagittis.augue@yahoo.couk", "Colleen", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Strickland", null, "AQAAAAEAACcQAAAAEPvtU3EvGwLRGQDKDuB8uf+TICBJmyhfkyM08NYZA8zCBB5Pq0g0y+MLS1yzdALqkQ==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-613-583-1805", null, 1, "LeopardRuns", false },
                    { 56, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "proin.ultrices.duis@outlook.net", "Mason", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Pennington", null, "AQAAAAEAACcQAAAAEEXZo616U4BjyCB2HyOjp4bibakmW29bMF7oAPXFkgmNutBceZHcw14aY/2K3GZsNg==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "236-6671", null, 0, "11woodydig", false },
                    { 51, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "taciti.sociosqu@yahoo.couk", "Fatima", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Hensley", null, "AQAAAAEAACcQAAAAECyqiLOMkeLhW039Fioj4tM4uVGFOanZDY07HjrzXrU2HRD82ad3HgEeUuN+to+RUw==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-181-464-6488", null, 1, "FreedomFrosh", false },
                    { 54, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "donec.felis@icloud.net", "Idona", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Herrera", null, "AQAAAAEAACcQAAAAEI2d843+kXIy6UA2LK6AYBMCUGpEpOQJHfIjVfQWyZ/RQKig2C5sf+R2EKSzdHDEVA==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-731-381-6125", null, 0, "TigerJason", false },
                    { 50, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "euismod.enim@outlook.com", "Elijah", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Bradshaw", null, "AQAAAAEAACcQAAAAEG/wFbjo7ppn1KPqmu4YrLd63ziFz5Fsxe86zeJvRnwzOx9FUAO2/ijVNUfFyI0T7Q==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "771-1365", null, 0, "SpeedHack", false },
                    { 45, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "vulputate.nisi@hotmail.ca", "Mari", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Klein", null, "AQAAAAEAACcQAAAAEPBu7AjL8RcXs4N260Hd5RjWv1rHCVMJ58zB03YKi/JJ5DDJObBByDlm9cyISfMrxA==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "636-6615", null, 0, "Interhack", false },
                    { 48, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "id@google.org", "Nissim", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Palmer", null, "AQAAAAEAACcQAAAAEC5+SFVKTxosqzkEPA1o7slkoMvaefNPsf324zXPXAcOMfv+jvr+4qDfB7v81hiugA==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-779-557-8531", null, 0, "Shorty78", false },
                    { 47, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "bibendum.ullamcorper@icloud.edu", "Kaye", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Luna", null, "AQAAAAEAACcQAAAAEEBG8CEeWI9EjS4ZYttGttEe0XtGmDUeXxQExhp6CT5xoydlacRk+JHKiYJ2CVfYaw==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-532-416-2858", null, 1, "FighterMountainJames", false },
                    { 46, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "volutpat.nulla@icloud.edu", "Juliana", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Brock", null, "AQAAAAEAACcQAAAAEGee5HmP51kYvTd3RxdDHJSIPQNp6jGMxWMjKlyUTw/FamvMORsgF9SdlgDeWNR0XQ==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "851-6553", null, 1, "FreeChiquita", false },
                    { 44, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "orci.lobortis.augue@protonmail.ca", "Alika", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Williamson", null, "AQAAAAEAACcQAAAAEBETK77vvg0vI1PcDQ0+S+Ai69NDGLSA6zjXStgOV3XcH4FJOCAm/3R9WLjkaArlfA==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-796-818-7731", null, 1, "SpeedStreet", false },
                    { 43, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "amet.dapibus@outlook.net", "Aquila", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Noel", null, "AQAAAAEAACcQAAAAEKOA1Kkmds1A4vr4z3QsH2W9aZPoW695O6YSw/0JY3LCltsGd9be5rqpElJ5e/qitw==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "492-4165", null, 0, "HacksPacs", false },
                    { 42, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "praesent.eu@yahoo.com", "Constance", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Everett", null, "AQAAAAEAACcQAAAAEPGUwLnoYazCc71U0Aveu0RZjJZCawykZePnmFrlv/KGYgLP5RvsXkoZjMK0jRRabg==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-155-637-2278", null, 0, "PiracyPig", false },
                    { 58, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "nulla.ante@outlook.org", "Burton", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Harris", null, "AQAAAAEAACcQAAAAEBmIs4fvsIrrbgzwCp+/CBhU1eKSfb7DlG5i6ToW5WE8CQV1clhFwNVJQ17SnzDGcg==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "708-4738", null, 0, "GreatGreenth", false },
                    { 49, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "in.nec@outlook.com", "Rafael", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Moss", null, "AQAAAAEAACcQAAAAEOrgxdRoLfspdZ5Qgh8pI2ZgSEe3nA7B6T6KiImYptYq4Pb0Vx5iT769Rbu7nuxSBg==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-974-415-6565", null, 1, "DriftRip", false },
                    { 59, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "nibh.enim@outlook.edu", "Thomas", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Garcia", null, "AQAAAAEAACcQAAAAEOiWeAOoVxDuqV3kLXJjL2Q2rFyp1aLTR/FNBUmZMoCDdPPMWYB9uzJZjlBm1j19/w==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "788-4704", null, 1, "JasonPink", false },
                    { 70, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "consectetuer.adipiscing@hotmail.org", "Victor", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Erickson", null, "AQAAAAEAACcQAAAAEDA3IbASETXqHpB9zDyuhZZKIV2nRIRHZfkWWcx5X11oRMY8U5/w9eCeVWfyYz4IXQ==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "454-2197", null, 0, "LentilLeopards", false },
                    { 61, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "vitae.mauris@icloud.couk", "Kimberley", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Snow", null, "AQAAAAEAACcQAAAAED90vcEqhKuCzB+QgHtdl+zBf7Y8h6m+JofE4STuPD17+Ivy2djIlbS7pVUoOjMwVw==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-941-687-3886", null, 1, "Bleak11", false },
                    { 78, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "magna.suspendisse@hotmail.ca", "Bryar", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Hoffman", null, "AQAAAAEAACcQAAAAEKWcc5PnCuTJmDHRX8qR43XGoz9mMv3Izg3sG7LUnRTUVvTTVuF3Z217cUO2Ephw9w==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "542-2676", null, 1, "Sualiena11", false }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "CreatedOn", "Email", "FirstName", "IsBlocked", "IsLogged", "LastLogIn", "LastName", "ModifiedOn", "Password", "PersonalInfo", "PhoneNumber", "Photo", "Role", "Username", "isDeleted" },
                values: new object[,]
                {
                    { 77, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "rhoncus.id@outlook.org", "Seth", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Kennedy", null, "AQAAAAEAACcQAAAAEAJZuVPf3LlBhQyyG6JF3izgRITE01ig1IdoH9rEN5dTy5kNXUsVldzP1ZvongCQ4Q==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "288-1595", null, 0, "LeopardTrain", false },
                    { 76, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "purus@protonmail.org", "Ariel", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Briar", null, "AQAAAAEAACcQAAAAEI8x5tu27IG4HY5vwKKbGBo6LxifRkwBL6+wMirmq9Czy4w8bDZ0YQMrKntQ4faF/Q==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "511-9867", null, 0, "Aryhack", false },
                    { 75, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "fringilla.cursus@hotmail.couk", "Clayton", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Macaulay", null, "AQAAAAEAACcQAAAAEF6L4tk2HSB72Z/a6y7BHTmBum7nn1ZJiHGC5qyBa4KevemnGsKbkJX9JfQyq72YPg==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-842-442-0667", null, 0, "FreeMarquis", false },
                    { 74, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "risus.donec@protonmail.edu", "Ryder", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Raven", null, "AQAAAAEAACcQAAAAEKrAJcXdh+mc94Pc9H7Wdtl1DD3lX/9QtWSl9hwz7WNROKIx2vi/Qgl6IoZTDDkPAg==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-210-557-7712", null, 1, "Mountainte", false },
                    { 73, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "semper.erat.in@google.com", "Damon", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Underwood", null, "AQAAAAEAACcQAAAAEAlBUM14nnKZJltW6TDCjH65/hKzt4DGT9g3HgdmxDjoJfLfs6cygDCTztM93OK3wQ==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-877-698-6134", null, 0, "Befobest", false },
                    { 72, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "in@google.org", "Mia", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Gross", null, "AQAAAAEAACcQAAAAEM1zmD792xFQvb2EKAC6NIFbS7es7oQNIbLa+IgTuWyc9rgpOVZZFQ3PgxPsCi9C8Q==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "693-4376", null, 0, "MiniverMink", false },
                    { 71, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "odio.auctor@hotmail.org", "Colby", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Browning", null, "AQAAAAEAACcQAAAAEFmcY7kkeKB/N1bOjHHoCX/rM9weetNVGCojdU9rQZ/A9gnoHX4mzAgTpt5HqK1hiA==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "329-4151", null, 1, "RunFun", false },
                    { 41, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "nunc.sed@hotmail.net", "Juliet", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Greer", null, "AQAAAAEAACcQAAAAEDDMu0m9/MhI5MWuT9EwU2ND/+X1d4S09ooxmS/T+LRINSrRjaS8+E1JdMIuSxkwvg==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "765-7275", null, 0, "Acostreet", false },
                    { 69, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "porttitor@outlook.couk", "Francis", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Bradshaw", null, "AQAAAAEAACcQAAAAENtbXAm3MjqtEF66cj/hCRRS/HRkk83CJxyTB6Xh3gu08df5/2WTdUmhVduysF7sTg==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "833-8232", null, 1, "DocentExcellent", false },
                    { 68, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "odio.phasellus.at@protonmail.net", "Jocelyn", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Marsh", null, "AQAAAAEAACcQAAAAECFK048oXLMmyhN8GR2GH3qri1IBl17zN1CLbY5H/8oTPr5SbJCvOGpTI/20ZXLLqg==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "349-2506", null, 1, "FighterFight", false },
                    { 67, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "sapien.imperdiet@outlook.ca", "Courtney", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Talley", null, "AQAAAAEAACcQAAAAEFQK6dpuMOCfqrO2oEZbDP1RYVNqDCqtF9tuX7EAQPOK64QnwL0RpjktTWAuBpjcbg==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-883-478-2176", null, 0, "JailJason", false },
                    { 66, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "tempus.risus@outlook.couk", "Amela", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Moran", null, "AQAAAAEAACcQAAAAECQscCPkAayTGmMju0fOm18jhnRIt0Ry4hj74DaR2hYEQUH4Z4bakQvmU20qhJAp9w==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-615-792-3347", null, 0, "Clodhack", false },
                    { 65, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "aliquam@yahoo.org", "Kiara", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "West", null, "AQAAAAEAACcQAAAAECs2Jgugll4uqRku1Lr5qDqeOOFUB2xro0nccH/1/DzTWvozTnVo0thdrbyOOiKCVw==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-975-297-5564", null, 1, "SpeedDrift", false },
                    { 64, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "facilisis.eget@hotmail.net", "Garth", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Slater", null, "AQAAAAEAACcQAAAAEBjI+6jqn9PeJFY3oyePN3uNsD2/5ODjx452w/ntLO2pM3F8ySqA+wcoV7r8BqI1yA==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-284-268-7757", null, 0, "Freenpho", false },
                    { 63, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "id@yahoo.com", "August", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Hopper", null, "AQAAAAEAACcQAAAAEOwu7ZuoXSs+Rl0Oc+rgOGorZ40ATPJLRr+Zb2lRebEvgz920WxbML3b6mc0Vld58Q==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-569-865-4389", null, 1, "Parfree", false },
                    { 62, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "cras.dictum.ultricies@yahoo.org", "Thane", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Hardin", null, "AQAAAAEAACcQAAAAEE1RTOtl2J0IAf1lTRWXbGZ7pC9he2GjJoL2QQY1Mx6yIaRIIH6rAseUCBICQD17kQ==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-761-231-8543", null, 0, "erediato", false },
                    { 60, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "vestibulum.lorem.sit@protonmail.net", "Alvina", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Skinner", null, "AQAAAAEAACcQAAAAEHhyavS68HLaDM8SrgoOyPXXMbUYF1Ye6exjoTE7ZjSdaJA18ou7RZP8TQ2hN/OvqQ==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "586-2771", null, 0, "LeopardChiquita", false },
                    { 40, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "enim@aol.edu", "Melvin", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Vaughn", null, "AQAAAAEAACcQAAAAEO7eZEYq7pLQYc/5FPS95U7uTnKUMUZg97AaIKXpRxJ7/2xj4wR1IJtizo9Gg2pwYg==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-443-523-2240", null, 0, "Cheerup9", false },
                    { 30, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "ligula.donec@protonmail.couk", "Ocean", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Wallace", null, "AQAAAAEAACcQAAAAEHyEb0Or11LUgluAP357xY1RYkDPGioVZYld1nK/qcX5z1ytE2nNjxw5l/Uk1xtKNg==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-920-438-1734", null, 0, "Brilanimal", false },
                    { 38, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "fusce.diam@icloud.com", "Inga", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Benton", null, "AQAAAAEAACcQAAAAED45ekQgo8+uXJ8+3EoMLaATRWlfT3HocpzpeNdlSx4KxsktkNidjBXVJBH9+SkVlg==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-391-676-2252", null, 1, "GrimSpeed", false },
                    { 16, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "luctus@aol.net", "Paki", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Glenn", null, "AQAAAAEAACcQAAAAEJtLWek5w6b0zXsPhja1KsQHNV8xtCfeL1y1fUUXwnJKwz7Dl0VOthfhDJ0HTGAFHg==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-211-193-9865", null, 0, "Ejamrioni7", false },
                    { 15, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "nulla@yahoo.net", "Kimberly", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Rhodes", null, "AQAAAAEAACcQAAAAENgxsDdfYNLnRMBUD5sa1Ab3WQLRGCc6CphaLwRUoHPxvn+J854j1e7yW53QOsfyaw==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "685-3175", null, 0, "CashCanon", false },
                    { 14, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "aliquam.eu@hotmail.couk", "Dean", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Howell", null, "AQAAAAEAACcQAAAAEFhXoh1CZWFPos2dxxXepKPSTa36YJrqaqsQsFdFUkgX+F0NDKB0LOlTYRM1e2znlA==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-231-810-4835", null, 0, "BeerThug", false },
                    { 13, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "nascetur.ridiculus@protonmail.ca", "Constance", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Rollins", null, "AQAAAAEAACcQAAAAEKcUNw2wqenpTIxfbvq9TmR5sFuCFmOVzAYrN8bDpwtAPJQel7PZETd7K1+vfMg1CQ==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "749-6663", null, 1, "LiquidityLipper", false },
                    { 12, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "vivamus@outlook.ca", "Mannix", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Ortega", null, "AQAAAAEAACcQAAAAENzYM2cpxQdNGntMLe2YDEv9YcdtRSGyA6YLmK/0L/Jxk7Reb3xacBqDASUVjeKJog==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-576-908-3853", null, 0, "MorningSevenTen", false },
                    { 11, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "magna.nec.quam@aol.edu", "Jasper", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Mccall", null, "AQAAAAEAACcQAAAAEBhaNYExchTz/C4kxItfyt6C3ICYVwDDRu3LqE5Vrc8+3EbJmc6tJB4BunH/zxjwqg==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-515-942-5555", null, 0, "EmirBeer", false },
                    { 10, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "nec.leo@google.org", "Caryn", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Cooley", null, "AQAAAAEAACcQAAAAEApC15NKrHu6eaLvixlerZTXtqcZ5LswtcngTcB6V6uUfNj/+Y/goxzCmYDDR/A5/A==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "567-6508", null, 0, "DogMoney7", false },
                    { 17, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "egestas.sed.pharetra@yahoo.net", "India", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Bryan", null, "AQAAAAEAACcQAAAAEG9jCaGya6f+g1mu7Ku6o/uz2HRlMa0FQddbhMaRVvVG2l2ZHYVoM0zZlfdxP2MswQ==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "219-2112", null, 0, "GougerLager", false },
                    { 9, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "ac.facilisis@outlook.com", "Brendan", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Mcdaniel", null, "AQAAAAEAACcQAAAAEGwThYlcV2gKK0sIL9cVTBjtHU7zSmFarMGr8IT65FKLEsL1Xf2jHaM22m8205l6pw==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-881-139-1553", null, 1, "johnpocope", false },
                    { 7, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "tempus@aol.org", "Thane", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Bean", null, "AQAAAAEAACcQAAAAEH8zIiogjvQrgvPZhx6oe0KhSbOgkxkH6YdCztWCh6TWWCnPSCYetp2SlbR8CeITjw==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-174-513-7301", null, 1, "DrinksDroob", false },
                    { 6, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "cursus.et@hotmail.edu", "Martin", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Blankenship", null, "AQAAAAEAACcQAAAAEBGQ/TLQpazatm6fpBp3q5QA36madY7TFmDDJ18M0ZEOPCr1VIKmnOrRl3auZVwx+A==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "174-6643", null, 0, "Mfootball", false },
                    { 5, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "ut.sagittis@icloud.com", "Lev", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Walls", null, "AQAAAAEAACcQAAAAEAMOr8t+Z76oC40/DhL/ejglbyGehxTsjLyWjBZy0P/6pXg3XIg4AgCQ4fDcBFmy0g==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "237-1896", null, 1, "SaviorBeer", false },
                    { 4, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "etiam.gravida@protonmail.edu", "Alan", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Maldonado", null, "AQAAAAEAACcQAAAAELqgwYIC3M9O3UZ14pSJj4qS+wuBUFBw4NkC5chO6Q02xXrxem3EdRbbtcd5GmrA9A==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-635-537-7126", null, 1, "JamesMoney", false },
                    { 3, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "sit.amet@hotmail.edu", "Mara", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Pate", null, "AQAAAAEAACcQAAAAEICugTpSIc5F+KGX9nxLqcfL6B0BID9b/9QU362FhY0lCM0rCvjnUTeAMfR8x9ix1Q==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore. Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "202-6136", null, 1, "Scanner", false },
                    { 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "vestibulum@yahoo.com", "Herrod", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Watkins", null, "AQAAAAEAACcQAAAAEBdVuy5b/3RrEAujd6RLD1KK3cKnQxcO1tlR8rzuukt8SfuvqneyGbHQ+vE1nxWiJw==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "832-6135", null, 0, "7Ball", false },
                    { 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "admin@google.couk", "Steven", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "McMillan", null, "AQAAAAEAACcQAAAAEKKiMwYW0rGmIJ4Tx1ZUUGGzj/D09XYLJ9oYID5ti3CxEhZ6jwpB0mJaz0P6SgLEXA==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-787-447-4525", null, 1, "TEST_USER", false },
                    { 8, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "sodales.purus@google.com", "Ariel", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Horton", null, "AQAAAAEAACcQAAAAEIHJe1WJ4zLdXT0bTuq7fUKMwMvgNwj5Mq6b6damlXYT/OkQNeOLe3y3aIs63tIYcQ==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "881-3259", null, 0, "BeerMoney", false },
                    { 18, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "dapibus@aol.ca", "Mira", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Chan", null, "AQAAAAEAACcQAAAAEN4Auk6yYdVZa4aFwk5+KmaWTXqvNx4y/h9DfDUOe4jN65WwKEj3CXgUM6mAFGjonw==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "448-2571", null, 0, "IslandDog", false },
                    { 19, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "et@protonmail.edu", "Willow", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Mccray", null, "AQAAAAEAACcQAAAAEGyv/VFaYdGHIRrp0d442eFSOqJCh1aoyYDY1/GMoQ/TVVDhFYduQ4WYEpkSxSIY2A==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "416-5549", null, 1, "BeerFootball", false },
                    { 20, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "in.ornare@hotmail.net", "Xaviera", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Rosario", null, "AQAAAAEAACcQAAAAEAq2VRDq1ZlJKoIXrfaGPE+pxk6ezjhaKhwAUn/np1m9oCf5FaRRmFLbDF2v1fOeUQ==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-693-869-3748", null, 0, "BeverageAlex", false },
                    { 37, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "et.commodo.at@outlook.org", "Eric", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Thornton", null, "AQAAAAEAACcQAAAAEGI6jI6X2seZXag8b8kNvO7lSmQlM4i/qqsdaqcuj0mjVuZJcyw9DxdqPJfF4yC3HQ==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-556-727-6264", null, 0, "Hacker333", false },
                    { 36, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "eu.placerat@icloud.net", "Juliet", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Padilla", null, "AQAAAAEAACcQAAAAEBjz77l11a1wzEThNwTFi4S3PnJceHDsonzRrXmNljDaDOntYTElzIT4cuFEc3HZwg==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "888-9492", null, 1, "Oktanimal", false }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "CreatedOn", "Email", "FirstName", "IsBlocked", "IsLogged", "LastLogIn", "LastName", "ModifiedOn", "Password", "PersonalInfo", "PhoneNumber", "Photo", "Role", "Username", "isDeleted" },
                values: new object[,]
                {
                    { 35, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "a.odio.semper@icloud.ca", "Beau", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Ross", null, "AQAAAAEAACcQAAAAEHODoBWDqOV6Su08pMXLsI+yv0q1+MEPRoeOiyImQC1xyYI5oWmFfB9VPO5jkalZXA==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-742-558-9886", null, 0, "RegularRelay", false },
                    { 34, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "donec.vitae.erat@icloud.net", "Kelsey", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Nicholson", null, "AQAAAAEAACcQAAAAENFGF+4g0c64spzLZfL/m3OiIxo61OpZ6Ap1ELYijfqLUiCdEDns+yvbAB1Fe5tTqA==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "628-2417", null, 0, "ChideHillside", false },
                    { 33, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "metus.vivamus@protonmail.org", "Travis", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Moore", null, "AQAAAAEAACcQAAAAEDWkz3I2ZxZ0dKIsMVRDWGbJjSAtt3wFc/gSpS2rdPw9L9B7cEmIcqCgv/HGsiDKuQ==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "742-4173", null, 1, "Cutie88", false },
                    { 32, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "lectus.justo.eu@aol.couk", "Keefe", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Woodard", null, "AQAAAAEAACcQAAAAEDtw6F07I71IqHxm/H0fBwgENcTJ67zeUhy8srr0+2PVnqlZVp6ojJHaKZhRbf+Dfg==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-776-267-7533", null, 1, "FreedomFrog", false },
                    { 31, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "dictum.mi.ac@outlook.com", "Meredith", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Schultz", null, "AQAAAAEAACcQAAAAEGYy47OeKFPQn7Fbm/OaXPmTE+VllnnFIXb/o0ciUXbVRa2WAqrEbSypms2vekZzPA==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-806-760-1912", null, 1, "Drjackfree", false },
                    { 79, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "imperdiet.ornare@protonmail.couk", "Ferdinand", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Bell", null, "AQAAAAEAACcQAAAAEBzAXcTHAs0Kfj+E4DpBCjzpO23ObdWiknkK1cfKkSa9ZKJCdULq1i3njkekrEwwow==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-512-177-3575", null, 1, "Panet99", false },
                    { 29, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "donec.feugiat.metus@protonmail.org", "Unity", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Velazquez", null, "AQAAAAEAACcQAAAAEGkb9k1q+LTv98tolBxzH2gjgspk/WayLTQaVBzRfqm0l6xyl1wBG1cD6GjflrqoWg==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "273-3418", null, 1, "Hills5", false },
                    { 28, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "orci.luctus@yahoo.couk", "Illana", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Short", null, "AQAAAAEAACcQAAAAEGr7VGq+p06afWD5b5CXY56fC2B9ufntJl8VUg1NlvmjcZ3JvX9YstHS5f7NzPFOZw==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "346-9086", null, 1, "Freevesper", false },
                    { 27, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "ultrices.posuere@aol.com", "Louis", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Wise", null, "AQAAAAEAACcQAAAAEOtckxh1zt2E6xITAmYzee+zxMU2E5l4W6Si0iWhWt0JgnC40bunGzQomKAhi8ilCA==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "375-4480", null, 1, "JerryLivestock", false },
                    { 26, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "cursus.in@icloud.couk", "Leandra", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Richmond", null, "AQAAAAEAACcQAAAAEF6GqV/51REgZRuCgcGzpINUQaXAOtmLU+X2qBkKrg02a+PHXIwgQLrBwNMCuFIzjw==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-429-684-0332", null, 1, "MountainHills", false },
                    { 25, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "mattis.ornare.lectus@outlook.edu", "Aaron", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Castillo", null, "AQAAAAEAACcQAAAAELL+iYl+KDBWbbc4+J5bTGQVl01/SqTIa0AhQSGzGLZNg+TMuEXuzSpKmjkqbhhhwA==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-127-888-3232", null, 1, "Bewalking", false },
                    { 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "dignissim@google.edu", "Keefe", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Flynn", null, "AQAAAAEAACcQAAAAECk4b8yV26UN5OQFXgc0aHAHifRBaQyKphZSSFPFXVlqBwFBaUCm5s/wCcta0xvnDA==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "363-3553", null, 0, "AnimalsAnnot", false },
                    { 23, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "eu@icloud.com", "Dante", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Leblanc", null, "AQAAAAEAACcQAAAAEG1/hFnKZ0syMpkBetUASy+WjbX3fVzE3hlPwZ4nigLKp8duKduS5TR2dRWapNOQaw==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-276-297-7664", null, 0, "JameFreeJame", false },
                    { 22, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "a.tortor@outlook.edu", "Kennedy", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Larsen", null, "AQAAAAEAACcQAAAAEMr3SHC0gdcdfvFbnsyCOiyvG9QgQ9nHe7SDybPN7cTPDOgeywjOpi1TOXYgItRr8g==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-743-997-6732", null, 0, "FootballDog", false },
                    { 21, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "neque@google.edu", "Yvonne", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Hansen", null, "AQAAAAEAACcQAAAAEKS58HZs+Vvv/C6HaY+BiQTEMlzLU1Aj83FgDYRJ7adXhRFMYHjejEZIJywit2u9aw==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "1-172-362-8555", null, 0, "Money777", false },
                    { 39, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "a.felis@yahoo.net", "Desiree", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Solomon", null, "AQAAAAEAACcQAAAAEBbzHFAwnliZx2al4c4nBpn1hmBc/8KxsxmL8pXycK4fCtBuSVyu6N6wJMe5QZ6Zmw==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "232-2979", null, 1, "Drifterer", false },
                    { 80, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "porta.elit@yahoo.ca", "Minerva", false, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Mosley", null, "AQAAAAEAACcQAAAAEO/zS5Ks2zWjg7bXlgor+O1u1FeQOldB7gQsUHkBYz9eXrhsrQVomDUCcDz/2JDZgg==", "Lorem ipsum dolor sit amet diam nonummy nibh dolore.", "141-0132", null, 1, "GoldGoofy", false }
                });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "Id", "CreatedOn", "Description", "ModifiedOn", "Photo", "Title", "UserId", "isDeleted" },
                values: new object[,]
                {
                    { 24, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Vivamus non lorem vitae odio sagittis semper. Nam tempor diam dictum sapien. Aenean massa. Integer vitae nibh. Donec est mauris, rhoncus id, mollis nec, cursus a, enim. Suspendisse aliquet, sem ut cursus luctus, ipsum leo elementum sem, vitae aliquam eros turpis non enim. Mauris quis turpis vitae purus gravida sagittis. Duis gravida. Praesent eu nulla at sem molestie sodales. Mauris blandit enim consequat purus. Maecenas libero est, congue a, aliquet vel, vulputate eu, odio. Phasellus at augue id ante dictum cursus. Nunc mauris elit, dictum eu, eleifend nec, malesuada", null, null, "massa. Integer vitae nibh.", 1, false },
                    { 55, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "accumsan laoreet ipsum. Curabitur consequat, lectus sit amet luctus", null, null, "vel,", 55, false },
                    { 38, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Nulla semper tellus id", null, null, "Duis cursus, diam at pretium aliquet,", 54, false },
                    { 33, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "felis ullamcorper viverra. Maecenas iaculis aliquet diam. Sed diam lorem, auctor quis, tristique ac, eleifend vitae, erat. Vivamus nisi. Mauris nulla. Integer urna. Vivamus molestie dapibus ligula. Aliquam erat volutpat. Nulla dignissim. Maecenas ornare egestas ligula. Nullam feugiat", null, null, "nec ante blandit viverra. Donec tempus,", 53, false },
                    { 30, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Sed pharetra, felis eget varius ultrices, mauris ipsum porta elit, a feugiat tellus lorem eu metus. In lorem. Donec elementum, lorem", null, null, "nec, eleifend non, dapibus rutrum, justo.", 53, false },
                    { 17, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "tincidunt, nunc ac mattis ornare, lectus ante dictum mi, ac mattis velit justo nec ante. Maecenas mi felis, adipiscing fringilla, porttitor vulputate, posuere vulputate, lacus. Cras interdum. Nunc sollicitudin commodo ipsum. Suspendisse non leo. Vivamus nibh dolor, nonummy ac, feugiat non, lobortis quis, pede. Suspendisse dui. Fusce diam nunc, ullamcorper eu, euismod ac, fermentum vel, mauris. Integer sem elit, pharetra ut, pharetra sed, hendrerit a, arcu. Sed et libero. Proin mi. Aliquam gravida mauris ut mi. Duis risus odio, auctor vitae, aliquet nec, imperdiet nec, leo. Morbi neque", null, null, "massa. Mauris vestibulum, neque sed dictum eleifend,", 50, false },
                    { 52, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Phasellus fermentum convallis ligula. Donec luctus aliquet odio. Etiam ligula tortor, dictum eu, placerat eget, venenatis a, magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Etiam laoreet, libero et tristique pellentesque, tellus sem mollis dui, in sodales elit erat vitae risus. Duis a mi fringilla mi lacinia mattis. Integer eu lacus. Quisque imperdiet, erat nonummy ultricies ornare, elit elit fermentum risus, at fringilla purus mauris a nunc. In at pede. Cras vulputate velit eu sem. Pellentesque ut ipsum ac mi eleifend egestas. Sed pharetra, felis eget varius ultrices, mauris ipsum porta elit, a feugiat tellus lorem eu", null, null, "at, velit.", 49, false },
                    { 26, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "nisi. Aenean eget metus. In nec orci. Donec nibh. Quisque nonummy ipsum non arcu. Vivamus sit amet risus. Donec egestas. Aliquam nec enim. Nunc ut erat. Sed nunc est, mollis non, cursus non, egestas a, dui. Cras pellentesque. Sed dictum. Proin", null, null, "ipsum leo", 49, false },
                    { 68, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "fringilla cursus purus. Nullam scelerisque neque sed sem egestas blandit. Nam nulla magna, malesuada vel, convallis in, cursus et, eros. Proin ultrices. Duis volutpat nunc sit amet metus. Aliquam erat volutpat. Nulla facilisis. Suspendisse commodo tincidunt nibh. Phasellus nulla. Integer vulputate, risus a ultricies adipiscing, enim mi tempor lorem, eget mollis lectus pede et", null, null, "dapibus", 55, false },
                    { 62, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Nunc ullamcorper, velit in aliquet lobortis, nisi nibh lacinia orci, consectetuer euismod est arcu ac", null, null, "Fusce fermentum fermentum arcu.", 48, false },
                    { 5, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "malesuada. Integer id magna et ipsum cursus vestibulum. Mauris magna. Duis dignissim tempor arcu. Vestibulum ut eros non enim commodo hendrerit. Donec porttitor tellus non magna. Nam ligula elit, pretium et, rutrum non, hendrerit id, ante. Nunc mauris sapien,", null, null, "Donec", 47, false },
                    { 79, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "ultrices sit amet, risus. Donec nibh enim, gravida sit amet, dapibus id, blandit at, nisi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin vel nisl. Quisque fringilla euismod enim. Etiam gravida molestie arcu. Sed eu nibh vulputate mauris sagittis placerat. Cras dictum ultricies ligula. Nullam enim. Sed nulla ante, iaculis nec, eleifend non, dapibus rutrum, justo. Praesent luctus. Curabitur egestas nunc sed libero.", null, null, "ante lectus", 45, false },
                    { 65, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "elit, dictum eu, eleifend nec, malesuada ut, sem. Nulla interdum. Curabitur dictum. Phasellus in felis. Nulla tempor augue ac ipsum. Phasellus vitae mauris sit amet lorem semper auctor. Mauris vel turpis. Aliquam adipiscing lobortis risus. In mi pede, nonummy ut, molestie in, tempus eu, ligula. Aenean euismod mauris eu elit. Nulla facilisi. Sed neque. Sed eget lacus. Mauris non dui nec urna suscipit", null, null, "lobortis", 45, false },
                    { 8, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "turpis. In condimentum. Donec at arcu. Vestibulum", null, null, "dui augue eu tellus. Phasellus elit", 45, false },
                    { 4, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "non nisi. Aenean eget metus. In nec orci. Donec nibh. Quisque nonummy ipsum non arcu. Vivamus sit amet risus. Donec egestas. Aliquam nec enim. Nunc ut erat. Sed nunc est, mollis non, cursus non, egestas a, dui. Cras pellentesque. Sed dictum. Proin eget odio. Aliquam vulputate ullamcorper magna. Sed eu eros. Nam consequat dolor vitae dolor. Donec fringilla. Donec feugiat metus sit amet ante. Vivamus non lorem vitae odio sagittis semper. Nam tempor diam dictum sapien. Aenean massa. Integer vitae nibh. Donec", null, null, "felis. Donec", 44, false },
                    { 64, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "metus urna convallis erat, eget", null, null, "in faucibus orci luctus", 42, false },
                    { 42, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis diam. Pellentesque habitant morbi tristique senectus et", null, null, "cursus et, eros. Proin", 41, false },
                    { 28, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "eu neque pellentesque massa lobortis ultrices. Vivamus rhoncus. Donec est. Nunc ullamcorper, velit in aliquet lobortis, nisi nibh lacinia orci, consectetuer euismod est arcu ac orci. Ut semper pretium neque. Morbi quis urna. Nunc quis arcu vel quam dignissim pharetra. Nam ac nulla. In tincidunt congue turpis. In condimentum. Donec", null, null, "dictum ultricies ligula. Nullam enim.", 48, false },
                    { 60, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Sed nec metus facilisis lorem tristique aliquet. Phasellus fermentum convallis ligula. Donec luctus aliquet odio. Etiam ligula tortor, dictum eu, placerat eget, venenatis a, magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Etiam laoreet, libero et tristique pellentesque, tellus sem mollis dui,", null, null, "Pellentesque", 40, false },
                    { 21, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "pharetra. Quisque ac libero nec ligula consectetuer rhoncus. Nullam velit dui, semper et, lacinia vitae, sodales at, velit. Pellentesque ultricies dignissim lacus. Aliquam rutrum lorem ac", null, null, "volutpat. Nulla dignissim.", 58, false },
                    { 46, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "erat vitae risus. Duis a mi fringilla mi lacinia mattis. Integer eu lacus. Quisque imperdiet, erat nonummy ultricies ornare, elit elit fermentum risus, at fringilla purus mauris a", null, null, "libero.", 61, false },
                    { 39, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "neque pellentesque massa lobortis ultrices. Vivamus rhoncus.", null, null, "Nunc mauris sapien,", 79, false },
                    { 27, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "auctor odio a purus. Duis elementum, dui quis accumsan convallis, ante lectus convallis est, vitae sodales nisi magna sed dui. Fusce aliquam, enim nec tempus scelerisque, lorem ipsum sodales purus, in molestie tortor nibh sit amet orci. Ut sagittis lobortis mauris. Suspendisse aliquet molestie tellus. Aenean", null, null, "quis accumsan convallis, ante lectus convallis", 78, false },
                    { 44, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "luctus aliquet odio. Etiam ligula tortor, dictum eu, placerat eget, venenatis a, magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Etiam laoreet, libero et tristique pellentesque, tellus sem mollis dui, in sodales elit erat vitae risus. Duis a mi fringilla mi lacinia mattis. Integer eu lacus. Quisque imperdiet, erat nonummy ultricies ornare, elit elit fermentum risus, at fringilla purus", null, null, "eleifend. Cras sed", 77, false },
                    { 72, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "cursus. Integer mollis. Integer tincidunt aliquam arcu. Aliquam ultrices iaculis odio. Nam interdum enim non nisi. Aenean eget metus. In nec orci. Donec nibh. Quisque nonummy ipsum non arcu. Vivamus sit amet risus. Donec egestas. Aliquam nec enim. Nunc ut erat. Sed nunc est,", null, null, "eleifend. Cras sed leo. Cras vehicula", 76, false },
                    { 23, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Fusce diam nunc, ullamcorper eu, euismod ac, fermentum vel, mauris. Integer sem elit, pharetra ut, pharetra sed, hendrerit a, arcu. Sed et libero. Proin mi. Aliquam gravida mauris ut mi. Duis risus odio, auctor vitae, aliquet", null, null, "mi. Aliquam gravida", 76, false },
                    { 78, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "volutpat. Nulla dignissim. Maecenas ornare egestas ligula. Nullam feugiat placerat velit. Quisque varius. Nam porttitor scelerisque neque. Nullam nisl. Maecenas malesuada fringilla est. Mauris eu turpis. Nulla aliquet. Proin velit. Sed malesuada augue ut lacus. Nulla tincidunt, neque vitae semper egestas, urna justo faucibus lectus, a sollicitudin orci sem eget massa. Suspendisse eleifend. Cras sed leo. Cras vehicula aliquet libero. Integer in magna. Phasellus dolor elit, pellentesque a, facilisis non, bibendum", null, null, "neque sed sem", 73, false },
                    { 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Integer tincidunt aliquam arcu. Aliquam ultrices iaculis odio. Nam interdum enim non nisi. Aenean eget metus. In nec orci. Donec nibh. Quisque nonummy ipsum non arcu. Vivamus sit amet risus. Donec egestas. Aliquam nec enim. Nunc ut erat. Sed nunc est, mollis non, cursus non, egestas a, dui. Cras pellentesque. Sed dictum. Proin eget odio. Aliquam vulputate ullamcorper magna. Sed eu eros. Nam consequat dolor", null, null, "ipsum primis", 73, false },
                    { 12, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "in molestie tortor nibh sit amet orci. Ut sagittis lobortis mauris. Suspendisse aliquet molestie tellus. Aenean egestas hendrerit neque. In ornare sagittis felis. Donec tempor, est ac mattis semper, dui lectus rutrum urna, nec luctus felis purus ac tellus. Suspendisse sed dolor. Fusce mi lorem, vehicula et, rutrum eu, ultrices sit amet, risus. Donec nibh enim, gravida sit amet, dapibus id, blandit at, nisi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur", null, null, "magna. Duis dignissim tempor", 59, false },
                    { 3, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "augue ut lacus. Nulla tincidunt, neque vitae semper egestas, urna justo faucibus lectus, a sollicitudin orci sem eget massa. Suspendisse eleifend. Cras sed leo. Cras vehicula aliquet libero. Integer in magna. Phasellus dolor elit, pellentesque a, facilisis non, bibendum sed, est. Nunc laoreet lectus quis massa. Mauris vestibulum, neque sed dictum eleifend, nunc risus varius orci, in consequat enim diam vel arcu. Curabitur ut odio vel est tempor bibendum. Donec felis orci, adipiscing non, luctus sit amet, faucibus ut, nulla. Cras eu tellus eu augue porttitor interdum. Sed auctor odio a purus. Duis elementum, dui quis accumsan convallis,", null, null, "molestie pharetra", 71, false },
                    { 19, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "conubia nostra, per inceptos hymenaeos. Mauris ut quam vel sapien imperdiet ornare. In faucibus. Morbi vehicula. Pellentesque tincidunt tempus risus. Donec egestas. Duis ac arcu. Nunc mauris. Morbi non sapien molestie orci tincidunt adipiscing. Mauris molestie pharetra nibh. Aliquam ornare, libero at auctor ullamcorper, nisl arcu iaculis enim, sit amet ornare lectus justo eu arcu. Morbi sit amet massa. Quisque porttitor eros nec tellus. Nunc lectus pede, ultrices a, auctor non, feugiat nec, diam. Duis mi enim, condimentum eget, volutpat ornare, facilisis eget, ipsum. Donec sollicitudin adipiscing ligula. Aenean gravida nunc sed pede. Cum", null, null, "Cum sociis natoque", 69, false },
                    { 70, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "ut eros non enim commodo hendrerit. Donec porttitor tellus non magna. Nam ligula elit, pretium et, rutrum non, hendrerit id, ante. Nunc mauris sapien, cursus in, hendrerit consectetuer, cursus et, magna. Praesent interdum ligula eu enim. Etiam imperdiet dictum magna. Ut tincidunt orci quis lectus. Nullam suscipit, est ac facilisis facilisis, magna tellus faucibus leo, in lobortis tellus justo sit amet nulla. Donec non justo. Proin non massa non ante bibendum ullamcorper. Duis cursus, diam at pretium aliquet, metus urna", null, null, "tellus justo sit amet", 68, false },
                    { 76, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "nec, cursus a, enim. Suspendisse aliquet, sem ut cursus luctus, ipsum leo elementum sem, vitae aliquam eros turpis non enim. Mauris quis turpis vitae purus gravida sagittis. Duis gravida. Praesent eu nulla at sem molestie sodales. Mauris blandit enim consequat purus. Maecenas libero est, congue a, aliquet vel, vulputate eu, odio. Phasellus at augue id ante dictum cursus. Nunc mauris elit, dictum eu, eleifend nec, malesuada ut, sem. Nulla interdum. Curabitur dictum. Phasellus in felis. Nulla tempor augue", null, null, "elit, pellentesque a, facilisis non,", 64, false },
                    { 48, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Nunc mauris sapien, cursus in, hendrerit consectetuer, cursus et, magna. Praesent interdum ligula eu enim. Etiam imperdiet dictum magna. Ut tincidunt orci quis lectus. Nullam suscipit, est ac facilisis facilisis, magna tellus faucibus leo, in lobortis tellus justo sit amet nulla. Donec", null, null, "nunc est,", 64, false },
                    { 34, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "lobortis risus. In mi pede, nonummy ut, molestie in, tempus eu, ligula. Aenean euismod mauris eu elit. Nulla facilisi. Sed neque. Sed eget lacus.", null, null, "ultrices a, auctor non, feugiat", 63, false },
                    { 36, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "magna. Ut tincidunt orci quis lectus. Nullam suscipit, est ac facilisis facilisis, magna tellus faucibus leo, in lobortis tellus justo sit amet nulla. Donec non justo. Proin non massa non ante bibendum ullamcorper. Duis cursus, diam at pretium aliquet, metus urna convallis erat, eget tincidunt dui augue eu tellus. Phasellus elit pede, malesuada vel, venenatis vel, faucibus id, libero. Donec consectetuer mauris id sapien. Cras dolor dolor, tempus non, lacinia at, iaculis quis, pede. Praesent eu dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur", null, null, "eget", 62, false },
                    { 57, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Cras dolor dolor, tempus non, lacinia at, iaculis quis, pede. Praesent eu dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean eget magna. Suspendisse tristique neque venenatis lacus. Etiam bibendum fermentum metus. Aenean sed pede nec ante blandit viverra. Donec tempus, lorem fringilla ornare placerat, orci lacus vestibulum lorem, sit amet ultricies sem magna nec quam. Curabitur vel lectus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec dignissim magna", null, null, "vestibulum", 61, false },
                    { 71, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Nunc mauris sapien, cursus in, hendrerit consectetuer, cursus et, magna. Praesent interdum ligula eu enim. Etiam imperdiet dictum magna. Ut tincidunt orci quis lectus. Nullam suscipit, est ac facilisis facilisis, magna tellus faucibus leo, in lobortis tellus justo", null, null, "eu, accumsan sed, facilisis", 69, false },
                    { 31, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "diam vel arcu. Curabitur ut odio vel est tempor bibendum. Donec felis orci, adipiscing non, luctus sit amet, faucibus ut, nulla. Cras eu tellus eu augue porttitor interdum. Sed auctor odio a purus. Duis elementum, dui quis accumsan convallis, ante lectus convallis est, vitae sodales nisi magna sed dui. Fusce aliquam, enim nec tempus scelerisque, lorem ipsum sodales purus, in molestie tortor nibh sit amet orci. Ut sagittis lobortis mauris. Suspendisse aliquet molestie tellus. Aenean egestas hendrerit neque. In ornare", null, null, "senectus et netus et malesuada fames", 40, false },
                    { 22, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "pellentesque. Sed dictum. Proin eget odio. Aliquam vulputate ullamcorper magna. Sed eu eros. Nam consequat dolor vitae dolor. Donec fringilla. Donec feugiat metus sit amet ante. Vivamus non lorem vitae odio sagittis semper. Nam tempor diam dictum sapien. Aenean massa. Integer vitae nibh.", null, null, "egestas a, dui. Cras pellentesque.", 39, false },
                    { 11, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "ornare, libero at auctor ullamcorper, nisl arcu iaculis enim, sit amet ornare lectus justo eu arcu. Morbi sit amet massa. Quisque porttitor eros nec tellus. Nunc lectus pede, ultrices a, auctor non, feugiat nec, diam. Duis mi enim, condimentum eget, volutpat ornare, facilisis eget, ipsum. Donec sollicitudin adipiscing ligula. Aenean gravida nunc sed pede. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus", null, null, "felis", 39, false },
                    { 43, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "ante blandit viverra. Donec tempus, lorem fringilla ornare placerat, orci lacus vestibulum lorem, sit amet ultricies sem magna nec quam. Curabitur vel lectus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec dignissim magna a tortor. Nunc commodo auctor velit. Aliquam nisl. Nulla eu neque pellentesque massa lobortis ultrices. Vivamus rhoncus. Donec est. Nunc ullamcorper, velit in aliquet lobortis, nisi nibh lacinia orci, consectetuer euismod est arcu ac orci. Ut semper pretium neque. Morbi quis urna. Nunc quis arcu vel quam dignissim pharetra. Nam ac nulla. In tincidunt congue", null, null, "eu tellus eu", 19, false }
                });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "Id", "CreatedOn", "Description", "ModifiedOn", "Photo", "Title", "UserId", "isDeleted" },
                values: new object[,]
                {
                    { 41, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "eu dolor egestas rhoncus. Proin nisl sem, consequat nec, mollis vitae, posuere at, velit. Cras lorem lorem, luctus ut, pellentesque eget, dictum", null, null, "scelerisque mollis. Phasellus libero mauris, aliquam", 19, false },
                    { 61, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "enim consequat purus. Maecenas libero est, congue a, aliquet vel, vulputate eu, odio. Phasellus at augue id ante dictum cursus. Nunc mauris elit, dictum eu, eleifend nec, malesuada ut, sem. Nulla interdum. Curabitur dictum. Phasellus in felis. Nulla tempor augue ac ipsum. Phasellus vitae mauris sit amet lorem semper auctor. Mauris vel turpis. Aliquam adipiscing lobortis risus. In mi pede, nonummy ut, molestie in, tempus eu, ligula. Aenean euismod mauris eu elit. Nulla facilisi.", null, null, "diam at pretium aliquet,", 17, false },
                    { 47, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "et netus et malesuada fames ac turpis egestas. Fusce aliquet magna a neque. Nullam ut nisi a odio semper cursus. Integer mollis. Integer tincidunt aliquam arcu. Aliquam ultrices iaculis odio. Nam interdum enim non nisi. Aenean eget", null, null, "Quisque ac libero nec ligula", 17, false },
                    { 10, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "egestas.", null, null, "rhoncus id, mollis nec, cursus", 17, false },
                    { 14, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "in molestie tortor nibh sit amet orci. Ut sagittis lobortis", null, null, "a felis ullamcorper viverra. Maecenas", 14, false },
                    { 32, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "nibh. Quisque nonummy ipsum non arcu. Vivamus sit amet risus. Donec egestas. Aliquam nec enim. Nunc ut erat. Sed nunc est, mollis non, cursus non, egestas a, dui. Cras pellentesque. Sed dictum. Proin eget odio. Aliquam vulputate ullamcorper magna. Sed eu eros. Nam consequat dolor vitae dolor. Donec fringilla. Donec feugiat metus sit amet", null, null, "malesuada fames ac turpis egestas.", 13, false },
                    { 54, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "lorem ut aliquam iaculis, lacus pede sagittis augue, eu", null, null, "ante. Nunc mauris sapien, cursus in, hendrerit", 19, false },
                    { 67, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "pede nec ante blandit viverra. Donec tempus, lorem fringilla ornare placerat, orci lacus vestibulum lorem, sit amet ultricies sem magna nec quam. Curabitur", null, null, "Sed congue, elit sed", 10, false },
                    { 25, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "diam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce aliquet magna a neque. Nullam ut nisi a odio semper cursus. Integer mollis. Integer tincidunt aliquam arcu. Aliquam ultrices iaculis odio. Nam interdum enim non nisi. Aenean eget metus. In nec orci. Donec nibh. Quisque nonummy ipsum non arcu. Vivamus sit amet risus. Donec egestas. Aliquam nec enim. Nunc ut erat. Sed nunc", null, null, "Donec feugiat metus sit amet", 9, false },
                    { 59, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "facilisi. Sed neque. Sed eget lacus. Mauris non dui nec urna suscipit nonummy. Fusce fermentum fermentum arcu. Vestibulum", null, null, "nec urna et arcu imperdiet ullamcorper.", 6, false },
                    { 7, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "amet, faucibus ut, nulla. Cras eu tellus eu augue porttitor", null, null, "pellentesque. Sed", 5, false },
                    { 75, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "nibh. Quisque nonummy ipsum non arcu. Vivamus sit amet risus. Donec egestas. Aliquam nec enim. Nunc ut erat. Sed nunc est, mollis non, cursus non, egestas a, dui.", null, null, "nibh. Aliquam ornare,", 3, false },
                    { 77, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis diam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce aliquet magna a neque. Nullam ut nisi a odio semper cursus. Integer mollis. Integer tincidunt aliquam arcu. Aliquam ultrices iaculis odio. Nam interdum", null, null, "venenatis vel, faucibus id, libero. Donec", 2, false },
                    { 66, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "eu, odio. Phasellus at augue id ante dictum cursus. Nunc mauris elit, dictum eu, eleifend nec, malesuada ut, sem. Nulla interdum. Curabitur dictum. Phasellus in felis. Nulla tempor augue ac ipsum. Phasellus vitae mauris sit amet lorem semper auctor. Mauris vel turpis. Aliquam adipiscing lobortis risus. In mi pede, nonummy ut, molestie in, tempus eu, ligula. Aenean euismod mauris eu elit. Nulla facilisi. Sed neque. Sed eget lacus. Mauris non dui nec urna suscipit nonummy. Fusce fermentum fermentum arcu. Vestibulum", null, null, "dapibus ligula.", 2, false },
                    { 40, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "mollis. Integer tincidunt aliquam arcu. Aliquam ultrices iaculis odio. Nam interdum enim non nisi. Aenean eget metus. In nec orci. Donec nibh. Quisque nonummy ipsum non arcu. Vivamus sit amet risus. Donec egestas. Aliquam nec enim. Nunc ut erat. Sed nunc est, mollis non, cursus non, egestas a, dui. Cras", null, null, "hendrerit neque. In ornare sagittis felis. Donec", 2, false },
                    { 45, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "mus. Aenean eget magna. Suspendisse tristique neque venenatis lacus. Etiam bibendum fermentum metus. Aenean sed pede nec ante blandit viverra. Donec tempus, lorem fringilla ornare placerat, orci lacus vestibulum lorem, sit amet ultricies sem", null, null, "felis orci, adipiscing non, luctus", 10, false },
                    { 73, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "felis.", null, null, "arcu et pede. Nunc", 21, false },
                    { 58, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "mi lorem, vehicula et, rutrum eu, ultrices sit amet, risus. Donec nibh enim, gravida sit amet, dapibus id, blandit at, nisi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin vel nisl. Quisque fringilla euismod enim. Etiam gravida molestie arcu. Sed eu nibh vulputate mauris sagittis placerat. Cras dictum ultricies ligula. Nullam enim. Sed nulla ante, iaculis nec, eleifend non, dapibus rutrum, justo. Praesent luctus. Curabitur egestas nunc sed libero. Proin sed turpis nec mauris", null, null, "porttitor", 22, false },
                    { 6, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "faucibus id, libero. Donec consectetuer mauris id sapien. Cras dolor dolor, tempus non, lacinia at, iaculis quis, pede. Praesent eu dui. Cum sociis natoque penatibus et magnis", null, null, "vitae mauris", 23, false },
                    { 49, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "purus. Nullam scelerisque neque sed sem egestas blandit. Nam nulla magna, malesuada vel, convallis in, cursus et, eros. Proin ultrices. Duis volutpat nunc sit amet metus. Aliquam erat volutpat. Nulla facilisis. Suspendisse commodo tincidunt nibh. Phasellus nulla. Integer vulputate, risus a ultricies adipiscing, enim mi tempor lorem, eget mollis lectus pede et risus. Quisque libero lacus, varius et, euismod et, commodo at, libero. Morbi accumsan laoreet ipsum. Curabitur consequat, lectus sit amet luctus vulputate, nisi sem semper erat, in consectetuer ipsum nunc id enim. Curabitur massa. Vestibulum accumsan neque et nunc. Quisque", null, null, "cursus in,", 38, false },
                    { 74, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "risus. Nunc ac sem ut dolor dapibus gravida. Aliquam tincidunt, nunc ac mattis ornare, lectus ante dictum mi, ac mattis velit justo nec ante. Maecenas mi felis, adipiscing fringilla, porttitor vulputate, posuere vulputate, lacus. Cras interdum. Nunc sollicitudin commodo ipsum. Suspendisse non leo. Vivamus nibh dolor, nonummy ac, feugiat non, lobortis quis, pede. Suspendisse dui. Fusce diam nunc, ullamcorper eu, euismod ac, fermentum", null, null, "ante", 37, false },
                    { 51, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "ligula. Aliquam erat volutpat. Nulla dignissim. Maecenas ornare egestas ligula. Nullam feugiat placerat velit. Quisque varius. Nam porttitor scelerisque neque. Nullam nisl. Maecenas malesuada fringilla est. Mauris eu turpis. Nulla aliquet. Proin velit. Sed malesuada augue ut lacus. Nulla tincidunt, neque vitae semper egestas, urna justo faucibus lectus, a sollicitudin orci sem eget massa. Suspendisse eleifend. Cras sed leo. Cras vehicula aliquet libero. Integer in magna. Phasellus dolor elit,", null, null, "Mauris magna. Duis dignissim tempor arcu.", 36, false },
                    { 20, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "fermentum metus. Aenean sed pede nec ante blandit viverra. Donec tempus, lorem fringilla ornare placerat, orci lacus vestibulum lorem, sit amet ultricies sem magna nec quam.", null, null, "euismod mauris eu elit.", 33, false },
                    { 18, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "egestas. Sed pharetra, felis eget varius ultrices, mauris ipsum porta elit, a feugiat tellus lorem eu metus. In lorem. Donec elementum, lorem ut aliquam iaculis, lacus pede sagittis augue, eu tempor erat neque non quam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam fringilla cursus purus. Nullam scelerisque neque sed sem egestas blandit. Nam nulla magna, malesuada vel, convallis in, cursus et, eros. Proin ultrices. Duis volutpat nunc sit amet metus. Aliquam erat volutpat. Nulla facilisis. Suspendisse commodo tincidunt nibh. Phasellus nulla. Integer vulputate, risus a ultricies adipiscing, enim", null, null, "egestas. Aliquam fringilla cursus purus. Nullam scelerisque", 33, false },
                    { 15, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "aliquet, metus urna convallis erat, eget tincidunt dui augue eu tellus. Phasellus elit pede, malesuada vel, venenatis vel, faucibus id, libero. Donec consectetuer mauris id sapien. Cras dolor dolor, tempus non, lacinia at, iaculis quis, pede. Praesent eu dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean eget magna. Suspendisse tristique neque venenatis lacus. Etiam bibendum fermentum metus. Aenean sed pede nec ante blandit viverra. Donec tempus, lorem fringilla", null, null, "dis parturient montes, nascetur ridiculus mus.", 33, false },
                    { 37, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "at, iaculis quis, pede. Praesent eu dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean eget magna. Suspendisse tristique neque venenatis lacus. Etiam bibendum fermentum metus. Aenean sed pede nec ante blandit viverra. Donec tempus, lorem fringilla ornare placerat, orci lacus vestibulum lorem, sit amet ultricies sem magna nec quam. Curabitur vel lectus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec dignissim magna a tortor. Nunc commodo auctor velit. Aliquam nisl. Nulla eu neque pellentesque massa lobortis", null, null, "Nullam nisl. Maecenas malesuada fringilla est.", 32, false },
                    { 35, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "tincidunt aliquam arcu. Aliquam ultrices iaculis odio. Nam interdum enim non nisi. Aenean eget metus. In nec orci. Donec nibh. Quisque nonummy ipsum non arcu. Vivamus sit amet risus. Donec egestas. Aliquam nec enim. Nunc ut erat. Sed nunc est, mollis non, cursus non, egestas a, dui. Cras pellentesque. Sed dictum. Proin eget odio. Aliquam vulputate ullamcorper magna. Sed eu eros. Nam consequat dolor vitae dolor. Donec fringilla. Donec feugiat metus sit amet ante. Vivamus non lorem vitae odio sagittis semper.", null, null, "ultricies", 31, false },
                    { 56, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "cursus luctus, ipsum leo elementum sem, vitae aliquam eros turpis non enim. Mauris quis turpis vitae purus gravida sagittis. Duis gravida. Praesent eu nulla at sem molestie sodales. Mauris blandit enim consequat purus. Maecenas libero est, congue a, aliquet vel, vulputate eu, odio. Phasellus at augue id ante dictum cursus. Nunc mauris elit, dictum eu, eleifend nec, malesuada ut, sem. Nulla interdum. Curabitur dictum. Phasellus in felis.", null, null, "facilisis facilisis,", 30, false },
                    { 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis diam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce", null, null, "nisl. Maecenas malesuada fringilla est.", 30, false },
                    { 63, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "hendrerit id, ante. Nunc mauris sapien, cursus in, hendrerit consectetuer, cursus et, magna. Praesent interdum ligula eu enim. Etiam imperdiet dictum magna. Ut tincidunt orci quis lectus. Nullam suscipit, est ac facilisis facilisis, magna tellus faucibus leo, in lobortis tellus justo", null, null, "lorem", 28, false },
                    { 29, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Nullam enim. Sed nulla ante, iaculis nec, eleifend non, dapibus rutrum, justo. Praesent luctus. Curabitur egestas nunc sed libero. Proin sed turpis nec mauris blandit mattis. Cras eget nisi dictum augue malesuada malesuada. Integer id magna et ipsum cursus vestibulum. Mauris magna. Duis dignissim tempor arcu. Vestibulum ut eros non enim commodo hendrerit. Donec porttitor tellus non magna. Nam ligula elit, pretium et, rutrum non, hendrerit", null, null, "natoque penatibus et magnis", 27, false },
                    { 13, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "tincidunt tempus risus. Donec egestas. Duis ac arcu. Nunc mauris. Morbi non sapien molestie orci tincidunt adipiscing. Mauris molestie pharetra nibh. Aliquam ornare, libero at auctor ullamcorper, nisl arcu iaculis enim, sit amet ornare lectus justo eu arcu. Morbi sit amet massa. Quisque porttitor eros nec tellus. Nunc lectus pede, ultrices a, auctor non, feugiat nec, diam.", null, null, "nisl. Nulla eu neque pellentesque massa lobortis", 27, false },
                    { 69, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin vel nisl. Quisque fringilla euismod enim. Etiam gravida molestie arcu. Sed eu", null, null, "mauris", 26, false },
                    { 80, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "magna. Phasellus dolor elit, pellentesque a, facilisis non, bibendum sed, est. Nunc laoreet lectus quis massa. Mauris vestibulum, neque sed dictum eleifend, nunc risus varius orci, in consequat enim diam vel arcu. Curabitur ut odio vel est tempor bibendum. Donec felis orci, adipiscing non, luctus sit amet, faucibus ut, nulla. Cras eu tellus eu augue porttitor interdum. Sed auctor odio a purus. Duis elementum, dui quis accumsan convallis, ante lectus convallis est, vitae sodales nisi magna sed dui. Fusce aliquam,", null, null, "tellus eu augue porttitor interdum. Sed", 24, false },
                    { 16, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "arcu. Vivamus sit amet risus. Donec egestas. Aliquam nec enim. Nunc ut erat. Sed nunc est, mollis non, cursus non, egestas a, dui. Cras pellentesque. Sed dictum. Proin eget odio. Aliquam vulputate ullamcorper magna. Sed eu eros. Nam consequat dolor vitae dolor. Donec fringilla. Donec feugiat metus sit amet ante. Vivamus non lorem vitae odio sagittis semper. Nam tempor diam dictum sapien. Aenean massa. Integer vitae nibh. Donec est mauris, rhoncus id, mollis nec, cursus a, enim. Suspendisse aliquet, sem ut cursus luctus, ipsum leo elementum", null, null, "urna. Vivamus molestie dapibus ligula.", 23, false },
                    { 9, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Fusce feugiat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam auctor, velit eget laoreet posuere, enim nisl elementum purus, accumsan", null, null, "et, lacinia", 23, false },
                    { 50, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "ante dictum mi, ac mattis velit justo nec ante. Maecenas mi felis, adipiscing fringilla, porttitor vulputate, posuere vulputate, lacus. Cras interdum. Nunc sollicitudin commodo ipsum. Suspendisse non leo. Vivamus nibh dolor, nonummy ac, feugiat non, lobortis quis, pede. Suspendisse dui. Fusce diam nunc, ullamcorper eu, euismod ac, fermentum vel, mauris. Integer sem elit, pharetra ut, pharetra sed, hendrerit a, arcu. Sed et libero. Proin mi. Aliquam gravida mauris ut mi. Duis risus odio, auctor vitae, aliquet nec, imperdiet nec, leo. Morbi neque tellus, imperdiet non,", null, null, "tempor augue ac ipsum. Phasellus", 79, false },
                    { 53, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "neque. Sed eget lacus. Mauris non dui nec urna suscipit nonummy. Fusce fermentum fermentum arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae Phasellus ornare. Fusce mollis. Duis sit amet diam eu dolor egestas rhoncus. Proin", null, null, "vitae sodales", 79, false }
                });

            migrationBuilder.InsertData(
                table: "Comments",
                columns: new[] { "Id", "Content", "CreatedOn", "ModifiedOn", "PostId", "UserId", "isDeleted" },
                values: new object[,]
                {
                    { 3, "a, dui. Cras pellentesque. Sed dictum. Proin eget odio. Aliquam vulputate ullamcorper magna. Sed eu eros. Nam consequat dolor vitae dolor. Donec fringilla. Donec feugiat metus sit amet ante. Vivamus non lorem vitae odio sagittis semper. Nam", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2552), null, 24, 49, false },
                    { 10, "Integer sem elit, pharetra ut, pharetra sed, hendrerit a, arcu. Sed et libero. Proin mi. Aliquam gravida mauris ut mi. Duis risus odio, auctor vitae, aliquet nec, imperdiet", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2618), null, 35, 25, false },
                    { 50, "a, dui. Cras pellentesque. Sed dictum. Proin eget odio. Aliquam vulputate ullamcorper magna. Sed eu eros. Nam consequat dolor vitae dolor. Donec fringilla. Donec feugiat metus sit amet ante. Vivamus non lorem vitae odio sagittis semper. Nam tempor diam dictum sapien. Aenean massa. Integer vitae nibh. Donec", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2959), null, 3, 55, false },
                    { 55, "faucibus", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(3001), null, 3, 3, false },
                    { 76, "mattis semper, dui lectus rutrum urna, nec luctus felis purus ac tellus. Suspendisse sed dolor. Fusce mi lorem, vehicula et, rutrum eu, ultrices sit amet,", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(3195), null, 3, 38, false },
                    { 65, "convallis est, vitae sodales nisi magna sed dui. Fusce aliquam, enim nec tempus scelerisque, lorem ipsum sodales purus, in molestie tortor nibh sit amet orci. Ut sagittis lobortis mauris. Suspendisse aliquet molestie tellus. Aenean egestas hendrerit neque. In ornare sagittis felis.", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(3083), null, 63, 22, false },
                    { 47, "Morbi accumsan laoreet ipsum. Curabitur consequat, lectus", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2933), null, 29, 21, false },
                    { 40, "tincidunt aliquam arcu. Aliquam ultrices iaculis odio. Nam interdum enim non nisi. Aenean eget metus. In", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2874), null, 29, 15, false },
                    { 16, "luctus et ultrices posuere cubilia Curae Phasellus ornare. Fusce mollis. Duis sit amet diam eu dolor", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2669), null, 13, 6, false },
                    { 29, "luctus felis purus ac tellus. Suspendisse sed dolor. Fusce mi lorem, vehicula et, rutrum eu, ultrices sit amet, risus. Donec nibh enim, gravida sit amet, dapibus id, blandit at, nisi. Cum sociis natoque penatibus et magnis dis", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2781), null, 16, 5, false },
                    { 28, "parturient montes, nascetur ridiculus mus. Proin vel nisl. Quisque fringilla euismod enim. Etiam", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2772), null, 16, 1, false },
                    { 2, "metus. In lorem. Donec elementum, lorem ut aliquam iaculis, lacus pede sagittis augue, eu tempor erat neque non quam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam fringilla cursus purus. Nullam", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2372), null, 16, 65, false },
                    { 18, "Ut sagittis lobortis mauris. Suspendisse", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2685), null, 9, 53, false },
                    { 41, "libero et tristique pellentesque, tellus sem mollis", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2883), null, 6, 61, false },
                    { 79, "Aliquam erat volutpat. Nulla dignissim. Maecenas ornare egestas ligula. Nullam feugiat placerat velit. Quisque varius. Nam porttitor scelerisque neque. Nullam nisl. Maecenas malesuada fringilla est. Mauris eu turpis. Nulla aliquet.", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(3219), null, 23, 41, false },
                    { 68, "at, velit. Cras lorem lorem, luctus ut, pellentesque eget, dictum placerat, augue. Sed molestie. Sed id risus quis diam luctus lobortis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Mauris ut quam vel sapien imperdiet ornare. In faucibus. Morbi vehicula. Pellentesque", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(3107), null, 58, 10, false },
                    { 49, "Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2951), null, 58, 13, false },
                    { 38, "mattis ornare, lectus ante dictum mi, ac mattis velit justo nec ante. Maecenas mi felis, adipiscing fringilla, porttitor vulputate, posuere vulputate, lacus. Cras interdum. Nunc sollicitudin", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2858), null, 58, 64, false },
                    { 33, "sit amet, faucibus ut, nulla. Cras eu tellus eu augue porttitor interdum. Sed auctor odio a", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2814), null, 58, 9, false },
                    { 42, "lorem eu metus. In", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2891), null, 73, 14, false },
                    { 66, "mus. Donec dignissim magna a tortor. Nunc commodo auctor velit. Aliquam nisl. Nulla eu neque pellentesque massa lobortis ultrices. Vivamus rhoncus. Donec est. Nunc ullamcorper,", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(3091), null, 43, 26, false },
                    { 39, "Donec luctus aliquet odio. Etiam ligula tortor, dictum eu, placerat eget, venenatis a, magna. Lorem ipsum dolor sit", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2866), null, 43, 70, false },
                    { 4, "et risus. Quisque libero lacus, varius et, euismod et, commodo at,", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2568), null, 43, 13, false },
                    { 51, "mauris elit, dictum eu, eleifend nec, malesuada ut, sem. Nulla interdum. Curabitur dictum. Phasellus in felis. Nulla tempor augue ac ipsum. Phasellus vitae mauris sit amet lorem semper auctor. Mauris", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2967), null, 41, 51, false },
                    { 7, "Integer vulputate, risus a ultricies adipiscing, enim mi tempor lorem, eget mollis lectus pede et risus. Quisque", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2594), null, 41, 27, false },
                    { 67, "Maecenas malesuada fringilla est. Mauris eu turpis. Nulla aliquet. Proin velit. Sed malesuada augue ut lacus. Nulla tincidunt, neque vitae semper egestas, urna justo faucibus lectus, a sollicitudin orci sem eget massa. Suspendisse eleifend. Cras sed leo. Cras vehicula aliquet libero. Integer in magna. Phasellus dolor", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(3099), null, 72, 58, false },
                    { 45, "magna. Phasellus dolor elit, pellentesque a, facilisis non, bibendum sed, est. Nunc laoreet lectus quis massa. Mauris vestibulum, neque sed dictum eleifend, nunc risus varius orci, in consequat enim diam vel arcu. Curabitur ut odio vel est", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2916), null, 47, 34, false },
                    { 53, "Proin mi. Aliquam gravida mauris ut mi. Duis risus odio, auctor vitae, aliquet nec, imperdiet nec, leo. Morbi neque tellus, imperdiet", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2984), null, 10, 77, false },
                    { 31, "a neque. Nullam ut nisi a odio semper cursus. Integer mollis. Integer tincidunt aliquam arcu. Aliquam ultrices iaculis odio. Nam interdum enim non nisi. Aenean eget metus. In nec orci. Donec nibh. Quisque nonummy ipsum non arcu. Vivamus sit amet", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2798), null, 35, 50, false },
                    { 6, "fermentum risus, at fringilla purus mauris a nunc. In at pede. Cras vulputate velit eu sem. Pellentesque ut ipsum ac mi eleifend egestas.", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2586), null, 37, 69, false },
                    { 48, "purus, accumsan", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2943), null, 15, 29, false },
                    { 58, "at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis diam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(3025), null, 70, 8, false },
                    { 26, "augue, eu tempor erat neque non quam.", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2754), null, 55, 17, false },
                    { 64, "mi tempor lorem, eget mollis lectus pede et risus. Quisque libero lacus, varius et, euismod et, commodo at, libero. Morbi accumsan laoreet ipsum. Curabitur consequat, lectus sit amet luctus vulputate, nisi sem semper erat, in consectetuer ipsum nunc id enim. Curabitur massa. Vestibulum accumsan neque et", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(3075), null, 68, 14, false },
                    { 75, "vel nisl. Quisque fringilla euismod enim.", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(3187), null, 68, 6, false },
                    { 56, "Sed neque. Sed eget lacus. Mauris non dui nec urna suscipit nonummy. Fusce fermentum fermentum arcu.", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(3009), null, 33, 35, false },
                    { 80, "eu, eleifend nec, malesuada ut, sem. Nulla interdum. Curabitur dictum. Phasellus in", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(3227), null, 17, 13, false },
                    { 77, "morbi tristique senectus", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(3203), null, 17, 10, false },
                    { 17, "turpis vitae purus gravida sagittis. Duis gravida. Praesent eu nulla at sem molestie sodales. Mauris blandit enim consequat purus. Maecenas libero est, congue a, aliquet vel, vulputate eu, odio. Phasellus at augue id ante dictum", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2677), null, 17, 41, false },
                    { 44, "enim non nisi. Aenean eget metus. In nec orci. Donec nibh.", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2907), null, 52, 78, false },
                    { 8, "suscipit, est ac facilisis facilisis, magna", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2602), null, 52, 25, false },
                    { 23, "augue id ante dictum cursus. Nunc mauris elit, dictum eu, eleifend nec, malesuada ut, sem. Nulla interdum.", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2728), null, 12, 56, false }
                });

            migrationBuilder.InsertData(
                table: "Comments",
                columns: new[] { "Id", "Content", "CreatedOn", "ModifiedOn", "PostId", "UserId", "isDeleted" },
                values: new object[,]
                {
                    { 21, "sed leo. Cras vehicula aliquet libero. Integer in magna. Phasellus dolor elit, pellentesque a, facilisis non, bibendum sed, est. Nunc laoreet lectus quis massa. Mauris vestibulum, neque sed dictum", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2711), null, 28, 39, false },
                    { 22, "eleifend nec, malesuada ut, sem. Nulla interdum. Curabitur dictum. Phasellus in felis. Nulla tempor augue ac ipsum. Phasellus vitae mauris sit amet lorem semper auctor. Mauris vel turpis. Aliquam adipiscing lobortis risus. In mi pede, nonummy ut, molestie in, tempus eu, ligula. Aenean", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2720), null, 57, 43, false },
                    { 27, "dolor dolor, tempus non, lacinia at, iaculis quis, pede. Praesent eu dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean eget", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2763), null, 57, 23, false },
                    { 24, "sagittis augue, eu tempor erat neque non quam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam fringilla cursus purus. Nullam scelerisque neque sed sem egestas blandit. Nam nulla magna, malesuada vel, convallis in,", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2736), null, 10, 3, false },
                    { 12, "senectus", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2635), null, 65, 74, false },
                    { 59, "id nunc interdum feugiat. Sed nec metus facilisis lorem tristique aliquet. Phasellus fermentum convallis ligula. Donec luctus aliquet odio. Etiam ligula tortor, dictum eu, placerat eget, venenatis a, magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Etiam laoreet, libero et tristique pellentesque, tellus sem mollis", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(3033), null, 57, 66, false },
                    { 19, "dignissim magna a tortor. Nunc commodo auctor velit. Aliquam nisl. Nulla eu neque pellentesque massa lobortis ultrices. Vivamus rhoncus. Donec est. Nunc ullamcorper, velit in aliquet lobortis, nisi nibh lacinia orci, consectetuer euismod est arcu ac orci. Ut semper pretium neque.", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2694), null, 8, 79, false },
                    { 71, "ac turpis egestas. Fusce aliquet magna a neque. Nullam ut nisi a odio semper cursus. Integer mollis. Integer tincidunt aliquam arcu. Aliquam ultrices iaculis odio. Nam interdum enim non nisi. Aenean eget metus. In nec orci. Donec nibh.", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(3151), null, 64, 6, false },
                    { 70, "ridiculus mus. Aenean eget magna. Suspendisse tristique neque venenatis lacus. Etiam bibendum fermentum metus. Aenean sed pede nec ante blandit viverra. Donec tempus, lorem fringilla ornare placerat, orci lacus vestibulum lorem, sit amet ultricies sem magna nec quam. Curabitur vel lectus. Cum sociis natoque penatibus et magnis", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(3124), null, 64, 18, false },
                    { 36, "justo eu arcu. Morbi sit amet massa. Quisque porttitor eros nec tellus. Nunc lectus pede, ultrices a, auctor non, feugiat nec, diam. Duis mi enim, condimentum eget, volutpat ornare, facilisis eget, ipsum. Donec sollicitudin adipiscing ligula. Aenean gravida nunc sed pede. Cum sociis", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2840), null, 64, 52, false },
                    { 15, "molestie orci tincidunt adipiscing. Mauris molestie pharetra nibh. Aliquam ornare, libero at auctor ullamcorper, nisl arcu iaculis enim, sit amet ornare lectus justo eu arcu. Morbi sit amet massa. Quisque porttitor eros nec tellus. Nunc lectus", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2661), null, 34, 22, false },
                    { 63, "sem molestie sodales. Mauris blandit enim consequat purus. Maecenas libero est, congue a, aliquet vel, vulputate eu, odio. Phasellus at augue id ante dictum cursus. Nunc mauris elit, dictum eu, eleifend nec, malesuada ut, sem. Nulla interdum. Curabitur", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(3066), null, 60, 27, false },
                    { 5, "Sed malesuada augue ut lacus. Nulla tincidunt, neque vitae semper egestas, urna justo faucibus lectus, a sollicitudin orci sem eget massa. Suspendisse eleifend. Cras sed leo. Cras vehicula aliquet libero. Integer", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2577), null, 48, 48, false },
                    { 1, "at, iaculis quis, pede. Praesent eu dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean eget magna. Suspendisse tristique neque", new DateTime(2023, 4, 13, 15, 55, 31, 590, DateTimeKind.Local).AddTicks(58), null, 31, 75, false },
                    { 62, "torquent per", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(3058), null, 20, 63, false },
                    { 32, "suscipit, est ac facilisis facilisis, magna tellus faucibus leo, in lobortis tellus justo sit amet nulla. Donec non justo. Proin non massa non ante bibendum ullamcorper. Duis cursus, diam at pretium aliquet, metus urna convallis erat, eget tincidunt dui", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2806), null, 70, 23, false },
                    { 54, "Fusce mollis. Duis sit amet diam eu dolor egestas rhoncus. Proin nisl sem,", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2993), null, 18, 34, false },
                    { 9, "Aliquam ornare, libero at auctor ullamcorper, nisl arcu iaculis enim, sit amet ornare lectus justo eu arcu. Morbi sit amet massa. Quisque porttitor eros nec tellus. Nunc lectus pede, ultrices a, auctor non, feugiat nec, diam.", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2610), null, 18, 57, false },
                    { 11, "Donec tempus, lorem fringilla ornare placerat,", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2627), null, 65, 6, false },
                    { 72, "sodales at, velit. Pellentesque ultricies dignissim lacus. Aliquam rutrum lorem ac risus. Morbi metus. Vivamus euismod urna. Nullam lobortis quam a felis ullamcorper viverra. Maecenas iaculis aliquet diam. Sed diam", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(3161), null, 14, 22, false },
                    { 25, "mollis. Duis sit amet diam eu dolor egestas rhoncus. Proin nisl sem, consequat nec, mollis vitae, posuere at, velit. Cras lorem lorem, luctus ut, pellentesque eget, dictum placerat, augue. Sed molestie. Sed id risus quis diam luctus lobortis. Class aptent taciti sociosqu ad litora torquent per", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2744), null, 63, 13, false },
                    { 13, "ante dictum mi, ac mattis velit justo nec ante. Maecenas mi felis, adipiscing fringilla, porttitor vulputate, posuere vulputate, lacus. Cras interdum. Nunc sollicitudin commodo ipsum. Suspendisse non leo. Vivamus nibh dolor, nonummy", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2644), null, 67, 46, false },
                    { 35, "Proin nisl sem, consequat nec, mollis vitae, posuere at, velit. Cras lorem lorem, luctus ut, pellentesque eget, dictum placerat, augue. Sed molestie. Sed id risus quis diam luctus lobortis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Mauris ut quam vel sapien imperdiet", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2832), null, 7, 65, false },
                    { 52, "ullamcorper viverra. Maecenas iaculis aliquet diam. Sed diam lorem, auctor quis, tristique ac, eleifend vitae, erat. Vivamus nisi. Mauris nulla. Integer urna. Vivamus molestie dapibus ligula. Aliquam erat volutpat. Nulla dignissim. Maecenas ornare egestas ligula. Nullam feugiat placerat velit. Quisque varius. Nam porttitor scelerisque neque. Nullam", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2976), null, 75, 62, false },
                    { 74, "montes, nascetur ridiculus mus. Aenean eget", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(3179), null, 45, 72, false },
                    { 43, "rutrum urna, nec luctus felis purus ac tellus. Suspendisse sed dolor. Fusce mi lorem, vehicula et, rutrum eu, ultrices sit amet, risus. Donec nibh enim, gravida sit amet, dapibus id, blandit at, nisi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2899), null, 39, 11, false },
                    { 30, "sit amet ultricies sem magna nec quam. Curabitur vel lectus. Cum sociis natoque penatibus", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2790), null, 39, 76, false },
                    { 46, "nulla. Donec non justo. Proin non massa non ante bibendum ullamcorper. Duis cursus, diam at pretium aliquet, metus urna convallis erat, eget tincidunt dui augue eu tellus. Phasellus elit pede, malesuada vel,", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2925), null, 77, 71, false },
                    { 61, "quis, tristique ac, eleifend vitae, erat. Vivamus nisi. Mauris nulla. Integer urna. Vivamus molestie dapibus ligula. Aliquam erat volutpat. Nulla dignissim. Maecenas ornare egestas ligula. Nullam feugiat placerat velit. Quisque varius. Nam porttitor scelerisque neque. Nullam nisl. Maecenas malesuada fringilla est. Mauris eu turpis. Nulla aliquet. Proin velit.", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(3050), null, 75, 19, false },
                    { 57, "turpis vitae", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(3017), null, 45, 72, false },
                    { 73, "Curabitur consequat, lectus sit amet luctus vulputate, nisi sem semper erat, in", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(3170), null, 25, 28, false },
                    { 60, "id magna et ipsum cursus vestibulum. Mauris magna. Duis dignissim tempor arcu. Vestibulum ut eros non enim commodo hendrerit. Donec porttitor tellus non magna. Nam ligula elit, pretium et, rutrum non, hendrerit id, ante. Nunc mauris sapien, cursus in,", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(3041), null, 44, 31, false },
                    { 34, "neque. Nullam ut nisi a odio semper cursus. Integer mollis. Integer tincidunt", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2822), null, 25, 61, false },
                    { 20, "eu metus. In lorem.", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2702), null, 66, 60, false },
                    { 78, "a odio semper cursus. Integer mollis. Integer tincidunt aliquam arcu. Aliquam ultrices iaculis odio. Nam interdum enim non nisi. Aenean eget metus. In nec orci. Donec nibh. Quisque nonummy ipsum non arcu. Vivamus sit amet risus. Donec egestas. Aliquam nec enim. Nunc ut erat. Sed nunc", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(3211), null, 40, 11, false },
                    { 37, "elit, pellentesque a, facilisis non, bibendum sed, est. Nunc laoreet lectus quis massa. Mauris vestibulum, neque sed dictum eleifend, nunc risus varius orci, in consequat", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2849), null, 25, 76, false },
                    { 69, "Donec tincidunt. Donec vitae erat vel pede blandit congue. In scelerisque scelerisque dui. Suspendisse ac metus vitae velit", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(3116), null, 24, 36, false },
                    { 14, "ac tellus. Suspendisse sed dolor. Fusce mi lorem, vehicula et, rutrum eu, ultrices sit amet, risus. Donec nibh enim, gravida sit amet, dapibus id, blandit at, nisi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin", new DateTime(2023, 4, 13, 15, 55, 31, 596, DateTimeKind.Local).AddTicks(2653), null, 39, 59, false }
                });

            migrationBuilder.InsertData(
                table: "PostTags",
                columns: new[] { "PostId", "TagId" },
                values: new object[,]
                {
                    { 76, 16 },
                    { 51, 10 },
                    { 51, 20 },
                    { 51, 16 }
                });

            migrationBuilder.InsertData(
                table: "PostTags",
                columns: new[] { "PostId", "TagId" },
                values: new object[,]
                {
                    { 26, 11 },
                    { 26, 18 },
                    { 11, 19 },
                    { 48, 19 },
                    { 49, 13 },
                    { 74, 12 },
                    { 26, 16 },
                    { 20, 13 },
                    { 18, 19 },
                    { 20, 14 },
                    { 59, 20 },
                    { 19, 8 },
                    { 59, 12 },
                    { 15, 9 },
                    { 15, 18 },
                    { 70, 21 },
                    { 20, 8 },
                    { 70, 9 },
                    { 18, 3 },
                    { 21, 14 },
                    { 7, 13 },
                    { 11, 9 },
                    { 26, 17 },
                    { 20, 10 },
                    { 39, 20 },
                    { 48, 13 },
                    { 60, 19 },
                    { 39, 14 },
                    { 28, 11 },
                    { 57, 17 },
                    { 77, 19 },
                    { 77, 7 },
                    { 65, 13 },
                    { 65, 8 },
                    { 65, 20 },
                    { 66, 4 },
                    { 79, 11 },
                    { 79, 7 },
                    { 79, 2 },
                    { 79, 9 },
                    { 5, 17 },
                    { 46, 6 }
                });

            migrationBuilder.InsertData(
                table: "PostTags",
                columns: new[] { "PostId", "TagId" },
                values: new object[,]
                {
                    { 46, 5 },
                    { 8, 2 },
                    { 28, 3 },
                    { 4, 13 },
                    { 4, 11 },
                    { 31, 2 },
                    { 31, 17 },
                    { 71, 7 },
                    { 60, 21 },
                    { 34, 15 },
                    { 75, 16 },
                    { 28, 4 },
                    { 22, 13 },
                    { 42, 10 },
                    { 42, 4 },
                    { 75, 12 },
                    { 75, 17 },
                    { 64, 5 },
                    { 64, 13 },
                    { 64, 20 },
                    { 4, 10 },
                    { 42, 16 },
                    { 71, 11 },
                    { 35, 20 },
                    { 46, 7 },
                    { 24, 4 },
                    { 43, 3 },
                    { 23, 11 },
                    { 54, 3 },
                    { 33, 12 },
                    { 73, 18 },
                    { 73, 10 },
                    { 67, 14 },
                    { 73, 15 },
                    { 33, 19 },
                    { 27, 16 },
                    { 45, 2 },
                    { 58, 18 },
                    { 23, 9 },
                    { 27, 6 },
                    { 53, 11 },
                    { 23, 7 }
                });

            migrationBuilder.InsertData(
                table: "PostTags",
                columns: new[] { "PostId", "TagId" },
                values: new object[,]
                {
                    { 68, 14 },
                    { 41, 5 },
                    { 44, 16 },
                    { 55, 18 },
                    { 32, 16 },
                    { 14, 12 },
                    { 14, 3 },
                    { 55, 12 },
                    { 32, 4 },
                    { 32, 9 },
                    { 67, 11 },
                    { 10, 11 },
                    { 24, 1 },
                    { 47, 4 },
                    { 47, 12 },
                    { 61, 4 },
                    { 61, 15 },
                    { 61, 6 },
                    { 38, 11 },
                    { 72, 10 },
                    { 71, 20 },
                    { 12, 11 },
                    { 30, 13 },
                    { 29, 12 },
                    { 29, 18 },
                    { 29, 19 },
                    { 3, 16 },
                    { 3, 4 },
                    { 55, 21 },
                    { 25, 2 },
                    { 63, 8 },
                    { 63, 6 },
                    { 63, 3 },
                    { 25, 11 },
                    { 56, 10 },
                    { 71, 8 },
                    { 71, 2 },
                    { 35, 15 },
                    { 25, 15 },
                    { 2, 1 },
                    { 25, 13 },
                    { 25, 5 }
                });

            migrationBuilder.InsertData(
                table: "PostTags",
                columns: new[] { "PostId", "TagId" },
                values: new object[,]
                {
                    { 78, 19 },
                    { 78, 15 },
                    { 30, 19 },
                    { 17, 12 },
                    { 2, 16 },
                    { 40, 11 },
                    { 21, 21 },
                    { 40, 18 },
                    { 2, 8 },
                    { 80, 6 },
                    { 2, 6 },
                    { 2, 19 },
                    { 69, 11 },
                    { 2, 10 },
                    { 52, 12 }
                });

            migrationBuilder.InsertData(
                table: "Votes",
                columns: new[] { "PostId", "UserId", "Vote" },
                values: new object[,]
                {
                    { 68, 43, 2 },
                    { 50, 74, 1 },
                    { 50, 46, 1 },
                    { 50, 77, 2 },
                    { 50, 44, 2 },
                    { 21, 74, 1 },
                    { 21, 19, 1 },
                    { 72, 67, 0 },
                    { 36, 25, 2 },
                    { 72, 11, 0 },
                    { 72, 29, 1 },
                    { 72, 35, 1 },
                    { 72, 15, 2 },
                    { 23, 7, 1 },
                    { 44, 49, 2 },
                    { 44, 24, 1 },
                    { 44, 27, 1 },
                    { 78, 79, 0 },
                    { 78, 1, 1 },
                    { 78, 25, 0 },
                    { 27, 35, 0 },
                    { 2, 52, 0 },
                    { 2, 69, 1 },
                    { 2, 70, 0 },
                    { 27, 78, 2 },
                    { 27, 50, 0 },
                    { 36, 18, 2 }
                });

            migrationBuilder.InsertData(
                table: "Votes",
                columns: new[] { "PostId", "UserId", "Vote" },
                values: new object[,]
                {
                    { 36, 24, 0 },
                    { 34, 38, 1 },
                    { 39, 68, 1 },
                    { 39, 30, 1 },
                    { 48, 64, 1 },
                    { 46, 50, 2 },
                    { 48, 11, 2 },
                    { 76, 20, 0 },
                    { 72, 72, 0 },
                    { 70, 55, 0 },
                    { 19, 74, 1 },
                    { 2, 24, 1 },
                    { 27, 19, 2 },
                    { 48, 56, 1 },
                    { 55, 78, 2 },
                    { 11, 7, 1 },
                    { 38, 50, 0 },
                    { 32, 67, 2 },
                    { 14, 25, 0 },
                    { 10, 20, 2 },
                    { 10, 25, 0 },
                    { 47, 22, 1 },
                    { 61, 53, 1 },
                    { 43, 57, 1 },
                    { 73, 13, 2 },
                    { 58, 4, 0 },
                    { 58, 10, 0 },
                    { 6, 59, 1 },
                    { 6, 52, 2 },
                    { 6, 72, 0 },
                    { 6, 27, 2 },
                    { 6, 80, 0 },
                    { 9, 39, 1 },
                    { 9, 25, 0 },
                    { 9, 75, 0 },
                    { 16, 65, 2 },
                    { 16, 68, 1 },
                    { 80, 22, 0 },
                    { 32, 11, 1 },
                    { 32, 50, 0 },
                    { 67, 52, 2 },
                    { 67, 25, 1 }
                });

            migrationBuilder.InsertData(
                table: "Votes",
                columns: new[] { "PostId", "UserId", "Vote" },
                values: new object[,]
                {
                    { 24, 71, 2 },
                    { 40, 51, 0 },
                    { 40, 74, 0 },
                    { 40, 38, 1 },
                    { 40, 9, 1 },
                    { 75, 16, 2 },
                    { 75, 55, 1 },
                    { 75, 14, 2 },
                    { 7, 52, 0 },
                    { 7, 33, 2 },
                    { 80, 32, 1 },
                    { 7, 16, 2 },
                    { 25, 63, 2 },
                    { 25, 53, 2 },
                    { 25, 26, 1 },
                    { 25, 28, 0 },
                    { 25, 78, 0 },
                    { 45, 9, 0 },
                    { 45, 18, 0 },
                    { 45, 52, 1 },
                    { 45, 66, 1 },
                    { 67, 54, 1 },
                    { 59, 54, 0 },
                    { 69, 53, 1 },
                    { 13, 15, 0 },
                    { 13, 32, 2 },
                    { 60, 39, 1 },
                    { 60, 66, 1 },
                    { 60, 35, 0 },
                    { 4, 70, 0 },
                    { 8, 71, 2 },
                    { 8, 46, 0 },
                    { 8, 56, 1 },
                    { 65, 27, 1 },
                    { 65, 76, 1 },
                    { 79, 67, 1 },
                    { 60, 10, 0 },
                    { 5, 48, 2 },
                    { 5, 63, 1 },
                    { 5, 3, 1 },
                    { 28, 51, 0 },
                    { 62, 18, 1 }
                });

            migrationBuilder.InsertData(
                table: "Votes",
                columns: new[] { "PostId", "UserId", "Vote" },
                values: new object[,]
                {
                    { 62, 44, 2 },
                    { 26, 29, 1 },
                    { 52, 73, 1 },
                    { 17, 16, 1 },
                    { 30, 24, 2 },
                    { 33, 55, 2 },
                    { 5, 79, 1 },
                    { 55, 80, 1 },
                    { 31, 21, 0 },
                    { 49, 61, 2 },
                    { 29, 15, 0 },
                    { 29, 29, 1 },
                    { 63, 7, 2 },
                    { 63, 51, 0 },
                    { 56, 67, 0 },
                    { 56, 7, 0 },
                    { 56, 13, 0 },
                    { 35, 16, 0 },
                    { 35, 4, 0 },
                    { 35, 39, 1 },
                    { 53, 54, 1 },
                    { 37, 4, 0 },
                    { 37, 9, 1 },
                    { 15, 71, 2 },
                    { 15, 51, 1 },
                    { 15, 6, 1 },
                    { 18, 53, 2 },
                    { 18, 37, 2 },
                    { 20, 10, 2 },
                    { 51, 77, 1 },
                    { 51, 30, 1 },
                    { 74, 64, 2 },
                    { 37, 28, 2 },
                    { 53, 33, 1 }
                });

            migrationBuilder.InsertData(
                table: "Replies",
                columns: new[] { "Id", "CommentId", "Content", "CreatedOn", "ModifiedOn", "UserId", "isDeleted" },
                values: new object[,]
                {
                    { 24, 3, "leo elementum sem, vitae aliquam", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(8877), null, 7, false },
                    { 23, 8, "suscipit nonummy. Fusce fermentum fermentum arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae Phasellus ornare. Fusce mollis.", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(8870), null, 77, false },
                    { 8, 8, "lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis diam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce aliquet magna a neque. Nullam ut nisi a odio semper cursus.", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(8756), null, 11, false },
                    { 5, 11, "accumsan laoreet ipsum. Curabitur consequat, lectus sit amet luctus vulputate, nisi sem semper erat, in consectetuer ipsum nunc id enim. Curabitur massa. Vestibulum accumsan neque et nunc. Quisque ornare tortor at risus. Nunc ac sem ut dolor dapibus gravida.", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(8733), null, 61, false },
                    { 44, 71, "tortor nibh sit amet orci.", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9094), null, 40, false },
                    { 12, 71, "tempus mauris erat eget ipsum. Suspendisse", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(8786), null, 30, false },
                    { 31, 36, "iaculis nec, eleifend non,", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(8994), null, 67, false },
                    { 26, 36, "quis, pede. Suspendisse dui. Fusce diam nunc, ullamcorper eu, euismod ac, fermentum vel, mauris. Integer sem elit, pharetra", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(8956), null, 8, false },
                    { 11, 44, "ac orci. Ut", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(8778), null, 27, false },
                    { 16, 63, "luctus et ultrices posuere cubilia Curae Donec tincidunt. Donec vitae erat vel pede blandit congue. In scelerisque scelerisque dui. Suspendisse ac metus vitae velit egestas lacinia. Sed congue, elit sed consequat auctor, nunc nulla vulputate dui, nec", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(8816), null, 37, false },
                    { 52, 1, "sapien imperdiet ornare. In faucibus. Morbi vehicula. Pellentesque tincidunt tempus risus. Donec egestas. Duis ac arcu. Nunc mauris. Morbi non sapien molestie orci tincidunt adipiscing. Mauris molestie pharetra nibh. Aliquam ornare, libero at auctor ullamcorper, nisl arcu iaculis enim, sit amet ornare lectus justo", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9153), null, 35, false },
                    { 29, 62, "suscipit, est ac facilisis facilisis, magna tellus faucibus leo, in lobortis tellus justo sit amet nulla. Donec non justo. Proin non massa non ante bibendum ullamcorper. Duis cursus, diam at pretium aliquet, metus urna convallis erat, eget", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(8979), null, 69, false },
                    { 68, 9, "vel, convallis in, cursus et, eros. Proin ultrices. Duis volutpat nunc sit amet metus. Aliquam erat volutpat. Nulla", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9275), null, 38, false },
                    { 77, 48, "posuere at, velit. Cras lorem lorem, luctus ut, pellentesque eget, dictum placerat, augue. Sed molestie. Sed id risus quis diam luctus lobortis. Class aptent taciti sociosqu", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9342), null, 54, false },
                    { 45, 48, "ac metus vitae", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9102), null, 57, false },
                    { 39, 6, "rutrum urna,", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9056), null, 46, false },
                    { 15, 6, "Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean eget magna. Suspendisse tristique neque venenatis lacus. Etiam bibendum fermentum metus. Aenean sed pede nec ante blandit viverra. Donec tempus, lorem", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(8809), null, 3, false },
                    { 74, 1, "tortor. Nunc commodo auctor velit. Aliquam nisl.", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9320), null, 79, false },
                    { 42, 31, "eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a,", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9080), null, 48, false },
                    { 21, 44, "leo elementum sem, vitae aliquam eros turpis non enim. Mauris quis turpis vitae purus gravida sagittis. Duis gravida. Praesent eu nulla at sem molestie sodales. Mauris blandit enim", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(8853), null, 64, false },
                    { 62, 77, "metus. Vivamus euismod urna. Nullam", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9232), null, 79, false },
                    { 76, 60, "Duis ac arcu. Nunc mauris. Morbi non sapien molestie orci tincidunt adipiscing. Mauris molestie pharetra nibh. Aliquam ornare, libero at auctor ullamcorper, nisl arcu iaculis enim, sit amet ornare lectus justo eu arcu. Morbi sit amet massa. Quisque porttitor eros nec tellus. Nunc lectus", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9335), null, 70, false },
                    { 63, 67, "malesuada malesuada. Integer id magna et ipsum cursus vestibulum. Mauris magna. Duis dignissim tempor arcu. Vestibulum", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9239), null, 40, false },
                    { 58, 67, "nisi a odio semper cursus. Integer mollis. Integer tincidunt aliquam arcu. Aliquam ultrices", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9201), null, 66, false },
                    { 78, 79, "aliquam eros turpis non enim. Mauris quis turpis vitae purus gravida sagittis. Duis gravida. Praesent eu nulla at sem molestie", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9351), null, 45, false },
                    { 10, 55, "volutpat nunc sit amet metus. Aliquam erat volutpat. Nulla facilisis. Suspendisse commodo tincidunt nibh. Phasellus nulla. Integer vulputate, risus", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(8771), null, 52, false },
                    { 72, 50, "consectetuer adipiscing elit. Etiam laoreet, libero et tristique pellentesque, tellus sem mollis dui,", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9306), null, 41, false },
                    { 33, 32, "sit amet orci. Ut sagittis lobortis mauris. Suspendisse aliquet molestie tellus. Aenean egestas hendrerit neque. In ornare sagittis felis. Donec tempor, est ac mattis semper, dui lectus rutrum urna, nec luctus felis purus ac tellus. Suspendisse sed dolor. Fusce mi lorem, vehicula et, rutrum eu, ultrices", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9009), null, 20, false },
                    { 69, 44, "eget, dictum placerat, augue. Sed molestie.", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9283), null, 12, false },
                    { 17, 32, "felis, adipiscing fringilla, porttitor vulputate, posuere vulputate, lacus. Cras interdum. Nunc sollicitudin commodo ipsum. Suspendisse non leo. Vivamus nibh dolor, nonummy ac, feugiat non,", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(8824), null, 19, false },
                    { 73, 15, "feugiat metus", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9313), null, 39, false },
                    { 34, 15, "convallis in, cursus et, eros. Proin ultrices. Duis volutpat nunc sit amet metus. Aliquam erat volutpat.", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9016), null, 67, false },
                    { 1, 22, "senectus et netus et malesuada fames ac turpis egestas. Fusce aliquet magna a neque. Nullam ut nisi a odio semper cursus.", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(7552), null, 38, false },
                    { 51, 23, "at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus.", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9146), null, 10, false },
                    { 9, 64, "Maecenas ornare egestas ligula. Nullam feugiat placerat velit. Quisque varius. Nam porttitor scelerisque neque. Nullam nisl. Maecenas", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(8764), null, 63, false },
                    { 70, 26, "amet orci. Ut sagittis lobortis mauris. Suspendisse aliquet molestie tellus. Aenean egestas hendrerit neque. In ornare", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9291), null, 47, false },
                    { 43, 26, "vehicula et, rutrum eu, ultrices sit amet, risus. Donec nibh enim,", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9087), null, 75, false },
                    { 3, 32, "nec luctus felis purus ac tellus. Suspendisse sed dolor. Fusce mi", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(8715), null, 76, false },
                    { 19, 31, "eleifend, nunc risus varius orci, in consequat enim diam vel arcu. Curabitur ut odio", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(8838), null, 73, false },
                    { 57, 10, "pede sagittis augue, eu tempor erat neque non quam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam fringilla cursus purus. Nullam scelerisque neque sed sem", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9192), null, 75, false },
                    { 48, 10, "accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis diam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce aliquet magna a neque. Nullam ut nisi a odio semper cursus. Integer mollis. Integer tincidunt aliquam arcu. Aliquam ultrices iaculis odio.", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9124), null, 3, false },
                    { 64, 4, "mattis semper, dui lectus rutrum urna, nec luctus felis purus ac tellus. Suspendisse sed dolor. Fusce mi lorem,", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9246), null, 74, false }
                });

            migrationBuilder.InsertData(
                table: "Replies",
                columns: new[] { "Id", "CommentId", "Content", "CreatedOn", "ModifiedOn", "UserId", "isDeleted" },
                values: new object[,]
                {
                    { 59, 7, "et, eros. Proin ultrices. Duis volutpat nunc sit amet metus. Aliquam erat volutpat. Nulla facilisis. Suspendisse commodo tincidunt nibh. Phasellus", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9209), null, 57, false },
                    { 80, 53, "magna", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9365), null, 55, false },
                    { 50, 24, "velit egestas lacinia. Sed congue, elit sed consequat auctor, nunc nulla vulputate dui, nec tempus mauris erat eget ipsum. Suspendisse sagittis. Nullam vitae diam. Proin dolor. Nulla semper tellus id nunc interdum feugiat. Sed nec metus facilisis lorem tristique aliquet. Phasellus fermentum convallis ligula. Donec luctus aliquet odio. Etiam", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9139), null, 21, false },
                    { 75, 72, "accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis diam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce aliquet magna a neque. Nullam ut nisi a odio", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9328), null, 13, false },
                    { 56, 74, "sapien imperdiet ornare. In faucibus. Morbi vehicula. Pellentesque tincidunt", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9185), null, 41, false },
                    { 14, 74, "luctus et ultrices posuere cubilia Curae", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(8801), null, 8, false },
                    { 79, 4, "id nunc interdum feugiat. Sed nec metus facilisis lorem", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9358), null, 33, false },
                    { 71, 73, "magna. Phasellus dolor elit, pellentesque a, facilisis non, bibendum sed, est. Nunc laoreet lectus quis massa. Mauris vestibulum, neque sed dictum", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9298), null, 70, false },
                    { 6, 73, "a ultricies adipiscing, enim mi tempor lorem, eget mollis lectus pede et risus. Quisque libero lacus, varius et, euismod et, commodo", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(8742), null, 59, false },
                    { 61, 35, "gravida molestie arcu. Sed eu nibh vulputate mauris sagittis placerat. Cras dictum ultricies ligula. Nullam enim. Sed nulla ante, iaculis nec, eleifend non, dapibus rutrum, justo. Praesent luctus. Curabitur egestas nunc sed libero. Proin sed turpis", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9224), null, 16, false },
                    { 35, 61, "fringilla euismod enim. Etiam gravida molestie arcu. Sed eu nibh vulputate mauris sagittis placerat. Cras dictum ultricies ligula. Nullam enim. Sed nulla ante, iaculis nec, eleifend non, dapibus rutrum, justo. Praesent luctus. Curabitur egestas nunc sed libero. Proin sed", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9024), null, 63, false },
                    { 60, 52, "Suspendisse sagittis. Nullam vitae diam. Proin dolor. Nulla semper tellus id nunc interdum feugiat. Sed nec metus facilisis lorem tristique aliquet. Phasellus fermentum", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9216), null, 18, false },
                    { 41, 20, "malesuada vel, venenatis vel, faucibus id, libero. Donec consectetuer mauris id sapien. Cras dolor dolor, tempus non, lacinia", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9072), null, 69, false },
                    { 20, 20, "dolor. Nulla semper tellus id nunc interdum feugiat. Sed nec metus facilisis lorem tristique aliquet. Phasellus fermentum convallis ligula. Donec luctus aliquet odio. Etiam ligula tortor, dictum eu, placerat eget, venenatis a, magna. Lorem ipsum dolor sit amet, consectetuer adipiscing", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(8845), null, 16, false },
                    { 38, 78, "arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae Donec tincidunt. Donec vitae erat vel pede blandit congue. In scelerisque scelerisque dui. Suspendisse ac metus vitae velit egestas lacinia. Sed congue, elit sed consequat auctor, nunc nulla vulputate dui, nec tempus", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9048), null, 10, false },
                    { 25, 73, "tincidunt congue turpis. In condimentum. Donec at arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae Donec tincidunt. Donec vitae erat vel pede blandit congue. In scelerisque scelerisque dui. Suspendisse", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(8948), null, 28, false },
                    { 27, 39, "lorem, auctor quis, tristique ac, eleifend vitae, erat. Vivamus nisi. Mauris nulla. Integer urna. Vivamus molestie dapibus ligula. Aliquam erat", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(8963), null, 32, false },
                    { 18, 66, "ultrices sit amet, risus. Donec nibh enim, gravida sit amet, dapibus id, blandit at,", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(8831), null, 13, false },
                    { 49, 33, "elit pede, malesuada vel, venenatis vel, faucibus id, libero. Donec consectetuer mauris id sapien. Cras dolor dolor, tempus non, lacinia at, iaculis quis, pede. Praesent eu dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9132), null, 75, false },
                    { 30, 65, "aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(8987), null, 38, false },
                    { 7, 25, "purus mauris a nunc. In at pede. Cras vulputate velit eu sem. Pellentesque ut ipsum ac mi eleifend egestas. Sed pharetra, felis eget varius ultrices, mauris ipsum porta elit, a feugiat tellus lorem eu metus. In lorem. Donec elementum, lorem ut aliquam iaculis, lacus", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(8749), null, 25, false },
                    { 55, 47, "ornare lectus justo eu", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9178), null, 5, false },
                    { 46, 47, "est mauris, rhoncus id, mollis nec, cursus a, enim. Suspendisse aliquet, sem ut cursus luctus, ipsum leo elementum sem, vitae aliquam eros turpis non enim. Mauris quis turpis vitae purus gravida sagittis. Duis gravida.", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9110), null, 44, false },
                    { 66, 40, "odio vel est tempor bibendum. Donec felis orci, adipiscing non, luctus sit amet, faucibus ut, nulla. Cras eu tellus eu augue porttitor interdum. Sed auctor odio a purus. Duis elementum, dui quis accumsan", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9261), null, 51, false },
                    { 40, 16, "vitae semper egestas, urna justo faucibus lectus, a sollicitudin orci sem eget massa. Suspendisse eleifend. Cras sed leo. Cras vehicula aliquet libero. Integer in magna. Phasellus dolor elit, pellentesque a, facilisis non, bibendum sed, est. Nunc laoreet lectus quis massa. Mauris vestibulum, neque", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9065), null, 17, false },
                    { 65, 29, "aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis diam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce aliquet magna a neque. Nullam ut nisi a odio semper cursus. Integer mollis. Integer tincidunt aliquam arcu. Aliquam ultrices iaculis odio. Nam interdum enim", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9254), null, 20, false },
                    { 22, 29, "Donec egestas. Aliquam nec enim. Nunc ut erat. Sed nunc est, mollis non, cursus non, egestas a, dui. Cras pellentesque. Sed dictum. Proin eget odio. Aliquam vulputate ullamcorper magna. Sed eu eros. Nam consequat dolor vitae dolor.", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(8862), null, 56, false },
                    { 4, 29, "Nam nulla magna, malesuada vel, convallis in, cursus et, eros. Proin ultrices. Duis volutpat nunc sit amet metus. Aliquam erat volutpat. Nulla facilisis. Suspendisse commodo tincidunt nibh. Phasellus nulla. Integer vulputate, risus a ultricies adipiscing, enim mi tempor lorem, eget mollis lectus pede et risus. Quisque libero", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(8726), null, 45, false },
                    { 37, 28, "quam quis diam. Pellentesque habitant morbi tristique senectus et netus et", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9040), null, 25, false },
                    { 36, 28, "eu tellus eu augue porttitor interdum. Sed auctor odio a purus. Duis elementum, dui", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9033), null, 76, false },
                    { 13, 28, "urna convallis erat, eget tincidunt dui augue eu tellus. Phasellus elit pede, malesuada vel, venenatis vel, faucibus id, libero. Donec consectetuer mauris id sapien. Cras dolor dolor, tempus non,", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(8793), null, 56, false },
                    { 67, 49, "dictum eu, placerat eget,", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9268), null, 57, false },
                    { 54, 49, "Vivamus molestie dapibus ligula. Aliquam erat volutpat. Nulla dignissim. Maecenas ornare egestas ligula. Nullam feugiat placerat velit. Quisque varius. Nam porttitor scelerisque neque.", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9169), null, 75, false },
                    { 32, 49, "tellus id nunc", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9002), null, 50, false },
                    { 28, 49, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Etiam laoreet, libero et tristique pellentesque, tellus sem mollis dui, in sodales elit erat vitae risus. Duis a mi fringilla mi", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(8971), null, 12, false },
                    { 47, 38, "nibh. Donec est mauris, rhoncus id, mollis nec, cursus a, enim. Suspendisse aliquet, sem ut cursus luctus, ipsum leo elementum sem, vitae aliquam eros turpis non enim. Mauris quis turpis vitae purus gravida sagittis. Duis gravida. Praesent eu nulla at sem molestie sodales. Mauris", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9117), null, 2, false },
                    { 2, 14, "ut cursus luctus, ipsum leo elementum sem, vitae aliquam eros turpis", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(8663), null, 26, false },
                    { 53, 14, "natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec dignissim magna a tortor. Nunc commodo auctor velit. Aliquam", new DateTime(2023, 4, 13, 15, 55, 31, 603, DateTimeKind.Local).AddTicks(9161), null, 71, false }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Comments_PostId",
                table: "Comments",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_UserId",
                table: "Comments",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_UserId",
                table: "Posts",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_PostTags_TagId",
                table: "PostTags",
                column: "TagId");

            migrationBuilder.CreateIndex(
                name: "IX_Replies_CommentId",
                table: "Replies",
                column: "CommentId");

            migrationBuilder.CreateIndex(
                name: "IX_Replies_UserId",
                table: "Replies",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Tags_PostId",
                table: "Tags",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_Email",
                table: "Users",
                column: "Email",
                unique: true,
                filter: "[Email] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Users_Username",
                table: "Users",
                column: "Username",
                unique: true,
                filter: "[Username] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Votes_UserId",
                table: "Votes",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PostTags");

            migrationBuilder.DropTable(
                name: "Replies");

            migrationBuilder.DropTable(
                name: "Votes");

            migrationBuilder.DropTable(
                name: "Tags");

            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "Posts");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
