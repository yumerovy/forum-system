﻿using FMS.Data.Models;
using FMS.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace FMS.Services.Services.Contracts
{
    public interface IHomeService
    {
        int GetPostsCount();

        int GetUsersCount();

        int GetAdmisCount();

        IEnumerable<PostDTO> GetMostCommentedPosts();

        IEnumerable<PostDTO> GetMostRecentPosts();

        IEnumerable<Post> GetTrendingPosts();


        IEnumerable<User> GetMostActiveUsers();

    }
}
